<?php

$lang['required'] = 'Le %s est n&eacute;cessaire.';
$lang['isset'] = 'Le %s doit avoir une valeur.';
$lang['valid_email'] = 'Le %s doit contenir une adresse email valide.';
$lang['valid_emails'] = 'Le %s ne doit contenir que des adresses emails valides.';
$lang['valid_url'] = 'Le %s doit contenir un URL valide.';
$lang['valid_ip'] = 'Le %s doit contenir une adresse IP valide.';
$lang['min_length'] = 'Le %s doit avoir au moins %s caract&egrave;res.';
$lang['max_length'] = 'Le %s ne peut pas avoir plus de %s caract&egrave;res.';
$lang['exact_length'] = 'Le %s doit contenir exactement %s caract&egrave;res.';
$lang['alpha'] = 'Le %s ne peut contenir que des caract&egrave;res alphab';
$lang['alpha_numeric'] = 'Le %s ne peut contenir que des caract&egrave;res alpha-num&eacute;riques.';
$lang['alpha_dash'] = 'Le %s ne peut contenir que des caract&egrave;res alpha-num&eacute;riques, des tirets bas ou des tirets.';
$lang['numeric'] = 'Le %s ne doit contenir que des nombres.';
$lang['is_numeric'] = 'Le %s ne peut contenir que des caract&egrave;res num&eacute;riques.';
$lang['integer'] = 'Le %s doit contenir un entier.';
$lang['matches'] = 'Le %s ne correspond pas au champ %s.';
$lang['is_natural'] = 'Le %s doit contenir un nombre positif.';
$lang['is_natural_no_zero'] = 'Le %s doit contenir un nombre plus grand que z&eacute;ro.';
?>