<?php $this->load->view('mainSideMenu.php') ?>


<div class="contentCenter">

	<h1>L'équipe du site</h1>
	<div style="float:left;">
	<?php foreach($team as $user): ?>

		<a href="<?php echo site_url('profil-'.$user['id'].'/'.url_title( $user['username'])) ?>" class="miniprofil<?php echo $user['online'] == 1 ? ' online' : '' ?>">
			<div class="avatar"><?php echo avatar_url($user['id'],true) ?></div>
			<div class="profilInfos">
				<div>
					<span class="pseudo" style="color:<?php echo $user['color'] ?>">
						<?php echo $user['sexe'] == 0 ? img('male.png') :  img('female.png') ?>
						<?php echo $user['username'] ?>
					</span>
					
					<span style="float:right;font-size:14px">
						<?php if($user['group'] == 0) echo '<span style="color:#ff8c00">Membre</span>'; ?>
						<?php if($user['group'] == 1) echo '<span style="color:#800000">Banni</span>'; ?>
						<?php if($user['group'] == 2) echo '<span style="color:#008000">Modérateur</span>'; ?>
						<?php if($user['group'] == 3) echo '<span style="color:#ff0000">Administrateur</span>'; ?>
						<?php if($user['group'] == 4) echo '<span style="color:#4169e1">Webmaster</span>'; ?>
					</span>
				</div>
				<div class="profilGils"> <?php echo number_format($user['gils'],1,',',' ') ?> gils</div>
			</div>
		</a>
	<?php endforeach; ?>
		<div style="clear:both"></div>
		<br />
	</div>
	
	<h1>Liste des <?php echo $nbMembers ?> membres</h1>
	<div style="text-align:center">
		<select class="txt" id="sortMembers" style="width:360px">
			<option <?php echo $sort == 1 ? 'selected' : '' ?> value="1">Trier par nombre de gils</option>
			<option <?php echo $sort == 2 ? 'selected' : '' ?> value="2">Trier par nombre de commentaires / postes</option>
			<option <?php echo $sort == 3 ? 'selected' : '' ?> value="3">Trier par nombre de messages sur la chatbox</option>
			<option <?php echo $sort == 4 ? 'selected' : '' ?> value="4">Trier par dernier passage</option>
			<option <?php echo $sort == 5 ? 'selected' : '' ?> value="5">Trier par nombre de connexions</option>
			<option <?php echo $sort == 5 ? 'selected' : '' ?> value="6">Trier par date d'inscription</option>
			<option <?php echo $sort == 6 ? 'selected' : '' ?> value="7">Trier par nom</option>
		</select>
	</div>
	<br />
	<br />

	<script>
	$('#sortMembers').on('change', function()
	{
		window.location = baseUrl+'listeDesMembres/'+$('#sortMembers').val()+'/0';
	});
	</script>

	<?php echo $this->pagination->create_links(); ?>

	<?php foreach($members as $user): ?>

		<a href="<?php echo site_url('profil-'.$user['id'].'/'.url_title( $user['username'])) ?>" class="miniprofil<?php echo $user['online'] == 1 ? ' online' : '' ?>">
			<div class="avatar"><?php echo avatar_url($user['id'],true) ?></div>
			<div class="profilInfos">
				<div>
					<span class="pseudo" style="color:<?php echo $user['color'] ?>">
						<?php echo $user['sexe'] == 0 ? img('male.png') :  img('female.png') ?>
						<?php echo $user['username'] ?>
					</span>
				</div>
				<div class="profilGils"> <?php echo number_format($user['gils'],1,',',' ') ?> gils</div>
			</div>
		</a>
	<?php endforeach; ?>

	<div style="clear:both"></div>
	<?php echo $this->pagination->create_links(); ?>
</div>