<?php $this->load->view('mainSideMenu.php') ?>


<div class="contentCenter">

	<h1><?php echo $title ?></h1>

	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>

	<div id="map" style="width:100%;height: 650px"></div>

	<script>
	var myOptions = {
	  center: new google.maps.LatLng(46.672056,2.557983),
	  zoom: 6,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	var map = new google.maps.Map(document.getElementById("map"), myOptions);
	var geocoder = new google.maps.Geocoder();

	function marqueurMembre(adresse,title, texte)
	{	
		geocoder.geocode( { 'address': adresse}, function(results, status) 
		{
			if (status == google.maps.GeocoderStatus.OK) 
			{
				var markerOptions = {
					map: map, 
					title: title, 
					position: results[0].geometry.location
				}
				
				var infowindow = new google.maps.InfoWindow({
					content: texte
				});
				
				var marker = new google.maps.Marker(markerOptions);
					
				google.maps.event.addListener(marker, 'click', function() 
				{
					infowindow.open(map,marker);
				});
				
			}
		});
	}

	<?php foreach($users as $user): ?>
	marqueurMembre('<?php echo addslashes($user['country'].' '.$user['city'].' '.$user['postal'].' '.$user['address']) ?>','<?php echo $user['username'] ?>', 'Voir le profil de <?php echo profil_url($user['username'],$user['id'],$user['color']) ?>');
	<?php endforeach; ?>


	</script>
</div>