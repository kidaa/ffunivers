<?php $this->load->view('mainSideMenu.php') ?>


<div class="contentCenter">

	<h1><?php echo $title ?></h1>
	
	<?php echo $this->pagination->create_links(); ?>


	<ul class="classic">
		<?php foreach($news as $actu): ?>
			<li>
				<a href="<?php echo site_url($actu['url']) ?>"><?php echo htmlspecialchars($actu['title']) ?></a>
				<span><?php echo madate($actu['date']) ?></span>
			</li>
		<?php endforeach; ?>
		</ul>

	<div style="clear:both"></div>
	<?php echo $this->pagination->create_links(); ?>
	
</div>