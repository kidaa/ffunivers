<?php $site = parse_url(base_url(),PHP_URL_HOST); ?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<link rel="icon" type="image/fid" href="<?php echo img_url('favicon.jpg') ?>" />
	<link rel="stylesheet" href="<?php echo css_url('reset') ?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo css_url('fonts') ?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo css_url('design') ?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo css_url($site) ?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo css_url('editor') ?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo css_url('superfish') ?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo plugin_url('tiptip/tipTip.css') ?>" type="text/css" />
	<script>var baseUrl = '<?php echo base_url() ?>';</script>
	<!-- // <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script> -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
	<script src="<?php echo js_url('form') ?>"></script>
	<script src="<?php echo js_url('superfish') ?>"></script>
	<script src="<?php echo js_url('hoverintent') ?>"></script>
	<script src="<?php echo plugin_url('tiptip/jquery.tipTip.minified.js') ?>"></script>
	
	<?php if(isset($noIndex)): ?>
		<meta name="robots" content="noindex, nofollow">
	<?php endif; ?>

	
	<title><?php echo htmlspecialchars(isset($headTitle) ? $headTitle : $title) ?></title>
	<?php if(@trim($description) != ''): ?> <meta name="description" content="<?php echo htmlspecialchars($description) ?>" /> <?php endif; ?>
	<script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>

	<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-8766549-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
	
	<?php if($site == 'www.ff10univers.com'): ?>
	<div style="text-align:center;color:#fff;padding-bottom:10px;font-size:20px;font-style:italic;text-shadow:1px 1px 0 #000;">
		Ce site web est actuellement en construction. Envie de participer au projet ? Contactez-moi <a style="color:#aac8ed" href="mailto:daeschlerdavid@hotmail.com">par mail en cliquant ici</a>.
	</div>
	<?php endif; ?>

	<?php list($gils, $nbMP,$group) = $this->users->getInfosHeader(); ?>
	<?php if($group == 1) exit('<span style="color:#fff">Vous êtes banni.</a>'); ?>
	<div class="global">
		<div class="header">
			<ul class="menuLeft">
				<li><a href="<?php echo base_url() ?>"><?php echo img('home.png','','margin-right:0') ?> </a>  <span></span> </li>
				<li><a href="<?php echo site_url('forum') ?>"><?php echo img('forum.png','') ?> Forum</a> <span></span> </li>
				<li><a href="<?php echo site_url('articles') ?>"><?php echo img('articles.png','') ?> Articles</a> <span></span> </li>
				<li><a href="<?php echo site_url('images') ?>"><?php echo img('images.png','') ?>Galerie</a> <span></span></li>
				<!-- <li><a target="_blank" href="http://www.ffsoundtrack.com/fr"><?php echo img('soundtrack.png','') ?> Musiques</a> <span></span> </li> -->
				<li class="plusSubMenu"><a nohref><?php echo img('plus.png','') ?> Site</a> 
					<ul class="subMenu">
						<li><a href="<?php echo site_url('listeDesMembres') ?>"><?php echo img('user.png','') ?> Membres</a>  </li>
						<li><a href="<?php echo site_url('rechercher') ?>"><?php echo img('searchHead.png','') ?>Rechercher</a> </li>
						<li><a href="<?php echo site_url('evenements') ?>"><?php echo img('calendar.png','') ?>Evènements</a> </li>
						<li><a title="Bientôt..." style="color:#666" href="<?php echo site_url('livreDor') ?>"><?php echo img('book.png','') ?>Livre d'or</a> </li>
						<li><a target="_blank" href="http://www.square-enix-boutique.com/?utm_source=site_ff13univers&utm_medium=lien_generique&utm_campaign=communaute_ff"><?php echo img('shop.png','') ?> Boutique</a></li>
					</ul>
				</li>

			</ul>

			
			<ul class="menuRight">
				
				<li class="plusSubMenu"><a href="<?php echo site_url('monCompte') ?>"><?php echo img('user.png') ?> <?php echo $this->session->userdata('username') ?></a> <span></span>
					<ul class="subMenu subMenuAccount">
						<li><a href="<?php echo site_url('monCompte/messagerie') ?>"><?php echo img('mail.png','') ?> 
							<?php if($nbMP == 0): echo 'Messagerie'; ?>
							<?php elseif($nbMP == 1): echo '1 message'; ?>
							<?php else: echo $nbMP.' messages'; endif; ?>
						</a>  </li>
						<li><a href="<?php echo site_url('monCompte/images') ?>"><?php echo img('images.png','') ?> Mes images</a>  </li>
						<li><a href="<?php echo site_url('monCompte/contributions') ?>"><?php echo img('articles.png','') ?> Mes contributions</a>  </li>
						<?php if($this->session->userdata('isAnonymous') == 0): ?>
							<li><a href="<?php echo site_url('profil-'.$this->session->userdata('id').'/'.$this->session->userdata('username')) ?>"><?php echo img('profil.png','') ?> Voir mon profil</a>  </li>
							<li><a href="<?php echo site_url('logout') ?>"><?php echo img('logout2.png','') ?> Deconnexion</a>  </li>
						<?php endif; ?>
					</ul>
				</li>
				
				<li>
					<a <?php if($nbMP > 0) echo 'style="text-decoration:blink"'; ?>  href="<?php echo site_url('monCompte/messagerie') ?>" class="letter"> <?php echo img('letter.png') ?> 
						<?php echo $nbMP ?>
					</a> 
					<span></span>
				</li>
				
				<li>
					<a href="<?php echo site_url('monCompte/gils') ?>" class="gold"> <?php echo img('gold.png') ?> <?php echo number_format($gils,0,',',' ') ?> gils</a> 
					<?php if($this->session->userdata('isAnonymous') == 0): ?>
						<span></span>
					<?php endif; ?>
				</li>
				
				<?php if($this->session->userdata('group') > 1): ?>
					<li><a href="<?php echo site_url('admin/content') ?>" class="letter"> <?php echo img('admin.png','') ?>Admin</a></li>
				<?php endif; ?>
				
					<!-- <li><a href="<?php echo site_url('logout') ?>" class="letter"> <?php echo img('logout.png','','margin-right:0') ?></a></li> -->
			</ul>
		</div>


			<script>
				var config = {    
				     over: showSubmenu,  
				     timeout: 600,
				     sensitivity:30,
				     out: hideSubmenu 
				};

				$('.plusSubMenu').hoverIntent(config);

				function showSubmenu()
				{
	
					$(this).children('.subMenu').stop(true,true).slideDown(200);
				}

				function hideSubmenu()
				{
					$(this).children('.subMenu').stop(true,true).slideUp(200);
				}
			</script>

		<?php if(!isset($_COOKIE['closeFollow'])): ?>
		<div id="boxFollow">
			<div id="follow">
				<a href="<?php echo site_url('closeFollow') ?> " class="closeFollow" title="Ok, j'ai compris !"></a>
				<strong>Suivez-nous :)</strong>

				<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2Fffunivers&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font=trebuchet+ms&amp;height=21&amp;appId=116379825130127" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe>
				<br />
				<a href="https://twitter.com/ff_univers" class="twitter-follow-button" data-show-count="false" data-lang="fr" data-dnt="true">Suivre @ff_univers</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

			</div>
		</div>
		<script type="text/javascript">
			$('.closeFollow').on('click', function()
			{
				$.ajaxSetup({ cache: true });

				$.get($(this).attr('href'), function()
				{
					$('#follow').fadeOut();
					$.ajaxSetup({ cache: false });

				});
				return false;
			})

		</script>
		<?php endif; ?>

		<a href="<?php echo base_url() ?>" class="banner" style="background-image:url(' <?php echo img_url('banner_'.$site.'.png') ?>');"></a>


		<div class="menuHead">
			<?php echo treeNavToMainMenu( $this->contents->getTreeNav() ) ?>
			<a id="menuAdd" href="<?php echo site_url('editerContenu/0/2') ?>" title="Envie de contribuer au site ? <br> Ajoutez des rubriques simplement !"></a>
		</div>
		
		
		
		<div class="body">
			<div id="content">
			<?php $this->load->view($view.'.php'); ?>
			</div>
			<div style="clear:both"></div>

		</div>
	</div>

	<div class="footer">
		<?php echo parse_url(base_url(),PHP_URL_HOST) ?> - 2008 / <?php echo date('Y') ?> 
		- 
		<a target="_blank" href="http://daeschlerdavid.fr/">CV / Portfolio</a> 
		-	
		<a href="mailto:aoemaster@ffunivers.com">Contact</a>
		<br />
		<a target="_blank" href="http://www.finaland.com/">Finaland</a>
		-
		<a target="_blank" href="http://cyberlight.ptibook.com/">Cyberlight</a>
		-
		<a target="_blank" href="http://www.lapausegeek.fr/">Le blog geek</a>
		-
		<a target="_blank" href="http://tombeaucroft.net/">Tombeau Croft</a>
		-
		<a target="_blank" href="http://www.triple-triad-online.com/">Triple Triad Online</a>

	</div>
	
	<script>
	$('.menuHead ul').superfish({
		delay:       1200,                           
		animation:   {opacity:'show',height:'show'}, 
		speed:       'fast',
		autoArrows:  false,                           
		dropShadows: false                            
	});
			
	// alert($('.contentCenter').css('height'));
	// auto scroll sur le menu de gauche
	$(document).scroll(function() 
	{
		var fixed = $(document).scrollTop() >= 224 ;
		
		$('.sideMenuLeft').toggleClass('fixed', fixed);

	});
	
	
	$('*[title]').tipTip({maxWidth: "300px", edgeOffset: 4, delay: 50});


	</script>
	
	
	
</body>
</html>