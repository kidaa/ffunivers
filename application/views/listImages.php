<div class="sideMenuLeft">
	<div class="menu">
	<form class="noValid" id="filtresGallery">
		<ul class="classic mainSide">
			<li><a style="background-image:url(<?php echo img_url('addImage.png') ?>);" href="<?php echo site_url('monCompte/ajouterImages') ?>">Ajouter des images</a></li>
		</ul>
		<br />
		<h4>Catégories</h4>
	
		<?php foreach($categories as $cat):	
		if($cat['content_type'] != 4) continue; ?>	
			<input checked type="checkbox" name="categories[]" id="cat_<?php echo $cat['id'] ?>" value="<?php echo $cat['id'] ?>"> 
			<label for="cat_<?php echo $cat['id'] ?>"> <?php echo $cat['name'] ?></label><br />
		<?php endforeach ?>
		<br />
		<h4>Type d'images</h4>
		<?php foreach($categories as $cat):	
		if($cat['content_type'] != 5) continue; ?>	
			<input checked type="checkbox" name="categorieTypes[]" id="cat_<?php echo $cat['id'] ?>" value="<?php echo $cat['id'] ?>"> 
			<label for="cat_<?php echo $cat['id'] ?>"> <?php echo $cat['name'] ?></label><br />
		<?php endforeach ?>
		
	</form>
	</div>
	
	<div class="pub">
		<script type="text/javascript"><!--
		google_ad_client = "ca-pub-5044588723656079";
		/* FF13v4 */
		google_ad_slot = "7319975990";
		google_ad_width = 200;
		google_ad_height = 200;
		//-->
		</script>
		<script type="text/javascript"
		src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
	</div>
</div>

<div class="contentCenter">
	<h1><?php echo $title ?></h1>
	
	<div class="infosContent">Descendez tout en bas de la page pour afficher de nouvelles images</div>
	
	<div class="allImages" id="allImages"></div>
	
	<div style="clear:both"></div>
</div>


<script>
var page = 0;
function displayImages()
{
	page++;		
	$.post(baseUrl+'getImagesFromAjax/'+page, $('#filtresGallery').serialize(),
	function(data)
	{
		arrayImages = $.parseJSON(data);
		
		for(key in arrayImages)
		{
			$('#allImages').append('<a style="display:none" onclick="return hs.expand(this)" target="_blank" href="'+baseUrl+'public/images_upload/big/'+arrayImages[key]['name']+'"><img src="'+baseUrl+'public/images_upload/mini/'+arrayImages[key]['name']+'" /></a>');
		}
		
		$('#allImages a:hidden').each( function()
		{
			$(this).fadeIn(500);
		});
		
		hs.updateAnchors();
			
	});
}

displayImages();


// initialise les bonnes valeurs checked en cas de f5
$('#filtresGallery input').attr('checked',true);

$('#filtresGallery input').click( function()
{
	page = 0;
	$('#allImages').html('');
	displayImages();
});


$(window).scroll(function() {
  
	if ($(window).scrollTop()+1 == $(document).height() - $(window).height())
	{
		displayImages();
	}
	else if ($(window).scrollTop() == $(document).height() - $(window).height())
	{
		displayImages();
	}
});

</script>

<script src="<?php echo plugin_url('highslide/highslide-with-gallery.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo plugin_url('highslide/highslide.css') ?>" />
<script>
    hs.graphicsDir = '<?php echo base_url() ?>public/plugins/highslide/graphics/';
	hs.align = 'center';
	hs.transitions = ['expand', 'crossfade'];
	hs.fadeInOut = true;
	hs.dimmingOpacity = 0.7;
	hs.captionEval = 'this.thumb.alt';

	// Add the slideshow providing the controlbar and the thumbstrip
	hs.addSlideshow({
		//slideshowGroup: 'group1',
		interval: 5000,
		repeat: false,
		useControls: false,
		thumbstrip: {
			position: 'middle left',
			mode: 'vertical',
			relativeTo: 'viewport'
		}
	});
</script>
