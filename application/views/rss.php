<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<rss version="2.0">
	<channel>
		<title><?php echo $title ?></title> 
		<link><?php echo $feed_url ?></link>
		<description><?php echo $page_description ?></description>


		<?php foreach($news as $new): ?>
			<item>
				<title><?php echo $new['title'] ?></title> 
				<link><?php echo site_url($new['url']) ?></link> 
				<description><?php echo htmlspecialchars($new['content']) ?></description> 
				<pubDate><?php echo date('D, d M Y H:i:s', datetime2timestamp($new['date'])) ?> GMT</pubDate>
			</item>
		<?php endforeach; ?>
	</channel>
</rss>