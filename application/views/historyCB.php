<?php $this->load->view('mainSideMenu.php') ?>

<div class="contentCenter">
	<h1><?php echo $title ?></h1>

	<div id="historyCB"></div>

	
	<script>
	$.ajaxSetup({cache:false});

	var page = 0;
	function displayhistory()
	{
		page++;		
		$.getJSON(baseUrl+'listMsg/'+page,
		function(dataJSON)
		{
			var tab = new Array();
			var i = 0;
			for(key in dataJSON)
			{
				tab[i] = dataJSON[key];
				i++;
			}

			tab.sort();
			tab.reverse();

			for(var i = 0; i < tab.length ; i++)
			{
				$('#historyCB').append(tab[i]);
			}
		
			$('#historyCB .message:hidden').fadeIn(500);
				
		});
	}

	displayhistory();


	$(window).scroll(function() {
	  
		if ($(window).scrollTop()+1 == $(document).height() - $(window).height())
		{
			displayhistory();
		}
		else if ($(window).scrollTop() == $(document).height() - $(window).height())
		{
			displayhistory();
		}
	});

	</script>
</div>