<div class="myImages allImages" id="allImages">
	<?php foreach($myImages as $img): ?>
		<img path="<?php echo site_url('public/images_upload/big/'.$img['name']) ?>" src="<?php echo site_url('public/images_upload/mini/'.$img['name']) ?>" />
	<?php endforeach ?>
</div>


<h1 class="mainH1"><?php echo $title ?></h1>

<?php if(isset($infos['idCreation'])): ?>
	<div class="question">
		<div></div>
		<p>
			Vous avez la possibilité d'éditer cette page du site si vous pensez qu'elle contient une erreur, ou bien si vous voulez tout simplement contribuer à son contenu. Les administrateurs seront avertis de votre modification, verront quelles lignes vous avez modifié, et publieront votre contribution si ils l'estiment nécessaire. Si votre modification est publié, votre pseudonyme apparaitra sur la page en question et bien sur,  <a href="<?php echo site_url('monCompte/gils') ?>">vous recevrez des gils !</a>
		</p>
	</div>
<?php endif; ?>



<script src="<?php echo plugin_url('ckeditor/ckeditor.js') ?>"></script>
<script src="<?php echo plugin_url('jcrop/js/jquery.Jcrop.min.js') ?>"></script>
<link rel="stylesheet" href="<?php echo plugin_url('jcrop/css/jquery.Jcrop.min.css') ?>" type="text/css" />

<form editor="1" method="post" action="<?php echo site_url('submitContent') ?>">

	<div class="cadreParam">
		<h2>Paramètres du contenu</h2>
		<?php if(isset($infos['idCreation'])) 
		echo '<div title="Ces paramètres sont des données sensibles, merci de ne les modifier qu\'en cas de réelle nécessité." class="showSettingsAction">Modifier les paramètres du contenu</div>' ?>
		
		
		<div class="showSettings" <?php if(isset($infos['idCreation'])) echo 'style="display:none"' ?>>
		
			<div class="warning">
			<div></div>
			<p>
				Les champs <span style="color:red">(*)</span> sont obligatoires. <br />
				Pour avoir des indications supplémentaires concernant certains champs, laissez votre souris au dessus de l'intitulé du champ. <br />
				L'ajout de contenu est bien entendu <a href="<?php echo site_url('monCompte/gils') ?>">récompensé par des gils</a>.
			</p>
		</div>
		
			<div class="lineForm">
				<label for="">Type de contenu <span>(*)</span></label>
				<select class="txt" name="type">
					<?php 
					foreach($contentTypes as $contentType): 
						if( (isset($infos['type']) && $infos['type'] == $contentType['id']) || $defaultType == $contentType['id'])
						$selected = 'selected';
						else
						$selected = '';
						
						echo '<option '.$selected.' value="'.$contentType['id'].'">'.$contentType['name'].'</option>';
					endforeach;
					?>
				</select>
			</div>
			
			<div class="lineForm">
				<label title="Pour les actualités, choisissez simplement la catégorie qui lui correspond. Pour les rubriques,  il faut séléctionner la sous-catégorie dans laquelle votre page doit être 'rangée'. Ceci permet d'avoir un contenu organisé et cohérent, merci de ne pas négliger cet aspect important." for="">Catégorie <span>(*)</span></label>
				<select class="txt" name="categorie"></select>
			</div>
			
			<?php foreach($contentTypes as $contentType): ?>
				<select style="display:none" id="categorie_<?php echo $contentType['id'] ?>">
					<?php if($contentType['id'] == 2): ?>
						<option value="0">Aucune</option>
						<?php echo treeNavToOptions($treeNav, (isset($infos['parent']) ? $infos['parent'] : 0)); ?>
					<?php else: ?>		
						<?php 
						foreach($categories as $cat)
						{
							if($cat['content_type'] == $contentType['id'])
							{
								if(isset($infos['categorie']) && $infos['categorie'] == $cat['id'])
								$selected = 'selected';
								else
								$selected = '';
						
								echo '<option '.$selected.' value="'.$cat['id'].'">'.$cat['name'].'</option>';
							}
						}
						?>
					<?php endif ?>
				</select>
			<?php endforeach; ?>
			
			<div class="lineForm">
				<label for="" title="Le titre doit être explicite et donner envie à l'internaute de cliquez sur le lien. Veuillez ne pas ajouter d'articles tels que &quot;Le&quot;, &quot;La&quot;, &quot;Les&quot;, ou &quot;L' &quot;">Titre du contenu <span>(*)</span></label>
				<input type="text" name="title" class="txt" value="<?php echo @htmlspecialchars($infos['title']) ?>" />
			</div>
			
			<div class="lineForm">
				<label title="Si vous voulez que cette page renvoie vers un autre lien automatiquement, remplissez ce champ avec l'url en question. La plupart du temps ce champs n'est pas nécessaire, laissez le donc vide." for="">Rediriger vers</label>
				<input type="text" name="redirect" class="txt" value="<?php echo @htmlspecialchars($infos['redirect']) ?>" />
			</div>
			
			<div class="lineForm">
				<label title="La description est facultative mais recommandée car elle permet d'indiquer aux moteurs de recherches de quoi la page parle. Elle doit être courte, deux phrases maximum qui indiquent rapidement de quoi il s'agit. C'est un atout pour attirer plus de visiteurs." for="">Description</label>
				<input type="text" name="description" class="txt" value="<?php echo @htmlspecialchars($infos['description']) ?>" />
			</div>
			
			<input type="hidden" id="x" name="x" />
			<input type="hidden" id="y" name="y" />
			<input type="hidden" id="w" name="w" />
			<input type="hidden" id="h" name="h" />
			
			<div class="gestionImage">
				<div class="lineForm">
					<label title="Ce champ est uniquement obligatoire pour les articles. Il permet d'ajouter une image de présentation à l'article afin d'embélir son lien d'accès. Fournissez l'adresse d'une image ou séléctionnez en une dans vos images via le bouton à droite, puis recadrez l'image selon vos envies. Laissez simplement ce champ vide pour une actualité." for="">Image de présentation <span id="articleStar">(*)</span></label>
					<input type="text" name="image" class="txt" value="<?php echo isset($infos['idCreation']) && trim($infos['image']) != '' ? htmlspecialchars(site_url($infos['image'])) : '' ?>" />  <input id="btnMyImages" class="btn2" type="button" value="Parcourir mes images" />
				</div>
				
				<br />
				<img id="cropbox" src="" >
			</div>
		</div>
	</div>
	
	<input type="hidden" name="id" value="<?php echo isset($infos['idCreation']) ? $infos['id'] : 0; ?>" />
	<input type="hidden" name="idCreation" value="<?php echo isset($infos['idCreation']) ? $infos['idCreation'] : 0; ?>" />
	<input type="hidden" name="editFromVersion" value="<?php echo isset($infos['idCreation']) ? $infos['version'] : 0; ?>" />
	
	<div class="helper">
		<a nohref title="Afin de garder une cohérence dans le site, évitez de rajouter des styles (couleurs, gras...) aux titres. Respectez vos espacements, gardez une cohérence dans la structure de votre page. Ne mettez pas trop d'espaces blancs. Pensez à justifier les paragraphes plus conséquents.">Normes à respecter</a> 
		| 
		<a nohref title="Veuillez ne pas dépasser 700px de largeur pour les tableaux.">Les tableaux</a> 
		| 
		<a nohref title="Pour faire un lien vers un endroit particulier dans une page, rendez-vous tout d'abord dans la page en question. Cliquez ensuite sur un titre de niveau 2 dans cette page. L'adresse dans la barre d'adresse en haut du navigateur changera alors. Faites ensuite un lien habituel vers cette adresse pour automatiquement amener l'internaute à cet endroit de la page.">Les ancres</a> 
		| 
		<a nohref title="La touche entrée provoque la création d'un nouveau paragraphe. Si vous voulez aller à la ligne, appuyez plutôt sur les touches &quot;SHIFT&quot; et &quot;ENTREE&quot;.">Les sauts de lignes</a> 
		| 
		<a nohref title="Les paragraphes sont automatiquemnt indentés avec un alinéa et des marges inférieurs et supérieurs. Pour éviter ce comportement, dans la liste déroulante &quot;Format&quot;, séléctionnez le choix &quot;Normal (DIV)&quot;">Les paragraphes</a> 
		| 
		<a nohref title="Pour intégrer correctement des images, uploadez les via votre compte membre. Chaque image possède sa version miniature : pour l'intégrer et faire en sorte que cette miniature s'agrandisse vers une grande image lorque le visiteur cliquera dessus, récupérez l'adresse de l'image et remplacez le terme &quot;big&quot;  par &quot;mini&quot;. Incluez cette petite image dans votre page, puis dans l'onglet &quot;Lien&quot; dans les propriétés de l'image, mettez l'adresse de la grande image (avec le terme &quot;big&quot; dans l'adresse).">Les images</a>
		|
		<a nohref title="Pensez à aligner vos images à droite/gauche des paragraphes, l'effet est plus jolie. Pour éviter que le texte soit collé à l'image, mettez au moins la valeur 10 dans le champ &quot;Espacement horizontal&quot; des propriétés de l'image.">Astuces</a> 
		
	</div>
	
	<textarea name="contentTxt" id="contentTxt"><?php echo @$infos['content'] ?></textarea>

	<div style="text-align:center">
		<input type="submit" value="Valider" class="btn" />
	</div>
</form>



<script>
var jcrop_api;
$(window).load( function(){


	CKEDITOR.replace('contentTxt',
	{
		toolbar : [
		{ name: 'document', items : [ 'Source','-','DocProps','ShowBlocks' ] },
		{ name: 'clipboard', items : [ 'Undo','Redo' ] },
		{ name: 'styles', items : [ 'Format','FontSize' ] },
		{ name: 'links', items : [ 'Link','Unlink'] },
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','-','RemoveFormat' ] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote',
		'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
		{ name: 'insert', items : [ 'Image','Table','HorizontalRule' ] },
		{ name: 'colors', items : [ 'TextColor','BGColor' ] }
	],
		entities : false,
		bodyClass:'editor',
		format_tags: 'p;div;h2;h3;h4;h5;h6',
		fontSize_sizes:'14/14px;15/15px;16/16px;17 (par défaut)/17px;18/18px;19/19px;20/20px;21/21px;22/22px;23/23px;24/24px;25/25px;',
		height:660,
		contentsCss: ['<?php echo css_url('reset') ?>', '<?php echo css_url('fonts') ?>', '<?php echo css_url('editor') ?>', '<?php echo css_url('editorTextarea') ?>']
	}); 




	// ----------------------------------------------
	// Gestion des images
	// ----------------------------------------------

	$('input[name=image]').on('keyup', showFullImage);
	$('input[name=image]').on('click', showFullImage);

	 

		jcrop_api = $.Jcrop('#cropbox',
		{
			boxWidth: 900,
			bgOpacity:0.4,
			onSelect: updateCoords,
			onChange: updateCoords,
			addClass: 'jcrop-dark'
		});

	jcropUrl = '';


	function showFullImage()
	{
		var urlImg = $('input[name=image]').val();
		if(jcropUrl == urlImg) return false;
		
		jcrop_api.setImage(urlImg);
		jcropUrl = urlImg;
		
		
		setRatio();
	}

	function setRatio()
	{
		var currentType = $('select[name=type]').val();
		if(currentType == 1) // news
		{
			jcrop_api.setOptions({ aspectRatio: <?php echo round($size_slider[0]/$size_slider[1],5) ?> });
		}
		if(currentType == 3) // article
		{
			jcrop_api.setOptions({ aspectRatio: <?php echo round($size_article[0]/$size_article[1],5) ?> });
		}
	}

	function updateCoords(c)
	{
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
	}
	showFullImage();


	// ----------------------------------------------
	// Gestion "mes images"
	// ----------------------------------------------
	$('.myImages img').on('click', function()
	{
		$('input[name=image]').val( $(this).attr('path'));
		showFullImage();
	});


	$('#btnMyImages').on('click', function()
	{

		if($('.myImages').css('display') == 'none')
		{
			$('.myImages').slideDown(300);
			$('#btnMyImages').val('Fermer mes images');
		}
		else
		{
			$('.myImages').slideUp(300);
			$('#btnMyImages').val('Parcourir mes images');
		}
	});


	// ----------------------------------------------
	// Gestion des parametres lors d'une edition
	// ----------------------------------------------
	$('.showSettingsAction').on('click', function()
	{
		if($('.showSettings').css('display') == 'none')
		{
			$('.showSettings').slideDown(200);
			$('.showSettingsAction').html('Fermer la modification des paramètres');
		}
		else
		{
			$('.showSettings').slideUp(200);
			$('.showSettingsAction').html('Modifier les paramètres du contenu');
		}
	});


	// ----------------------------------------------
	// Gestion des catégories
	// ----------------------------------------------
	$('select[name=type]').on('change', showSelectCategorie);

	var currentCat = null;
	function showSelectCategorie()
	{
		catToShow = $('select[name=type]').val();
		
		if(catToShow == 1)
		{
			$('.gestionImage').show();
			$('#articleStar').hide();
			setRatio();
		}
		else if(catToShow == 3)
		{
			$('.gestionImage').show();
			$('#articleStar').show();
			setRatio();
		}
		else
		{
			$('.gestionImage').hide();
			$('#articleStar').hide();
		}

		// affiche les bonnes catégories
		$('select[name=categorie]').html( $('#categorie_'+catToShow).html() )
		currentCat = catToShow;
	}

		
	showSelectCategorie();



	// ----------------------------------------------
	// Gestion de l'url
	// ----------------------------------------------
	$('input[name=title]').on('keyup', updateUrl);
	$('input[name=title]').on('click', updateUrl);

	function replaceAll(str, search, repl) {
	 while (str.indexOf(search) != -1)
	  str = str.replace(search, repl);
	 return str;
	}

	function updateUrl()
	{
		var title = $('input[name=title]').val();
		title = title.toLowerCase();

		var norm = new Array('À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë',
		'Ì','Í','Î','Ï', 'Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý',
		'Þ','ß', 'à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','ì','í','î',
		'ï','ð','ñ', 'ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ý','þ','ÿ');
		var spec = new Array('a','a','a','a','a','a','a','c','e','e','e','e',
		'i','i','i','i', 'd','n','o','o','o','0','o','o','u','u','u','u','y',
		'b','s', 'a','a','a','a','a','a','a','c','e','e','e','e','i','i','i',
		'i','d','n', 'o','o','o','o','o','o','u','u','u','u','y','y','b','y');

		for (var i = 0; i < spec.length; i++)
		title = replaceAll(title, norm[i], spec[i]);
		
		
		title = title.replace(/[^a-zA-Z 0-9]+/g,'');
		title = title.replace(/ /g, '-');
		
		$('input[name=urlAccess]').val(baseUrl+title);
	}

});

</script>