<?php $this->load->view('mainSideMenu.php') ?>

<div class="contentCenter">
	<h1><?php echo $title ?></h1>

	<?php
	foreach($comments as $commentaire)
	{
		echo '<a class="titleLastmsg" href="'.$commentaire['url'].'">'.$commentaire['categorie'].'<span> > </span>'.htmlspecialchars($commentaire['title']).'</a>';
		echo '<div idComm="'.$commentaire['id'].'" class="commentaire">';
			echo '<div class="commAvatar">'.avatar_url($commentaire['user']).'</div>';

			echo '<div class="commPseudo">';
			
				echo profil_url($commentaire['username'],$commentaire['user'],$commentaire['color']);
				echo ' <span class="commTime">'.madate($commentaire['date']).'</span>';
			
				echo '<div class="commActions">';
					if($this->session->userdata('group') >= 2)
					echo '<a class="commSuppr" href="'.site_url('commSuppr/'.$commentaire['id']).'">'.img('mini_delete.png').' Supprimer</a>';
					
					if($commentaire['user'] == $this->session->userdata('id'))
					echo '<a class="commEdit" href="'.site_url('editComm/'.$commentaire['id']).'">'.img('editComm.png').' Edition</a>';
				echo '</div>';
			echo '</div>';
			
			echo '<p>'.montexte($commentaire['message']).'</p>';
		echo '</div>';
	}

	?>
		
</div>

<script src="<?php echo js_url('comments') ?>"></script>
<script src="<?php echo plugin_url('highslide/highslide.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo plugin_url('highslide/highslide.css') ?>" />
<script>
hs.graphicsDir = '<?php echo base_url() ?>public/plugins/highslide/graphics/';
</script>