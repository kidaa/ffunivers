<?php $this->load->view('mainSideMenu.php') ?>

<div class="contentCenter">

	<h1>Informations</h1>

	<ul class="stats" style="float:left;margin-right:80px">
		<li><span>Personnes connectées </span> <?php echo $connected ?> </li>
		<li><span>Robots connectés </span> <?php echo $robotsConnected ?> </li>
		<li><span>Messages chatbox </span> <?php echo $chatbox ?> </li>
		<li><span>Nombre de membres </span> <?php echo $members ?> </li>
		<li><span>Total des gils </span> <?php echo number_format($gils['gils'],1,',',' ') ?> </li>
		<li><span>Actualités </span> <?php echo $news ?> </li>
		<li><span>Rubriques </span> <?php echo $rubriques ?> </li>
		<li><span>Articles </span> <?php echo $articles ?> </li>
	</ul>

	<ul class="stats">
		<li><span>Sujets privés</span> <?php echo $mpSubject ?> </li>
		<li><span>Messages privés </span> <?php echo $mp ?> </li>
		<li><span>Sujets forum</span> <?php echo $topics ?> </li>
		<li><span>Messages forum </span> <?php echo $forumMessages ?> </li>
		<li><span>Sondages </span> <?php echo $sondages ?> </li>
		<li><span>Commentaires </span> <?php echo $coms ?> </li>
		<li><span>Images envoyées </span> <?php echo $images ?> </li>
		<li><span>Images dans la galerie </span> <?php echo $galery ?> </li>
	</ul>

	<br />
	<h1>Statistiques</h1>

	<form id="formInterval" method="post" class="noValid" style="float:left;width:43%">
		<div class="lineForm">
			<label>Depuis quelle date ?</label> <input style="width:70px;text-align:center" class="txt" type="text" name="from" value="<?php echo $from ?>" />
		</div>
		<div class="lineForm">
			<label>Combien de jours ?</label> <input style="width:70px;text-align:center" class="txt" type="text" name="daysInterval" value="<?php echo $daysInterval ?>" />
		</div>

		<div class="lineForm">
			<label>&nbsp;</label><input type="submit" class="btn2" value="Valider" />
		</div>

	</form>
	<div class="resultVisits" style="padding-left:15px;float:left;height:270px;overflow:auto;border-left:1px solid #ccc;width:385px;margin-bottom:25px"></div>

	<br />
	<h1>Dernieres pages vues</h1>

	<div style="overflow:auto;max-height:240px" class="lastPageViewed"></div>

	<br />
	<h1>Dernieres visites via sites externes</h1>

	<div style="overflow:auto;max-height:240px">
		<table class="table">
			<?php foreach($externSources as $page): ?>
			<tr>
				<td style="padding-left:10px;text-align:left;">
					<a style="white-space:nowrap;width:400px;overflow:hidden;display:block;" href="<?php echo $page['referrer'] ?>"><?php echo htmlspecialchars($page['referrer']) ?></a>
				</td>
				<td style="width:140px">
					<?php echo profil_url($page['username'],$page['user'],$page['color']) ?>
				</td>
				<td style="width:120px" class="dateMP"><?php echo madate($page['date']) ?></td>
			</tr>			
			<?php endforeach ?>
		</table>
	</div>

	<br />
	<h1>Dernieres visites via moteurs de recherche</h1>

	<div style="overflow:auto;max-height:240px">
		<table class="table">
			<?php foreach($search as $page): ?>
			<tr>
				<td class="linkMP" style="padding-left:10px;">
					<a href="<?php echo $page['referrer'] ?>"><?php echo $page['keywords'] ?></a>
				</td>
				<td style="width:140px">
					<?php echo profil_url($page['username'],$page['user'],$page['color']) ?>
				</td>
				<td style="width:120px" class="dateMP"><?php echo madate($page['date']) ?></td>
			</tr>			
			<?php endforeach ?>
		</table>
	</div>

</div>

<script>
$('#formInterval').on('submit', loadStats);

function loadStats()
{
	$.post(baseUrl+'getVisite', $('#formInterval').serialize(), function(data)
	{
		$('.resultVisits').html(data);
	});

	return false;
}

function loadLastPages()
{
	$.get(baseUrl+'loadLastPages', function(data)
	{
		$('.lastPageViewed').html(data);
	});

	return false;
}

loadStats();
loadLastPages();

setInterval(loadStats,2000);
setInterval(loadLastPages,2000);
</script>