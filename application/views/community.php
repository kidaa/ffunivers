<div class="homeRight">

	<div class="community">
		<?php if($this->session->userdata('isAnonymous') == 1): ?>
		<h2>Connexion</h2>
		
		<form method="post" action="<?php echo site_url('connexion') ?>">
			<div class="lineForm">
				<label>Pseudonyme</label>
				<input class="txt" type="text" name="username1" />
			</div>
			
			<div class="lineForm">
				<label>Mot de passe</label>
				<input class="txt" type="password" name="password" style="width:90px" />
				<input class="ok" type="image" src="<?php echo img_url('current.png') ?>" />
			</div>
			
	<!-- 		<div class="remember">
				<input checked type="checkbox" name="remember" id="remember" /> <label for="remember">Se souvenir de moi</label>
			</div> -->
			
		
		</form>
		<br />
		
		<h2>Inscription</h2>
		
		<form method="post" action="<?php echo site_url('inscription') ?>">
			<div class="lineForm">
				<label>Pseudonyme</label>
				<input class="txt" type="text" name="username" />
			</div>
			
			<div class="lineForm">
				<label>Mot de passe</label>
				<input class="txt" type="password" name="password1" />
			</div>
			
			<div class="lineForm">
				<label>Confirmez</label>
				<input class="txt" type="password" name="password2" style="width:90px" />
				<input class="ok" type="image" src="<?php echo img_url('current.png') ?>" />
			</div>
			
		</form>
		<br />
		<?php endif; ?>

	<!-- 	<div class="headChatbox">
			<div class="current" id="roomName"></div>
			<div id="listRoom">Salons</div>	
		</div>
 -->
		<div id="showListRooms"><a class="more" target="_blank" nohref id="roomName"> <span></span><?php echo img('down.png') ?></a>
			<div id="listRooms"></div>
		</div>
		
		<div id="listMsg" <?php if(isset($onlyCB)) echo 'style="height:340px"' ?>></div>
		
		<form method="post" action="<?php echo site_url('addMessage') ?>">
			<input maxlength="255" type="text" name="message" id="addMessage" value="Votre message :)" onfocus="if(this.value=='Votre message :)') this.value=''" onblur="if(this.value=='') this.value='Votre message :)'" />
			<div class="mp">Syntaxe MP : /pseudo message</div>
		</form>
		
		<br />
		
		<div id="connected"></div>

		<div style="clear:both"></div>

		
		<script>
		<?php 
		if($this->session->userdata('currentRoom') !== false) 
		echo 'var idRoomToLoad = '.$this->session->userdata('currentRoom');
		else
		echo 'var idRoomToLoad = 1';
		?>
		</script>

		<script src="<?php echo js_url('chatbox') ?>"></script>
		
		<?php if(!isset($onlyCB)): ?>
			<br />
			<h2>Top membres</h2>
			
			<?php foreach($topGils as $key => $user): ?>
		
				<div class="lineUserTop">
					<?php echo ($key+1).'.  '.profil_url($user['username'],$user['id'],$user['color']) ?> 
					<span><?php echo number_format($user['gils'],0,',',' ') ?> gils</span>
				</div>
			<?php endforeach; ?>
			
			<br />
		

			<?php if(count($shop)!=0): ?>
			<a class="more" target="_blank" href="http://www.square-enix-boutique.com/?utm_source=site_ff13univers&utm_medium=lien_generique&utm_campaign=communaute_ff">Boutique</a>
			
			<a target="_blank" class="shop" href="<?php echo boutique_url($shop['link']) ?>">
				<img src="<?php echo $shop['image'] ?>" />
				<div><?php echo htmlspecialchars($shop['title']) ?></div>
			</a>
			
			<?php endif ?>
			
			
		<?php endif ?>
	</div>
</div>