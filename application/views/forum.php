<?php $this->load->view('mainSideMenu.php') ?>

<div class="contentCenter">

	<h1><?php echo $title ?></h1>

	<div style="overflow:auto;max-height:540px">
		<table class="table">
			<tr>
				<th style="width:30px"></th>
				<th style="width:30px"></th>
				<th>Titre du sujet</th>
				<th style="width:140px">Auteur</th>
				<th style="width:40px">Nb</th>
				<th style="width:120px">Der. msg</th>
			</tr>
			<?php foreach($topics as $topic): ?>
			<tr>
				<td class="notifMP">
					<?php if($this->input->cookie('topic_'.$topic['id']) != false && $this->input->cookie('topic_'.$topic['id']) < datetime2timestamp($topic['lastDate'])) echo img('notif.png') ?>
				</td>
				<td class="infoImg">
				<?php 
					if( $topic['blocked'] == 1 ) echo  img('blocked.png'); 
					elseif( $topic['epingled'] == 1 ) echo img('epingled.png'); 
					elseif( $topic['sondage'] == 1 ) echo  img('sondage.png'); 
					elseif( $topic['nbPost'] >= 500 ) echo  img('topic4.png'); 
					elseif( $topic['nbPost'] >= 100 ) echo  img('topic3.png'); 
					elseif( $topic['nbPost'] >= 20 ) echo  img('topic2.png'); 
					else echo  img('topic1.png'); 
				?>
				</td>
				<td class="linkMP">
					<a href="<?php echo site_url('forum/'.$topic['id'].'/'.url_title($topic['title'])) ?>"><?php echo htmlspecialchars($topic['title']) ?></a>
				</td>
				<td>
					<?php echo profil_url($topic['username'],$topic['user'],$topic['color']) ?>
				</td>
				<td><?php echo $topic['nbPost']-1 ?></td>
				<td class="dateMP"><?php echo madate($topic['lastDate']) ?></td>
			</tr>			
			<?php endforeach ?>
		</table>
	</div>
	<br />

	<h1>Nouveau sujet</h1>

	<form id="addComment" action="<?php echo site_url('addTopic') ?>" method="post">
		
		<div class="lineForm">
			<label id="title">Titre du sujet</label>
			<input type="text" class="txt" name="title" />
		</div>
		
		<div class="lineForm">
			<label>Faire un sondage ?</label>
			<select autocomplete="off" class="txt"  name="sondage">
				<option value="0" selected>Non</option>
				<option value="1">Oui</option>
			</select>
		</div>
		
		<div id="sondage" style="display:none">
			<div class="lineForm">
				<label>Réponse n°1</label>
				<input type="text" class="txt" name="response[]" />
			</div>
			
			<div class="lineForm">
				<label>Réponse n°2</label>
				<input type="text" class="txt" name="response[]" />
			</div>
			
			<div style="text-align:right;margin:5px 282px 30px 0;">
				<input type="button" class="btn2" id="plus" value="Ajouter un choix de réponse">
			</div>
		</div>
		
			<div class="lineFormTextarea">
			<label>Message</label>
			<textarea class="txt"  name="message"></textarea>
		</div>
		
		<div style="text-align:center">
			<input type="submit" value="Envoyer" class="btn" />
		</div>
		
	</form>
	<?php echo '<script>$("#addComment").prepend("<input type=\"hidden\" value=\"1\" name=\"noRobot\" />")</script>'; ?>

	<script>
	$('select[name=sondage]').on('change', function()
	{
		if( $('#sondage').css('display') == 'none')
		{
			$('#sondage').slideDown(300);
			$('#title').html('Intitulé de la question');
		}
		else
		{
			$('#sondage').slideUp(300);
			$('#title').html('Titre du sujet');
		}
	});

	var nb = 2;

	$('#plus').on('click', function()
	{
		nb++;
		$('#sondage .lineForm:last').after('<div class="lineForm"><label>Réponse n°'+nb+'</label><input type="txt" name="response[]" class="txt" /></div>');
	});

	</script>
</div>