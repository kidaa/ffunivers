<link rel="stylesheet" href="<?php echo plugin_url('estroslider/js/pe.kenburns/themes/neutral_light/skin.min.css') ?>" type="text/css" />
<script src="<?php echo plugin_url('estroslider/js/pe.kenburns/jquery.pixelentity.kenburnsSlider.min.js') ?>"></script>
<style>
.peKenBurns 
{
	width: 670px;
	height: 300px;
}
/* set border size to 8px */
.peKenBurns .peKb_slides 
{
	padding: 7px;
	/* customize rounder border (not supported in ie 9) */
	border-radius: 4px;
}

/* outer border background */
.peKenBurns .peKb_slides {
background: rgb(191,191,191);
background: -moz-linear-gradient(top, rgba(191,191,191,1) 0%, rgba(216,216,216,1) 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(191,191,191,1)), color-stop(100%,rgba(216,216,216,1)));
background: -webkit-linear-gradient(top, rgba(191,191,191,1) 0%,rgba(216,216,216,1) 100%);
background: -o-linear-gradient(top, rgba(191,191,191,1) 0%,rgba(216,216,216,1) 100%);
background: -ms-linear-gradient(top, rgba(191,191,191,1) 0%,rgba(216,216,216,1) 100%);
background: linear-gradient(to bottom, rgba(191,191,191,1) 0%,rgba(216,216,216,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bfbfbf', endColorstr='#d8d8d8',GradientType=0 );
}


/* caption font */
.peKenBurns .peKb_slides .peKb_caption h2 {
	font-family:CronosPro;
	text-shadow:1px 1px 0 #333;
	font-size: 25px;
	color: #fff;
	font-weight:normal;
	overflow:hidden;
	text-overflow:ellipsis;
	max-width:600px;
	font-style:normal;
}

/* caption padding */
.peKenBurns .peKb_slides .peKb_caption .peKb_real
{
	padding:11px;
}

/* caption margins */
.peKenBurns .peKb_slides .peKb_caption {
	margin: 0px 5px 5px 5px;
}	

/* caption background */
.peKenBurns .peKb_slides .peKb_caption .peKb_background 
{
	background-color: #000;
}
</style>
<div class="homeLeft">

	<div class="peKenBurns" data-shadow="enabled" data-controls="always">

			<?php foreach($slider as $key => $slide): ?>
			<div <?php echo $key == 0 ? 'class="peKb_active"' : '' ?> data-delay="6" data-duration="6">
				<a href="<?php echo site_url($slide['url']) ?>">
					<img src="<?php echo $slide['image'] ?>" />
				</a>
				<h2><?php echo htmlspecialchars($slide['title']) ?></h2>
			</div>
		<?php endforeach ?>
	</div>

	<script>
	$(document).ready(function()
	{
		$(".peKenBurns").peKenburnsSlider()
	});
	</script>
	
	<h1 class="h1Home">
		L'actualité
		
		<div class="follow">
			<a title="Créer une nouvelle actualité" href="<?php echo site_url('editerContenu') ?>"><?php echo img('add2.png') ?></a>
			
			<div style="float:right;position:relative;bottom:2px;">
				<div class="g-plusone" data-annotation="none" data-href="<?php echo base_url() ?>"></div>
			</div>

			<a title="Aimez notre page facebook !" target="_blank" href="http://www.facebook.com/ffunivers"><?php echo img('facebook2.png') ?></a>
			<a title="Suivez notre compte twitter !" target="_blank" href="https://twitter.com/#!/ff_univers"><?php echo img('twitter2.png') ?></a>
			<a title="Abonnez-vous à notre flux RSS !" target="_blank" href="<?php echo site_url('rss') ?>"><?php echo img('rss.png') ?></a>
			<a title="Le livestream du site" href="<?php echo site_url('live') ?>"><?php echo img('bigLive.png') ?></a>
		</div>
	</h1>
	<div class="cadreHome listNews">
		<div>
		<?php foreach($listNews as $news): ?>
			<div class="news">
				<a href="<?php echo site_url($news['url']) ?>">
					<?php echo img('catNews/'.$news['categorie'].'.png') ?>
					<span class="title"><?php echo htmlspecialchars($news['title']) ?></span>
					<span class="sub"><?php echo str_replace('&nbsp;','',character_limiter(strip_tags($news['content']),200)) ?></span>
					<span class="nbComs"><?php echo $news['nbComment'] ?></span>
				</a>
			</div>
		<?php endforeach ?>
		</div>
	</div>

	<div style="text-align:center;margin-bottom:15px">
		<script type="text/javascript"><!--
		google_ad_client = "ca-pub-5044588723656079";
		/* FF13 v4 accueil */
		google_ad_slot = "6577898920";
		google_ad_width = 468;
		google_ad_height = 60;
		//-->
		</script>
		<script type="text/javascript"
		src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
	</div>

	
	<div style="clear:both"></div>
	
	<div class="cadreHome lastUpdates">
		<h2>Dernières mises à jour</h2>
		<div>
			<ul>
			<?php foreach($lastUpdate as $update): ?>
				<li>
					<a href="<?php echo site_url($update['url']) ?>"><?php echo htmlspecialchars($update['title']) ?></a> <br />
					<span>
						<?php if($update['type'] == 1) echo 'Actualité'; ?>
						<?php if($update['type'] == 2) echo 'Rubrique'; ?>
						<?php if($update['type'] == 3) echo 'Article'; ?>
						  - par <?php echo profil_url($update['username'],$update['user'],$update['color']) ?> <?php echo madate($update['editDate']) ?>
					</span>
				</li>
			<?php endforeach ?>
			</ul>
		</div>
	</div>
	
	<div class="cadreHome lastMsg">
		<a class="more" href="<?php echo site_url('derniersMessages') ?>">
			Derniers messages
			<?php echo img('more.png') ?>
		</a>
		<div>
			<?php foreach($lastComments as $comment): ?>
				<div class="homeComment">
					<a class="title" href="<?php echo $comment['url'] ?>"><?php echo $comment['categorie'] ?> <span>></span> <?php echo htmlspecialchars($comment['title']) ?></a>
					<?php echo avatar_url($comment['user'],true) ?>
					<span><?php echo profil_url($comment['username'],$comment['user'],$comment['color']) ?> <?php echo madate($comment['date']) ?></span>
					<p><?php echo character_limiter(montextepreview($comment['message']),110) ?></p>
					<div style="clear:both"></div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
	
	<?php if(count($sondage) != 0): ?>
	<div class="cadreHome sondage">
		<a class="more" href="<?php echo site_url('forum') ?>">
			Sondage
			<?php echo img('more.png') ?>
		</a>
		<div>
			<div class="sondageQuestion"><?php echo htmlspecialchars($sondage['title']) ?></div>
			
				<?php if(!$this->input->cookie('vote_'.$sondage['id'], TRUE)): ?>
				<ul>
					<?php foreach($responses as $rep): ?>
						<li class="choose"><a href="<?php echo site_url('voteSondage/'.$sondage['id'].'/'.$rep['id'].'/1') ?>"><?php echo htmlspecialchars($rep['response']); ?></a></li>
					<?php endforeach; ?>
				</ul>
				<?php else: ?>
					<?php foreach($responses as $rep): ?>
						<div class="cadreJauge">
							<div class="jauge" style="width:<?php echo round($rep['nbVote']*100/ $total) ?>%"></div>
						</div>
						<div class="result">
							<?php echo round($rep['nbVote']*100/ $total) ?>%
							<span><?php echo htmlspecialchars($rep['response']); ?></span>
						</div>
						<div style="clear:both"></div>
					<?php endforeach; ?>
				<?php endif; ?>
				
				<a class="debattre" href="<?php echo site_url('forum/'.$sondage['id'].'/'.url_title($sondage['title'])) ?>">Discuter à propos de ce sondage</a>
		</div>
	</div>
	<?php endif; ?>
	
	<div class="cadreHome events">
		<a class="more" href="<?php echo site_url('evenements') ?>">
			Prochains évènements
			<?php echo img('more.png') ?>
		</a>
		<div>
			<ul>
			<?php foreach($events as $event): ?>
				<li>
					<a href="<?php echo site_url('evenement/'.$event['id'].'/'.url_title($event['name'])) ?>"><?php echo htmlspecialchars($event['name']) ?></a> <br />
					<span><?php echo future_date($event['date']) ?></span>
				</li>
			<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>

<?php $this->load->view('community.php'); ?>

<div style="clear:both"></div>

<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

