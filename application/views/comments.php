<?php

echo '<div id="listCommentaires">';

if(in_array($table, array('contents')))
{
	echo '<br /><h1 id="nbComment">';
		if($nbCommentaires == 0) echo 'Aucun commentaire, soyez le premier !';
		elseif($nbCommentaires == 1) echo 'Un seul commentaire';
		else echo $nbCommentaires.' commentaires';
	echo '</h1>';
}

echo '<br />';
echo $this->pagination->create_links();

foreach($commentaires as $commentaire)
{
    echo '<div idComm="'.$commentaire['id'].'" class="commentaire">';
        echo '<div class="commAvatar">'.avatar_url($commentaire['user']).'</div>';

        echo '<div class="commPseudo">'.profil_url($commentaire['username'],$commentaire['user'],$commentaire['color']);
			echo ' <span class="commTime">'.madate($commentaire['date']).'</span>';
		
			echo '<div class="commActions">';
				if($this->session->userdata('group') >= 2)
				echo '<a class="commSuppr" href="'.site_url('commSuppr/'.$commentaire['id']).'">'.img('mini_delete.png').' Supprimer</a>';
				
				if($commentaire['user'] == $this->session->userdata('id'))
				echo '<a class="commEdit" href="'.site_url('editComm/'.$commentaire['id']).'">'.img('editComm.png').' Edition</a>';
			echo '</div>';
		echo '</div>';
		
        echo '<p>'.montexte($commentaire['message']).'</p>';
    echo '</div>';
}

echo $this->pagination->create_links();

	echo '<script>';
		echo 'url="'.$url.'";';    
		echo 'page='.$page.';';    
		echo 'table="'.$table.'";';
		echo 'idData='.$idData.';';
		echo 'nbMessageParPages='.$nbMessageParPages.';';
		echo 'nbCommentaires='.$nbCommentaires.';';
	echo '</script>';

echo '</div>';


if($this->session->userdata('id') && $isAjaxRequest == false && $blocked == false)
{
    echo '<br /><br /><h1 id="addMsg">Ajouter un message</h1>';

    echo '<form id="addComment" method="post" action="'.site_url('addComment').'">';

    echo '<div class="lineComment">
				<div style="float:left;">
					<div class="avatar">'.avatar_url($this->session->userdata('id')).'</div>
					
					<div class="bbcode">
						 <img title="Gras" alt="g" src="'.img_url('bold.png').'" />
						 <img title="Italique" alt="i" src="'.img_url('italic.png').'" />
						 <img title="Souligné" alt="s" src="'.img_url('underline.png').'" />
						 <img title="Spoiler" alt="spoil" src="'.img_url('spoiler.png').'" />
						 <img title="Image" alt="img" src="'.img_url('image.png').'" />
					</div>
				</div>
                <textarea id="message" name="message" class="txt"></textarea>
				<div class="smileys">
					 <img alt=":)" title=":)"  src="http://image.jeuxvideo.com/smileys_img/1.gif" />
					 <img alt=":-)" title=":-)"  src="http://image.jeuxvideo.com/smileys_img/46.gif" />
					 <img alt=":-)))" title=":-)))"  src="http://image.jeuxvideo.com/smileys_img/23.gif" />
					 <img alt=":content:" title=":content:"  src="http://image.jeuxvideo.com/smileys_img/24.gif" />
					 <img alt=":rire:" title=":rire:"  src="http://image.jeuxvideo.com/smileys_img/39.gif" />
					 <img alt=":-D" title=":-D"  src="http://image.jeuxvideo.com/smileys_img/40.gif" />
					 <img alt=":o))" title=":o))"  src="http://image.jeuxvideo.com/smileys_img/12.gif" />
					 <img alt=":snif:" title=":snif:"  src="http://image.jeuxvideo.com/smileys_img/20.gif" />
					 <img alt=":hum:" title=":hum:"  src="http://image.jeuxvideo.com/smileys_img/68.gif" />
					 <img alt=":noel:" title=":noel:"  src="http://image.jeuxvideo.com/smileys_img/11.gif" />
					 <img alt=":-p" title=":-p"  src="http://image.jeuxvideo.com/smileys_img/31.gif" />
					 <img alt=":doute:" title=":doute:"  src="http://image.jeuxvideo.com/smileys_img/28.gif" />
					 <img alt=":oui:" title=":oui:"  src="http://image.jeuxvideo.com/smileys_img/37.gif" />
					 <img alt=":non:" title=":non:"  src="http://image.jeuxvideo.com/smileys_img/35.gif" />
					 <img alt=":bave:" title=":bave:"  src="http://image.jeuxvideo.com/smileys_img/71.gif" />
					 <img alt=":cool:" title=":cool:"  src="http://image.jeuxvideo.com/smileys_img/26.gif" />
					 <img alt=":p)" title=":p)"  src="http://image.jeuxvideo.com/smileys_img/7.gif" />
					 <img alt=":(" title=":("  src="http://image.jeuxvideo.com/smileys_img/45.gif" />
					 <img alt=":-(" title=":-("  src="http://image.jeuxvideo.com/smileys_img/14.gif" />
					 <img alt=":-((" title=":-(("  src="http://image.jeuxvideo.com/smileys_img/15.gif" />
					 <img alt=":fou:" title=":fou:"  src="http://image.jeuxvideo.com/smileys_img/50.gif" />
					 <img alt=":fier:" title=":fier:"  src="http://image.jeuxvideo.com/smileys_img/53.gif" />
					 <img alt=":malade:" title=":malade:"  src="http://image.jeuxvideo.com/smileys_img/8.gif" />
					 <img alt=":cute:" title=":cute:"  src="http://image.jeuxvideo.com/smileys_img/nyu.gif" />
					 <img alt=":rouge:" title=":rouge:"  src="http://image.jeuxvideo.com/smileys_img/55.gif" />
					 <img alt=":nah:" title=":nah:"  src="http://image.jeuxvideo.com/smileys_img/19.gif" />
					 <img alt=":mort:" title=":mort:"  src="http://image.jeuxvideo.com/smileys_img/21.gif" />
					 <img alt=":ouch:" title=":ouch:"  src="http://image.jeuxvideo.com/smileys_img/22.gif" />
					 <img alt=":ouch2:" title=":ouch2:"  src="http://image.jeuxvideo.com/smileys_img/57.gif" />
					 <img alt=":peur:" title=":peur:"  src="http://image.jeuxvideo.com/smileys_img/47.gif" />
					 <img alt=":non2:" title=":non2:"  src="http://image.jeuxvideo.com/smileys_img/33.gif" />
					 <img alt=":hap:" title=":hap:"  src="http://image.jeuxvideo.com/smileys_img/18.gif" />
					 <img alt=":sarcastic:" title=":sarcastic:"  src="http://image.jeuxvideo.com/smileys_img/43.gif" />
					 <img alt=":pf:" title=":pf:"  src="http://image.jeuxvideo.com/smileys_img/pf.gif" />
					 <img alt=":monoeil:" title=":monoeil:"  src="http://image.jeuxvideo.com/smileys_img/34.gif" />
					 <img alt=":honte:" title=":honte:"  src="http://image.jeuxvideo.com/smileys_img/30.gif" />
				</div>
        </div>';

    echo '<input type="hidden" name="table" value="'.$table.'">';
    echo '<input type="hidden" name="idData" value="'.$idData.'">';

    echo '

        <div style="text-align:center;clear:both">
                <input type="submit" id="valid" value="Envoyer le message" class="btn" />
        </div>';

    echo '</form>';

    echo '<script>$("#addComment").prepend("<input type=\"hidden\" value=\"1\" name=\"noRobot\" />")</script>';
}
?>