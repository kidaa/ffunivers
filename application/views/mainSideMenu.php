<div class="sideMenuLeft">
	<div class="menu">
	<ul class="classic mainSide">
		<li><a style="background-image:url(<?php echo img_url('user.png') ?>);" <?php echo ($current == 'account' ? 'class="current"' : ''); ?> href="<?php echo site_url('monCompte') ?>">Mon compte</a>
			<?php if($current == 'account'): ?>
				<ul>
					<li><a <?php echo ($categorie == 'home' ? 'class="current"' : ''); ?> href="<?php echo site_url('monCompte') ?>">Mon profil</a></li>
					
					<li><a <?php echo ($categorie == 'messagerie' || $categorie == 'viewMessaging' ? 'class="current"' : ''); ?> href="<?php echo site_url('monCompte/messagerie') ?>">Ma messagerie</a></li>
					
					<li><a <?php echo ($categorie == 'images' || $categorie == 'trierImages' || $categorie == 'ajouterImages' ? 'class="current"' : ''); ?> href="<?php echo site_url('monCompte/images') ?>">Mes images</a></li>
					
					<li><a <?php echo ($categorie == 'contributions' ? 'class="current"' : ''); ?> href="<?php echo site_url('monCompte/contributions') ?>">Mes contributions</a></li>
				
				</ul>
			<?php endif; ?>
		</li>
		<li><a style="background-image:url(<?php echo img_url('members.png') ?>);" <?php echo (in_array($current, array('listMembers','mapMembers')) ? 'class="current"' : ''); ?> href="<?php echo site_url('listeDesMembres/1/0') ?>">Les membres</a></li>

		<?php if(in_array($current, array('listMembers','mapMembers'))): ?>
				<ul>
					<li><a <?php echo ($current == 'listMembers' ? 'class="current"' : ''); ?> href="<?php echo site_url('listeDesMembres/1/0') ?>">Liste des membres</a></li>
					<li><a <?php echo ($current == 'mapMembers' ? 'class="current"' : ''); ?> href="<?php echo site_url('carteDesMembres') ?>">Carte des membres</a></li>
				</ul>
			<?php endif; ?>

		
		<li><a style="background-image:url(<?php echo img_url('forum.png') ?>);" <?php echo (in_array($current, array('forum','mySubjects')) ? 'class="current"' : ''); ?> href="<?php echo site_url('forum') ?>">Forum de discussion</a></li>
		<?php if(in_array($current, array('forum','mySubjects'))): ?>
				<ul>
					<li><a <?php echo ($current == 'forum' ? 'class="current"' : ''); ?> href="<?php echo site_url('forum') ?>">Tous les sujets</a></li>
					<li><a <?php echo ($current == 'mySubjects' ? 'class="current"' : ''); ?> href="<?php echo site_url('mesSujets') ?>">Mes sujets</a></li>
				</ul>
			<?php endif; ?>


		<li><a style="background-image:url(<?php echo img_url('newSalon.png') ?>);" <?php echo ($current == 'historyCB' ? 'class="current"' : ''); ?> href="<?php echo site_url('historiqueChatbox') ?>">Historique de la chatbox</a></li>
		<li><a style="background-image:url(<?php echo img_url('lastMsg.png') ?>);" <?php echo ($current == 'lastMsg' ? 'class="current"' : ''); ?> href="<?php echo site_url('derniersMessages') ?>">Derniers messages</a></li>
		<li><a style="background-image:url(<?php echo img_url('events.png') ?>);" <?php echo ($current == 'listEvent' ? 'class="current"' : ''); ?> href="<?php echo site_url('evenements') ?>">Evènements</a></li>
		<li><a style="background-image:url(<?php echo img_url('news.png') ?>);" <?php echo ($current == 'historyNews' ? 'class="current"' : ''); ?> href="<?php echo site_url('archivesNews') ?>">Archives des actualités</a></li>
		<li><a style="background-image:url(<?php echo img_url('search.png') ?>);" <?php echo ($current == 'search' ? 'class="current"' : ''); ?> href="<?php echo site_url('rechercher') ?>">Rechercher sur le site</a></li>
	</ul>
	</div>
	
	<div class="pub">
		<script type="text/javascript"><!--
		google_ad_client = "ca-pub-5044588723656079";
		/* FF13v4 */
		google_ad_slot = "7319975990";
		google_ad_width = 200;
		google_ad_height = 200;
		//-->
		</script>
		<script type="text/javascript"
		src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
	</div>
</div>