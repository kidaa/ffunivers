<div class="sideMenuLeft">
	<div class="menu">
		<ul class="classic mainSide">
			<li><a style="background-image:url(<?php echo img_url('addEvent.png') ?>);" id="addEvent" href="<?php echo site_url('ajouterEvenement') ?>">Proposer un évènement</a></li>
			<li><a style="background-image:url(<?php echo img_url('events.png') ?>);" href="<?php echo site_url('evenements') ?>">Liste des évènements</a></li>
		</ul>
		<br />
		<h4>Prochains évènements</h4>
		<div class="listSide">
			<ul> 
				<?php foreach($events as $event): ?>
					<li>
						<a href="<?php echo site_url('evenement/'.$event['id'].'/'.url_title($event['name'])) ?>"><?php echo htmlspecialchars($event['name']) ?></a>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>

	</div>
	
	<div class="pub">
		<script type="text/javascript"><!--
		google_ad_client = "ca-pub-5044588723656079";
		/* FF13v4 */
		google_ad_slot = "7319975990";
		google_ad_width = 200;
		google_ad_height = 200;
		//-->
		</script>
		<script type="text/javascript"
		src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
	</div>
	
</div>

<div class="contentCenter">
	<h1><?php echo htmlspecialchars($title) ?></h1>
	
	<div class="infosContent">
		Evènement ajouté par <?php echo profil_url($infos['username'], $infos['user'], $infos['color']); ?>
	</div>
	
	<?php if(isset($commentaires)) : echo $commentaires; ?>
	<script src="<?php echo js_url('comments') ?>"></script>
	<?php endif; ?>
</div>

<script>
$('#addEvent').on('click', function()
{
	var name = prompt('Nom de l\'évènement');
	if(name == null) return false;
	
	var date = prompt('Date de l\'évènement au format (JJ/MM/AA HH:MM)', '<?php echo date('d/m/y H:i') ?>');
	if(date == null) return false;
	
	$.post(baseUrl+'addEvent',
	{
		name:name,
		date:date
	}, function()
	{
		alert('Votre évènement a bien été proposé, merci =D');
	})
	
	return false;
});
</script>