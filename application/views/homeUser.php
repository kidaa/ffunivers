<?php $this->load->view('mainSideMenu.php') ?>


<div class="contentCenter">
	<h1><?php echo $title ?></h1>

	<?php if($nbImageToIndexed > 0): ?>
		<div class="warning">
			<div></div>
			<p>
				Vous avez actuellement <strong><?php echo $nbImageToIndexed ?> image<?php echo ($nbImageToIndexed > 1 ? 's</strong> qui ne sont pas encore triées. Vous devriez les trier rapidement afin de vous assurer qu\'elles ne soient pas supprimées, et qu\'elles puissent être affichées dans la galerie si les administrateurs l\'éstiment nécessaire.':'</strong> qui n\'est pas encore triée. Vous devriez la trier rapidement afin de vous assurer qu\'elle ne soit pas supprimée, et qu\'elle puisse être affichée dans la galerie si les administrateurs l\'éstiment nécessaire.') ?></strong>  <br />
				<a href="<?php echo site_url('monCompte/trierImages') ?>">Cliquez juste ici pour trier vos images.</a> Un bonus de <a href="<?php echo site_url('monCompte/gils') ?>">gils</a> viendra récompenser votre labeur.
			</p>
		</div>
	<?php endif; ?>
	
	
	
	<?php if($categorie == 'home'): ?>
	
		<?php if($this->session->userdata('isAnonymous') == 1): ?>
			<div class="question">
				<div></div>
				<p>
					Vous êtes actuellement connecté à un compte membre anonyme. Un pseudonyme aléatoire vous a été automatiquement attribué afin que vous puissiez participer à la vie du site plus rapidement. Le site fera tout son possible à l'avenir pour essayer de vous connecter à ce même compte automatiquement, afin que vous retrouviez vos préférences et vos contributions. Cependant, votre adresse IP risque de changer, et votre compte sera peut-être perdu, c'est pourquoi nous vous conseillons vivement de vous inscrire officiellement, afin de choisir un pseudonyme un peu plus joli, et être sûr d'avoir toujours votre compte à disposition.
				</p>
			</div>
		<?php endif; ?>
		
		
		<div class="cadreParam" style="height:110px">
		<h2>Mon avatar</h2>
		
		<form class="noValid" method="post" action="<?php echo site_url('monCompte/uploadAvatar') ?>" enctype="multipart/form-data">
			<?php echo avatar_url( $this->session->userdata('id'), false, 'float:left;margin-right:12px') ?>
			<input type="file" name="avatar" /><br />
			
			<input style="margin-top:7px" type="submit" class="btn2" />
		</form>
	</div>
	
	
		<form method="post" action="<?php echo site_url('monCompte/updateProfil') ?>" >
		<div class="cadreParam">
			<h2>Mon profil</h2>
			

			<div class="lineForm">
				<label>Adresse e-mail</label>
				<input class="txt" type="text" name="mail" value="<?php echo htmlspecialchars($infos['mail']) ?>" />
			</div>
			
			<div class="lineForm">
				<label>Naissance (jj/mm/aaaa)</label>
				<input class="txt" type="text" name="birth" value="<?php echo ($infos['birth'] != '0000-00-00' ? date('d/m/Y', date2timestamp($infos['birth'])) : '') ?>" />
			</div>
			
			<div class="lineForm">
				<label>Vous êtes</label>
				<select class="txt" name="sexe">
					<option <?php echo $infos['sexe'] == 0 ? 'selected' : ''; ?> value="0">Un homme</option>
					<option style="color:#ff1493" <?php echo $infos['sexe'] == 1 ? 'selected' : ''; ?> value="1">Une femme</option>
				</select>
			</div>
			
				
			<div class="lineForm">
				<label>Site internet / blog</label>
				<input class="txt" type="text" name="urlWeb" value="<?php echo htmlspecialchars($infos['urlWeb']) ?>" />
			</div>
			
			<div class="lineForm">
				<label>Compte PSN</label>
				<input class="txt" type="text" name="idPSN" value="<?php echo htmlspecialchars($infos['idPSN']) ?>" />
			</div>
			
			<div class="lineForm">
				<label>Compte Xbox Live</label>
				<input class="txt" type="text" name="idXboxlive" value="<?php echo htmlspecialchars($infos['idXboxlive']) ?>" />
			</div>
			
			<div class="lineFormTextarea">
				<label>Description</label>
				<textarea class="txt" name="description"><?php echo htmlspecialchars($infos['description']) ?></textarea>
			</div>
			
		</div>
		
		<div class="cadreParam">
			<h2>Mes informations confidentielles</h2>
			

			<div class="lineForm">
				<label>Adresse</label>
				<input class="txt" type="text" name="address" value="<?php echo htmlspecialchars($infos['address']) ?>" />
			</div>
			
			<div class="lineForm">
				<label>Code postal</label>
				<input class="txt" type="text" name="postal" value="<?php echo htmlspecialchars($infos['postal']) ?>" />
			</div>
			
			<div class="lineForm">
				<label>Ville</label>
				<input class="txt" type="text" name="city" value="<?php echo htmlspecialchars($infos['city']) ?>" />
			</div>
			
			<div class="lineForm">
				<label>Pays</label>
				<input class="txt" type="text" name="country" value="<?php echo htmlspecialchars($infos['country']) ?>" />
			</div>
			
			<div class="lineForm">
				<label>Nom de famille</label>
				<input class="txt" type="text" name="lastName" value="<?php echo htmlspecialchars($infos['lastName']) ?>" />
			</div>
			
			<div class="lineForm">
				<label>Prénom</label>
				<input class="txt" type="text" name="firstName" value="<?php echo htmlspecialchars($infos['firstName']) ?>" />
			</div>
		</div>
		
		<div class="cadreParam">
			<h2>Mes options</h2>
			
			<div class="lineForm">
				<label>M'afficher sur <a style="color:firebrick" href="<?php echo site_url('carteDesMembres') ?>">la carte</a></label>
				
				<select class="txt" name="onMap">
					<option <?php echo $infos['onMap'] == 0 ? 'selected' : ''; ?> value="0">Non</option>
					<option <?php echo $infos['onMap'] == 1 ? 'selected' : ''; ?> value="1">Oui</option>
				</select>
			</div>
			<!--
			<div class="lineForm">
				<label>Notification MP par mail</label>
				
				<select class="txt" name="mailNotif">
					<option <?php echo $infos['mailNotif'] == 0 ? 'selected' : ''; ?> value="0">Non</option>
					<option <?php echo $infos['mailNotif'] == 1 ? 'selected' : ''; ?> value="1">Oui</option>
				</select>
			</div>
			-->
			
			<div class="lineForm">
				<label>Couleur du pseudo</label>
				
				<select class="txt" name="color">
					<?php foreach($colors as $name => $color): ?>
						<option <?php echo $infos['color'] == $color ? 'selected' : ''; ?> style="color:<?php echo $color ?>;font-weight:bold" value="<?php echo $color ?>"><?php echo $name ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		
		<div style="margin-bottom:0;" class="cadreParam">
			<h2>Modifier le mot de passe</h2>
			
			<div style="text-align:center;color:red">
				Laissez ces deux champs vides pour ne rien modifier.<br /><br />
			</div>
			
			<div class="lineForm">
				<label>Nouveau mot de passe</label>
				<input class="txt" type="password" name="mdp1" value="" autocomplete="off" />
			</div>
			
			<div class="lineForm">
				<label>Retapez le mot de passe</label>
				<input class="txt" type="password" name="mdp2" value="" autocomplete="off" />
			</div>
		</div>
		
			
		<div style="text-align:center">
			<input type="submit" class="btn" value="Valider les modifications" />
		</div>
		</form>
		
		
		
	<?php endif; ?>
	
	
	
	
	
	
	
	
	<?php if($categorie == 'viewMessaging'): ?>
	
		<div class="infosContent" style="font-size:15px;">
			Participants : 
			
			<?php foreach($usersInConvers as $user): ?>

				<?php if($user['notif'] == 1): ?>
					<span style="text-decoration:underline"><?php echo profil_url($user['username'],$user['user'],$user['color']) ?></span>
				<?php else: ?>
					<?php echo profil_url($user['username'],$user['user'],$user['color']) ?>
				<?php endif; ?>
			
			<?php endforeach ?>
		</div>
		
		<form method="post" action="<?php echo site_url('addToConvers') ?>">
			<div class="lineForm">
				<label style="width:300px">Ajouter des personnes à la conversation</label>
				<input style="width:320px" type="text" class="txt" name="usernames" />
				<input type="hidden" name="id" value="<?php echo $idConvers ?>" />
				<input type="submit" class="btn2" value="Ok" />
			</div>
		</form>
		
		<?php if(isset($commentaires)) : echo $commentaires; ?>
		<script src="<?php echo js_url('comments') ?>"></script>
		<?php endif; ?>
	<?php endif; ?>
	
	
	<?php if($categorie == 'messagerie'): ?>
	
	
		<table class="table">
			<tr>
				<th style="width:30px"></th>
				<th style="width:30px"></th>
				<th>Titre</th>
				<th style="width:160px">Participants</th>
				<th style="width:150px">Dernier msg</th>
			</tr>
			<?php foreach($myConvers as $convers): ?>
			<tr>
				<td class="notifMP"><?php echo $convers['notif'] == 1 ? img('notif.png') : ''; ?></td>
				<td class="deleteMP"><a href="<?php echo site_url('monCompte/deleteConvers/'.$convers['id']) ?>"><?php echo img('mini_delete.png') ?></a></td>
				<td class="linkMP"><a href="<?php echo site_url('messagerie-'.$convers['id']) ?>"><?php echo htmlspecialchars($convers['title']) ?></a></td>
				<td>
					<?php foreach($usernamesMyConvers as $user): ?>
						<?php if($convers['id'] != $user['messaging']) continue; ?>
						
						<?php if($user['notif'] == 1): ?>
							<span style="text-decoration:underline"><?php echo profil_url($user['username'],$user['user'],$user['color']) ?></span>
						<?php else: ?>
							<?php echo profil_url($user['username'],$user['user'],$user['color']) ?>
						<?php endif; ?>
					<?php endforeach; ?>
				</td>
				<td class="dateMP"><?php echo madate($convers['lastDate']) ?></td>
			</tr>			
			<?php endforeach ?>
		</table>
		
		<br />
		
		<p style="color:#666;font-size:14px;text-align:center;">Les pseudonymes soulignés n'ont pas encore lu les derniers messages.</p>
		
		<script>
		$('.deleteMP a').on('click', function()
		{
			if(!confirm('Etes-vous sur de vouloir quitter cette conversation ?'))
			return false;
			
			var source = $(this);
			
			$.get(source.attr('href'), function()
			{
				source.parent().parent('tr').fadeOut();
			});
			
			return false;
		});
		</script>
		
		<br />
		<br />
		<br />
	
	
	
	
	
		<h1 id="new_convers">Nouvelle conversation</h1>
		
		<form method="post" action="<?php echo site_url('monCompte/newConvers') ?>">
			
			<div class="lineForm">
				<label>Participants</label>
				<input type="text" class="txt" name="usernames" value="<?php echo trim($mpTo) == '' ? 'Chaque pseudo séparé par une virgule' : $mpTo ?>" onfocus="if(this.value=='Chaque pseudo séparé par une virgule') this.value=''" />
			</div>
			
			<div class="lineForm">
				<label>Titre de la conversation</label>
				<input maxlength="80" type="text" class="txt" name="title" />
			</div>
			
			<div class="lineFormTextarea">
				<label>Votre message</label>
				<textarea class="txt" name="message"></textarea>
			</div>
			
			<div style="text-align:center">
				<input type="submit" class="btn" value="Créer la conversation" />
			</div>
		</form>
	<?php endif; ?>
	
	
	
	
	
	
	
	
	
	
	
	
	<?php if($categorie == 'contributions'): ?>
			<div class="question">
				<div></div>
				<p>
					Vous trouverez ci-dessous la liste des pages ou vous avez apportez une contribution, trié par date d'édition. Chacune de vos modifications créent une nouvelle version de la page, le site lui attribue alors un numéro. <br /> Au survol de cette liste, plusieurs informations sont affichés, dont cette fameuse version ainsi que la version à partir de laquelle la page a été edité. Ainsi dans "v1 > v3", v3 indique le numero de la version actuelle, et v1 la version d'origine. <br />
					Différents boutons sont à votre disposition. Vous avez la possibilité d'éditer vos pages à partir de n'importe qu'elle version, et de supprimer vos versions.
				</p>
			</div>
			
		<div style="height:70px">
			<a class="add" href="<?php echo site_url('editerContenu') ?>">
				<?php echo img('new.png') ?>
				Ajouter du contenu
			</a>
		</div>
		
		<div class="lineForm">
			<label style="width:150px">Filtrer par type </label>
			
			
			<select style="width:200px" class="txt" id="type">
				<option value="*">Tout voir</option>
				<?php foreach($type_contents as $type): ?>
				<option value="<?php echo $type['id'] ?>"><?php echo $type['name'] ?> </option>
				<?php endforeach; ?>
			</select>
		</div>
		
		<div class="lineForm">
			<label style="width:150px">Filtrer par titre </label>
			
			<input style="width:250px;" type="text" id="title" class="txt" />
		</div>
		
		<div class="myContents">
			<?php foreach($myContents as $content): ?>
				<div class="myContent" type="<?php echo $content['type'] ?>">
				
				
				
					<span class="tiny">[<?php echo $content['name'] ?>]</span> 
					
					<span class="title"><?php echo htmlspecialchars($content['headTitle']) ?></span> 
					<div class="history">
															
						<?php foreach($content['history'] as $key => $history): ?>
							<div class="lineHistory">
							
								<a class="action" href="<?php echo site_url('historique/'.$history['id']) ?>"><?php echo img('view.png') ?></a>
								<a class="action" href="<?php echo site_url('editerContenu/'.$history['id']) ?>"><?php echo img('edit.png') ?></a>
								
								<?php if($this->session->userdata('id') == $history['user']): ?>
									<a class="action deleteMyContent" href="<?php echo site_url('deleteMyContenu/'.$history['id']) ?>"><?php echo img('delete.png') ?></a>
								<?php else: ?>
									<div class="action"><?php echo img('delete2.png') ?></div>
								<?php endif; ?>
								
								<?php if($history['isCurrent'] == 1): ?>
									<div class="action"><?php echo img('current.png') ?></div>
								<?php endif; ?>
								
								v<?php echo $history['editFromVersion'] ?> <span class="tiny">></span> v<?php echo $history['version'] ?>
								
													
								<span class="tiny" style="float:right">
									<?php echo profil_url($history['username'],$history['user'],$history['color'] ) ?>
									<?php echo madate($history['editDate']) ?> 
									<?php echo colorNb($history['linesModified'],'(',' ligne'.($history['linesModified'] > 1 ? 's' : '').' | '); ?>
									<?php echo colorNb($history['sizeModified'],'',' octet'.($history['sizeModified'] > 1 ? 's' : '').')'); ?>
								</span>
								
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		
		<script>

		
		// filtres
		$('#type').on('change', filtres)
		$('#title').on('keyup', filtres)
		$('#title').on('click', filtres)

		function filtres()
		{
			var filtreType = $('#type').val();
			var filtreTitle = $('#title').val();
			
			var lines = $('.myContent');
			
			
			$('.myContent').each(function()
			{
				var reg = new RegExp(filtreTitle, "gi");
				var currentTitle =  $(this).children('.title').html();
				var currentType =  $(this).attr('type');

				if(filtreType == '*')
				{
					if(currentTitle.match(reg))
					$(this).show();
					else
					$(this).hide();
				}
				else
				{
					if(currentType == filtreType && currentTitle.match(reg))
					$(this).show();
					else
					$(this).hide();
				}
			});
		}
		
		
		// suppression d'une version
		$('.deleteMyContent').on('click', function()
		{
			if(!confirm("Voulez-vous vraiment supprimer cette version ?")) 
			return false;
			
			var source = $(this);
			$.get( source.attr('href'), function(data)
			{
				source.parent('div.lineHistory').remove();
			});
			
			return false;
		});
		
		//effets hover
		$('.myContent').on('mouseenter', function()
		{
			$(this).children('div.history').stop(true,true).slideDown(200);
		});
		
		$('.myContent').on('mouseleave', function()
		{
			$(this).children('div.history').hide();
		});
		</script>
		
	<?php endif; ?>
	
	
	
	
	
	
	
	
	
	<?php if($categorie == 'images'): ?>
		<div style="height:70px">
			<a class="add" href="<?php echo site_url('monCompte/ajouterImages') ?>">
				<?php echo img('new.png') ?>
				Ajouter des images
			</a>
			<a class="add" href="<?php echo site_url('monCompte/trierImages') ?>">
				<?php echo img('sortImages.png') ?>
				Trier mes images
			</a>
		</div>
		
		
		<div class="listImagesUser allImages">
			<?php foreach($myImages as $img): ?>
				<?php $class=''; if($img['indexed'] == 0) $class='notIndexed'; ?>
				<a onclick="return hs.expand(this)" target="_blank" href="<?php echo site_url('public/images_upload/big/'.$img['name']) ?>">
					<img class="<?php echo $class ?>" src="<?php echo site_url('public/images_upload/mini/'.$img['name']) ?>" />
				</a>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
	
	
	
	
	<?php if($categorie == 'ajouterImages'): ?>
		<div class="question">
			<div></div>
			<p>
				Vous avez la possibilité d'ajouter / d'héberger des images via ce formulaire. Par la suite, si vous triez correctement vos images dans la bonne catégorie, elles auront peut-être la chance d'être publié dans la galerie du site. Bien entendu, <a href="<?php echo site_url('monCompte/gils') ?>">des gils sont à la clef</a> ! <br />
				Pour ajouter plusieurs images, cliquez simplement sur le bouton "Ajouter une autre image". 
			</p>
		</div>

		<div class="warning">
			<div></div>
			<p>
				Avant d'ajouter des images :<br />
				- Vérifiez qu'elle n'est pas déjà dans <a href="<?php echo site_url('images') ?>">la galerie</a> <br />
				- Vérifiez qu'elle est de bonne qualité, et assez grande si c'est un fond d'écran <br />
				- Si votre image contient des bandes noires, découpez-là (Paint fera l'affaire) <br />
				- Enfin, vérifiez qu'elle ne contient aucun texte, logo, ou nom de sites concurrents
			</p>
		</div>

		<form class="noValid" action="<?php echo site_url('uploadImages') ?>" method="post" enctype="multipart/form-data">
			<div class="lineForm">
				<label style="width:140px">Image n°1</label>
				<input type="file" name="files[]" /> ou
				<input style="width:210px" name="urlImg[]" type="text" value="Adresse internet d'une image" class="txt" onfocus="if(this.value='Adresse internet d\'une image') this.value=''; " />
				
			</div>
			<input type="button" class="btn2" id="plus" value="Ajouter une autre image">
			
			<div style="text-align:center">
				<input class="btn" type="submit" value="Uploader ces images" />
			</div>
		</form>


		<script>
		var nb = 1;

		$('#plus').on('click', function()
		{
			if(nb >= 5)
			{
				alert('Doucement ! Pas trop d\'images d\'un coup non plus !');
				return false;
			}
			
			nb++;
			$('.lineForm:last').after('<div class="lineForm"><label style="width:140px">Image n°'+nb+'</label><input type="file" name="files[]" /> ou <input style="width:210px" name="urlImg[]" type="text" class="txt"  value="Adresse internet d\'une image" class="txt" onfocus="if(this.value=\'Adresse internet d\\\'une image\') this.value=\'\'; "/></div>');
		});

		</script>
	<?php endif; ?>
	
	
	
	
	
	<?php if($categorie == 'trierImages'): ?>
		
		<?php foreach($myImages as $img): ?>
			<?php $class=''; if($img['indexed'] == 0) $class='notIndexed'; ?>
			<div id="divIndex_<?php echo $img['id'] ?>" class="<?php echo $class ?> allImages sortImagesUser">
				<a onclick="return hs.expand(this)" target="_blank" href="<?php echo site_url('public/images_upload/big/'.$img['name']) ?>">
					<img id="imgIndex_<?php echo $img['id'] ?>" class="<?php echo $class ?>" src="<?php echo site_url('public/images_upload/mini/'.$img['name']) ?>" />
				</a>
				<form noLoading method="post" action="<?php echo site_url('indexImage') ?>">
					<input type="hidden" value="<?php echo $img['id'] ?>" name="id" />
					<div class="lineForm">
						<label>Catégorie</label>
						<select class="txt" name="categorie">
							<option value="0">Aucune</option>
							<?php foreach($categories as $cat):	
							if($cat['content_type'] != 4) continue; 
							$selected=''; if($cat['id'] == $img['categorie']) $selected = 'selected'; ?>	
								<option <?php echo $selected ?> value="<?php echo $cat['id'] ?>"><?php echo $cat['name'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
					
					<div class="lineForm">
						<label>Type d'images</label>
						<select class="txt" name="categorieType">
							<option value="0">Aucune</option>
							<?php foreach($categories as $cat):	
							if($cat['content_type'] != 5) continue; 
							$selected=''; if($cat['id'] == $img['categorieType']) $selected = 'selected'; ?>	
								<option <?php echo $selected ?> value="<?php echo $cat['id'] ?>"><?php echo $cat['name'] ?></option>
							<?php endforeach ?>
						</select>
					</div>

					
					<input type="submit" value="Ok" class="btn" />
					
				</form>
				<a title="Supprimer cette image" class="refuse" href="<?php echo site_url('deleteMyImage/'.$img['id']) ?>"><?php echo img('delete.png') ?></a>
			</div>
		<?php endforeach; ?>
		
		<script>
		$('.refuse').on('click', function()
		{
			var source = $(this);
			$.get( source.attr('href'), function(data)
			{
				if(data != '')
				alert(data);
				else
				source.parent('.sortImagesUser').slideUp();
			});
			
			return false;
		});	
		
		$('.sortImagesUser form').on('submit', function()
		{
			var id = $(this).children('input[name=id]').val();
			$('#imgIndex_'+id).removeClass('notIndexed');
			$('#divIndex_'+id).removeClass('notIndexed');
		});
		</script>
	<?php endif; ?>
	
	
	
	<?php if($categorie == 'gils'): ?>
	
		<div class="question">
			<div></div>
			<p>
				L'utilisation des gils est assez variable. Ils permettent avant tout d'itentifier les membres les plus actifs, afin que leurs profils soient mis en avant. Les gils peuvent également servir à acceder à des concours VIP, ou à augmenter vos chances de gagner sur un concours classique... Leur utilisation peut varié selon le temps. <br />
				
				Pour gagner des gils, il vous suffit de participer à la vie du site. Voici un récapitulatif des gains : <br />
			</p>
		</div>
		
		<table class="gils">
			<tr>
				<th>Action à réaliser</th>
				<th>Gain en gils</th>
			</tr>

			<tr>
				<td><a style="color:green" href="<?php echo site_url('dons') ?>">Code audiotel</a></td>
				<td>5 000 gils</td>
			</tr>
			<tr>
				<td>Poster un message dans le tchat</td>
				<td>0,1 gils</td>
			</tr>
			<tr>
				<td>Remplir son profil et ses informations</td>
				<td>100 gils</td>
			</tr>
			<tr>
				<td>Poster un commentaire sur une actualité / un article</td>
				<td>10 gils</td>
			</tr>
			<tr>
				<td>Créer un sujet sur le forum</td>
				<td>15 gils</td>
			</tr>
			<tr>
				<td>Créer un sondage sur le forum</td>
				<td>20 gils</td>
			</tr>
			<tr>
				<td>Répondre à un sondage sur le forum</td>
				<td>2 gils</td>
			</tr>
			<tr>
				<td>Poster un message sur le forum</td>
				<td>5 gils</td>
			</tr>
			<tr>
				<td>Envoyer un message privée (la triche est detectée)</td>
				<td>2 gils</td>
			</tr>
			<tr>
				<td>Ajouter / editer une actualité / un article / une rubrique</td>
				<td>150 gils</td>
			</tr>
			<tr>
				<td>Avoir du contenu validé par un administrateur</td>
				<td>500 gils</td>
			</tr>
			<tr>
				<td>Ajouter une image au site</td>
				<td>5 gils</td>
			</tr>
			<tr>
				<td>Trier correctement une image</td>
				<td>10 gils</td>
			</tr>
			<tr>
				<td>Avoir une image ajouté à la galerie par un administrateur</td>
				<td>80 gils</td>
			</tr>
			<tr>
				<td>Proposer un évènement</td>
				<td>50 gils</td>
			</tr>
			<tr>
				<td>Avoir un évènement validé par un administrateur</td>
				<td>200 gils</td>
			</tr>
		</table>
	
			
		
	<?php endif; ?>
	
</div>

<script src="<?php echo plugin_url('highslide/highslide.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo plugin_url('highslide/highslide.css') ?>" />
<script>
    hs.graphicsDir = '<?php echo base_url() ?>public/plugins/highslide/graphics/';
</script>

<?php if($this->session->userdata('chatboxFixed') == 1) $this->load->view('community.php');  ?>
