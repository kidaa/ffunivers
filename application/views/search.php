<?php $this->load->view('mainSideMenu.php') ?>


<div class="contentCenter">
	<h1><?php echo $title ?></h1>
	
	<form class="noValid search" onsubmit="search(); return false;">
		<input class="searchTxt" type="text" name="keywords" value="Mots clefs de votre recherche" onfocus="if(this.value=='Mots clefs de votre recherche') this.value= ''" />
		<input class="loupe" type="image" src="<?php echo img_url('bigSearch.png') ?>">
			
		<table class="tableSearch">
			<tr>
				<td style="border-left:0;display:none">
					<input type="radio" id="where1" name="where" value="1" checked /> <label for="where1"> Contenu</label> <br />
				<!-- 	<input type="radio" id="where2" name="where" value="2" /> <label for="where2">Dans les messages</label> <br />
					<input type="radio" id="where3" name="where" value="3" /> <label for="where3">Dans le forum</label>  -->
				</td>
				<td style="border-left:0">
					
					<input type="radio" id="typeSearch1" name="typeSearch" value="1" checked /> <label for="typeSearch1"> Phrase exacte</label><br />
					<input type="radio" id="typeSearch2" name="typeSearch" value="2" /> <label for="typeSearch2"> Tous les mots</label><br />
				</td>
				<td>
					
					<input type="radio" id="sort1" name="sort" value="1" checked /> <label for="sort1"> Trier par occurences</label><br />
					<input type="radio" id="sort2" name="sort" value="2" /> <label for="sort2"> Trier par date</label>
				</td>
				<td style="width:200px;border-right:0">
					<div class="where_1" style="display:none">
						<input type="radio" id="title1" name="title" value="1" /> <label for="title1"> Dans les titres</label><br />
						<input type="radio" id="title2" name="title" value="2" checked /> <label for="title2"> Dans le contenu</label><br /><br />
						
						<input type="checkbox" id="cat1" name="cat1[]" value="1" checked /> <label for="cat1"> Actualités</label><br />
						<input type="checkbox" id="cat2" name="cat1[]" value="2" checked /> <label for="cat2"> Rubriques</label><br />
						<input type="checkbox" id="cat3" name="cat1[]" value="3" checked /> <label for="cat3"> Articles</label>
					</div>
					
					<div class="where_2" style="display:none">
						<em>Auteur :</em> <br />
						<input type="text" class="txt" name="author" style="width:130px;" /><br /><br />
						
						<input type="checkbox" id="cat21" name="cat2[]" value="1" checked /> <label for="cat21"> Forum</label><br />
						<input type="checkbox" id="cat22" name="cat2[]" value="2" checked /> <label for="cat22"> Articles</label><br />
						<input type="checkbox" id="cat23" name="cat2[]" value="3" checked /> <label for="cat23"> Actualités</label>
					</div>
					
					<div class="where_3" style="display:none">
						<em>Auteur :</em> <br />
						<input type="text" class="txt" name="author" style="width:130px" /><br /><br />
						
						<input type="checkbox" id="sondage1" name="sondage[]" value="1" checked /> <label for="sondage1"> Sans sondages</label><br />
						<input type="checkbox" id="sondage2" name="sondage[]" value="2" checked /> <label for="sondage2"> Avec sondages</label>
					</div>
				</td>
			</tr>
		</table>
		
		<input type="submit" style="display:none" />

	</form>
	
	<div id="results"></div>
	
</div>

<script>
// $('form.search').on('submit', search);
$('form.search input[type=radio]').on('click', search);
$('form.search input[type=checkbox]').on('click', search);

function search()
{
	$.post(baseUrl+'doSearch', $('form.search').serialize(), function(data)
	{
		$('#results').html(data);
	});

}

$('input[name=where]').on('click', show);

var currentShow = false;

function show()
{
	var toShow = $('input[name=where]:checked').val();
	if(currentShow == toShow) return false;
	if(!currentShow)
	{
		$('.where_'+toShow).fadeIn(300);
		currentShow = toShow;
	}
	else
	{

		$('.where_'+currentShow).fadeOut(300, function()
		{
			$('.where_'+toShow).fadeIn(300);
			currentShow = toShow;
		});	
	}
}

show();
</script>
