<div class="sideMenuLeft">
	<div class="menu">
		<div class="actions">
			<a onclick="window.open(this.href, 'Partager un lien', 'height=370, width=642') ; return false;"  title="Partager sur facebook" class="noajax"  href="http://www.facebook.com/sharer/sharer.php?u=<?php echo current_url() ?>"><?php echo img('facebook.png') ?></a>
			<a title="Partager sur twitter" class="noajax" href="https://twitter.com/intent/tweet?original_referer=<?php echo current_url() ?>&source=tweetbutton&text=<?php echo $title ?>&url=<?php echo current_url() ?>&via=ff13_univers"href=""><?php echo img('twitter.png') ?></a>
			<a title="Modifier ce contenu" href="<?php echo site_url('editerContenu/'.$infos['id']) ?>"><?php echo img('edit.png') ?></a>
			<a title="Ajouter du contenu au site" href="<?php echo site_url('editerContenu/0/'.$infos['type']) ?>"><?php echo img('new.png') ?></a>
			<a title="Voir les autres versions de la page" href="<?php echo site_url('historique/'.$infos['id']) ?>"><?php echo img('history.png') ?></a>
		</div>
		<?php if($history): ?>
			<ul class="classic"> 
				<li><a href="<?php echo site_url('historique/'.$listVersions[0]['id']) ?>">Dernière version</a></li>
				<li><a href="<?php echo site_url('historique/'.$listVersions[count($listVersions)-1]['id']) ?>">Première version</a></li>
				<li><a href="<?php echo site_url('historique/'.$listVersions[$keyCurrentContent]['id']) ?>">Version courante</a></li>
			<?php foreach($listVersions as $key => $version): ?>
				<?php if($keyCurrentView > ($key+3) || $keyCurrentView < ($key-3)) continue; ?>
				<li><a <?php echo $version['id'] == $infos['id'] ? 'class="current"' : ''; ?> href="<?php echo site_url('historique/'.$version['id']) ?>">Version <?php echo $version['version'] ?></a></li>
			<?php endforeach; ?>
			</ul>
		<?php else: ?>
		
		
		
		<?php 
		if($infos['type'] == 2)
		{
			echo '<div id="tree">'.treeNavToSideMenu($treeNav, $infos['id']).'</div>' ;
		}
		else
		{
			echo '<div class="listSide">';
			echo '<h4>'.($infos['type'] == 1 ? 'Les dernières actualités' : 'Les derniers articles').'</h4>';
			
				echo '<ul>';
				foreach($lastSide as $news)
				echo '<li><a href="'.site_url($news['url']).'">'.htmlspecialchars($news['title']).'</a></li>';
				echo '</ul>';
			
			echo '</div>';
		}
		?>
		
		<?php endif; ?>
	</div>
	
	<div class="pub">
		<script type="text/javascript"><!--
		google_ad_client = "ca-pub-5044588723656079";
		/* FF13v4 */
		google_ad_slot = "7319975990";
		google_ad_width = 200;
		google_ad_height = 200;
		//-->
		</script>
		<script type="text/javascript"
		src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
	</div>
	
</div>

<div class="contentCenter">
	<h1><?php echo $title ?></h1>
	
	<div class="infosContent">
		Page créée <strong><?php echo madate($infos['date']) ?></strong>
		<?php if($infos['editDate'] != $infos['date']): ?>
			et éditée <strong><?php echo madate($infos['editDate']) ?></strong>
		<?php endif; ?><br />

		Contributeurs : 
		<?php foreach($contributs as $user): ?>
			<?php echo profil_url($user['username'], $user['user'], $user['color']); ?> 
		<?php endforeach ?>

	</div>
	
	<?php if(isset($sommaire)): ?>
	<ul>
		<?php foreach($sommaire as $item): ?>
			<li><a class="itemSommaire" href="<?php echo site_url($item['url']) ?>"><?php echo htmlspecialchars($item['title']) ?></a></li>
			<p class="descSommaire"><?php echo htmlspecialchars($item['description']) ?></p>
		<?php endforeach ?>
	</ul>
	<?php else: ?>
		<div class="editor">
			<?php echo $infos['content'] ?>
		</div>
	<?php endif; ?>
	
	<?php if(isset($commentaires)) : echo $commentaires; ?>
	<script src="<?php echo js_url('comments') ?>"></script>
	<?php endif; ?>
</div>

<script>

$('.sideMenuLeft #tree a').on('click', function()
{
	var element = $(this).parent('li.plus').children('ul');
		
	if(element.length == 1 && element.css('display') == 'none')
	{
		element.slideDown(100, function() { element.parent('li').toggleClass('open closed'); } );
		return false;
	}
	else if(element.length == 1 && element.css('display') == 'block')
	{
		element.slideUp(100, function() { element.parent('li').toggleClass('open closed'); } );
		return false;
	}
	
});

</script>

<script src="<?php echo plugin_url('highslide/highslide.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo plugin_url('highslide/highslide.css') ?>" />
<script>
	<?php if(!$history && $infos['type'] == 2): ?>
	$('#tree').animate({scrollTop: $('#tree .current').offset().top},0);

	var hash = decodeURIComponent(window.location.hash);
	hash = hash.substring(1);
	if(hash != '')
	{
		$('.editor h2').each( function()
		{
			var current = $(this);
			if(current.text() == hash)
			{
				$(window).load(function() 
				{
					$('html,body').animate({scrollTop: current.offset().top},500);
				});

			}
		})
	}

	$('.editor h2').on('click', function()
	{
		var title = $(this).text();
		// title = title.replace(/(\r\n|\n|\r)/gm,"");
		window.location.hash = encodeURIComponent(title);
	});
	<?php endif; ?>


    hs.graphicsDir = '<?php echo base_url() ?>public/plugins/highslide/graphics/';

	
	$('.editor img').wrap( function()
	{
		var img = $(this);
		if(img.parent()[0].tagName == 'A')
		{
			var a = $(this).parent('a');
			
			// if(a.attr('href') == '') 
			// a.attr('href', $(this).attr('src'));
			
			a.attr('onclick','return hs.expand(this)');
		}
	});
</script>