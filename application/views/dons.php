<?php $this->load->view('mainSideMenu.php') ?>


	<div class="contentCenter">
	<h1><?php echo $title ?></h1>


	<div style="text-align:center">
		<?php echo img('chocobo.jpg')?>
	</div>

	<table class="table dons">
		<tr>
			<th style="width:60%">
				Acheter un code allopass
			</th>
			<th>
				Derniers acheteurs
			</th>
		</tr>
		<tr>
			<td>
				<form class="noValid" id="alloForm" method="post" action="https://payment.allopass.com/acte/access.apu">
					
			
				<div class="lineForm">
					<label>Votre nom</label>
					<input style="width:186px" class="txt" type="text" name="data" />
				</div>
			
				
				<div class="lineForm">
					<label for="pays">Selectionnez votre pays</label>
					<select style="width:200px" class="txt" id="pays">
						<option value=""></option>
						<option value="fr">France
						</option>
						<option value="be">Belgique (België)
						</option>
						<option value="de">Deutschland
						</option>
						<option value="es">España
						</option>
						<option value="ca">Canada
						</option>
						<option value="it">Italia
						</option>
						<option value="co">Colombia
						</option>
						<option value="ch">Suisse
						</option>
						<option value="ee">Eesti
						</option>
						<option value="lv">Latvija
						</option>
						<option value="lt">Lietuva
						</option>
						<option value="lu">Luxembourg
						</option>
						<option value="hu">Magyarország
						</option>
						<option value="nl">Nederland
						</option>
						<option value="no">Norge
						</option>
						<option value="pe">Perú
						</option>
						<option value="pl">Polska
						</option>
						<option value="at">Österreich
						</option>
						<option value="cz">Ceská republika</option>
					</select>
				</div>
				
				
				<div class="lineForm">
					<label>Code allopass</label>
					<input style="width:186px" type="text" name="code[]" maxlength="8" class="txt"  />
				</div>
				
				
				<input type="hidden" name="idd" value="1042460" />
				<input type="hidden" name="ids" value="253377" />
				<input type="hidden" name="recall" value="1" />
				<input type="hidden" name="lang" value="fr_FR" />
				
				<div style="text-align:center">
					<input type="submit" class="btn" value="Valider" />
				</div>
				<br />
			</form>
			</td>
			<td style="text-align:center">
				<?php foreach($dons as $don): ?>
					<div class="lineDon"><?php echo htmlspecialchars($don['name']) ?> <span><?php echo madate($don['date']) ?></span></div>
				<?php endforeach; ?>
			</td>
		</tr>
	</table>

	<script>
	$('#pays').on('change', function()
	{
	var pays = $(this).val();

	if(pays == '') return;

	window.open('http://payment.allopass.com/acte/scripts/popup/access.apu?ids=283773&idd=1176784&lang=fr&country='+pays,'phone','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=300,height=370');
	});
	</script>
</div>