<?php
require_once 'public/plugins/Diff/lib/Diff.php';

$options = array(
	// 'ignoreWhitespace' => true
	//'ignoreCase' => true,
);

$diff = new Diff($oldContent, $newContent, $options);
?>

<a id="closeOverlay" nohref="" onclick="$('#overlay').fadeOut(300);">Fermer cette fenêtre</a>

<?php

require_once 'public/plugins/Diff/lib/Diff/Renderer/Html/SideBySide.php';
$renderer = new Diff_Renderer_Html_SideBySide;
echo $diff->Render($renderer);
?>