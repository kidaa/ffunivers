<div class="sideMenuLeft">
	<div class="menu">
	<form class="noValid" id="filtresGallery">
		<ul class="classic mainSide">
			<li><a  style="background-image:url(<?php echo img_url('addArticle.png') ?>);" href="<?php echo site_url('editerContenu/0/3') ?>">Ajouter un article</a></li>
		</ul>
		
		<br />
		<ul class="classic">
			<li> <a <?php if($idCat == '*') echo 'class="current"'; ?> href="<?php echo site_url('articles') ?>">Tous les articles</a></li>
			<?php foreach($categories as $cat):	?>
			<?php if($cat['content_type'] != 3) continue; ?>	
			<li> <a <?php if($cat['id'] == $idCat) echo 'class="current"'; ?> href="<?php echo site_url('articles/'.$cat['id'].'/'.url_title($cat['name'])) ?>"><?php echo $cat['name'].'s' ?></a></li>
		<?php endforeach ?>
		</ul>
	</form>
	</div>
	
	<div class="pub">
		<script type="text/javascript"><!--
		google_ad_client = "ca-pub-5044588723656079";
		/* FF13v4 */
		google_ad_slot = "7319975990";
		google_ad_width = 200;
		google_ad_height = 200;
		//-->
		</script>
		<script type="text/javascript"
		src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
	</div>
</div>

<div class="contentCenter">
	<h1><?php echo $title ?></h1>
	
	<?php foreach($articles as $article): ?>
		<a href="<?php echo site_url($article['url']) ?>" class="cadreArticle" style="background:url(<?php echo site_url($article['image']) ?>) center no-repeat;">
			<span><?php echo htmlspecialchars($article['title']) ?></span>
		</a>
		
	<?php endforeach; ?>
</div>
