<?php $this->load->view('mainSideMenu.php') ?>


<div class="contentCenter">

	<div style="float:left;width:50%">
		<h2>Prochains évènements</h2>
			<ul class="classic">
			<?php foreach($nextEvents as $event): ?>
				<li>
					<a href="<?php echo site_url('evenement/'.$event['id'].'/'.url_title($event['name'])) ?>"><?php echo htmlspecialchars($event['name']) ?></a>
					<span><?php echo future_date($event['date']) ?></span>
				</li>
			<?php endforeach; ?>
			</ul>
		
	</div>
	
	<div style="float:left;width:50%">
		<h2>Anciens évènements</h2>
	
			<ul class="classic">
			<?php foreach($previousEvents as $event): ?>
				<li>
					<a href="<?php echo site_url('evenement/'.$event['id'].'/'.url_title($event['name'])) ?>"><?php echo htmlspecialchars($event['name']) ?></a>
					<span><?php echo future_date($event['date']) ?></span>
				</li>
			<?php endforeach; ?>
			</ul>
	</div>
	
</div>