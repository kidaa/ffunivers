<div class="sideMenuLeft">
	<div class="menu">
	<ul class="classic">
		<li><a <?php echo ($categorie == 'content' ? 'class="current"' : ''); ?> href="<?php echo site_url('admin/content') ?>">Gérer le contenu</a></li>
		<li><a <?php echo ($categorie == 'order' ? 'class="current"' : ''); ?> href="<?php echo site_url('admin/order') ?>">Ordonner l'arborescence</a></li>
		<li><a <?php echo (in_array($categorie, array('images','images2')) ? 'class="current"' : ''); ?> href="<?php echo site_url('admin/images') ?>">Gérer la galerie (<?php echo count($images) ?>)</a></li>
		<li><a <?php echo ($categorie == 'events' ? 'class="current"' : ''); ?> href="<?php echo site_url('admin/events') ?>">Gérer les évènements</a></li>
		<li><a <?php echo ($categorie == 'members' ? 'class="current"' : ''); ?> href="<?php echo site_url('admin/members') ?>">Gérer les membres</a></li>
		<li><a title="A faire après la validation de nouvelles versions, et lorsque l'ordre des pages a été modifié. Cette action est lourde en ressource et doit donc être réalisé manuellement une fois que vous éstimez avoir terminé vos modifications." id="refreshMenu" href="<?php echo site_url('refreshMenu') ?>">Mettre à jour le menu</a> </li>
	</ul>
	</div>
</div>

<script>
$('#refreshMenu').on('click', function()
{
	var source = $(this);
	$.get( source.attr('href'), function()
	{
		reload();
	});
	
	return false;
});
</script>

<div class="contentCenter">
	<h1><?php echo $title ?></h1>
	
	<?php if($categorie == 'content'): ?>
		<div class="legend">
		<strong>Légende :</strong> <br />
		<?php echo img('newContent.png') ?> Aucune version de la page n'est affichée sur le site<br />
		<?php echo img('notif.png') ?> Une version plus récente de la page est disponible<br />
		<?php echo img('updateComm.png') ?> La dernière version de la page est affichée sur le site<br />
		<?php echo img('inSlider.png') ?> Afficher la page dans le bandeau défilant sur l'accueil<br />
		</div>
		<br />
		
		
		<div class="lineForm">
			<label style="width:150px">Filtrer par type </label>
			
			
			<select style="width:200px" class="txt" id="type">
				<option value="*">Tout voir</option>
				<?php foreach($type_contents as $type): ?>
				<option value="<?php echo $type['id'] ?>"><?php echo $type['name'] ?> </option>
				<?php endforeach; ?>
			</select>
		</div>
		
		<div class="lineForm">
			<label style="width:150px">Filtrer par titre </label>
			
			<input style="width:250px;" type="text" id="title" class="txt" />
		</div>
		
		<div class="myContents">
			<?php foreach($contents as $content): ?>
				<div class="myContent" type="<?php echo $content['type'] ?>">
				
					<?php $new = true; foreach($content['history'] as $history)
					{
						if($history['isCurrent'] == 1) { $new = false; break; }
					}
					?>
					
					<?php 
						if($new)  echo img('newContent.png');
						else if($content['history'][0]['isCurrent'] == 0) echo img('notif.png');
						else echo img('updateComm.png'); 
					?>

				
					<span class="tiny">[<?php echo $content['name'] ?>]</span> 
					<span class="title"><?php echo htmlspecialchars($content['headTitle']) ?></span>
					
					<?php 
						if($content['inSlider'] == 1 && $content['type'] == 1)
						echo '<a class="inSlider" href="'.site_url('inSlider/'.$content['idCreation']).'">'.img('inSlider.png').'</a>';
						elseif($content['inSlider'] == 0 && $content['type'] == 1)
						echo '<a class="inSlider" href="'.site_url('inSlider/'.$content['idCreation']).'">'.img('notInSlider.png').'</a>';
					?>
					
					<div class="history">
															
						<?php foreach($content['history'] as $key => $history): ?>
							<div class="lineHistory">
							
								<a class="action" href="<?php echo site_url('historique/'.$history['id']) ?>"><?php echo img('view.png') ?></a>
								<a class="action" href="<?php echo site_url('editerContenu/'.$history['id']) ?>"><?php echo img('edit.png') ?></a>
								
								<a class="action deleteMyContent" href="<?php echo site_url('deleteMyContenu/'.$history['id']) ?>"><?php echo img('delete.png') ?></a>
								
								<a idCreation="<?php echo $content['idCreation'] ?>" class="action setCurrent" href="<?php echo site_url('setCurrent/'.$history['id']) ?>">
									<?php if($history['isCurrent'] == 1)  echo img('current.png');  else echo img('notCurrent.png'); ?>
								</a>
								
								v<?php echo $history['editFromVersion'] ?> <span class="tiny">></span> v<?php echo $history['version'] ?>
								
													
								<span class="tiny" style="float:right">
									<?php echo profil_url($history['username'],$history['user'],$history['color'] ) ?>
									<?php echo madate($history['editDate']) ?> 
									<?php echo colorNb($history['linesModified'],'(',' ligne'.($history['linesModified'] > 1 ? 's' : '').' | '); ?>
									<?php echo colorNb($history['sizeModified'],'',' octet'.($history['sizeModified'] > 1 ? 's' : '').')'); ?>
									<a class="diff" href="<?php echo site_url('diff/'.$history['idCreation'].'/'.$history['editFromVersion'].'/'.$history['version']) ?>">(diff)</a>
								</span>
								
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	
		<script>
		// slider accueil
		$('.inSlider').on('click', function()
		{
			var source = $(this);
			$.get(source.attr('href'), function(data)
			{
				if(data == 0)
				source.children('img').attr('src', baseUrl+'public/images/notInSlider.png');
				else
				source.children('img').attr('src', baseUrl+'public/images/inSlider.png');
			});
			
			return false;
		});
		
		// fenetre differences
		$('.diff').on('click', function()
		{
			var source = $(this);
			$.get(source.attr('href'), function(data)
			{
				$('body').prepend('<div id="overlay"><div id="windowDiff"></div></div>');
				$('#windowDiff').html(data);
				$('#windowDiff')[0].focus();
				$('#overlay').fadeIn(300);
				
			});
			
			return false;
		});
		
		
		// filtres
		$('#type').on('change', filtres)
		$('#title').on('keyup', filtres)
		$('#title').on('click', filtres)

		function filtres()
		{
			var filtreType = $('#type').val();
			var filtreTitle = $('#title').val();
			
			var lines = $('.myContent');
			
			
			$('.myContent').each(function()
			{
				var reg = new RegExp(filtreTitle, "gi");
				var currentTitle =  $(this).children('.title').html();
				var currentType =  $(this).attr('type');

				if(filtreType == '*')
				{
					if(currentTitle.match(reg))
					$(this).show();
					else
					$(this).hide();
				}
				else
				{
					if(currentType == filtreType && currentTitle.match(reg))
					$(this).show();
					else
					$(this).hide();
				}
			});
		}
		
			
		// suppression d'une version
		$('.deleteMyContent').on('click', function()
		{
			if(!confirm("Voulez-vous vraiment supprimer cette version ?")) 
			return false;
			
			var source = $(this);
			$.get( source.attr('href'), function(data)
			{
				source.parent('div.lineHistory').remove();
			});
			
			return false;
		});
		
		// setCurrent
		$('.setCurrent').on('click', function()
		{	
			var source = $(this);
			var idCreation = source.attr('idCreation');
			
			$.get( source.attr('href'), function(data)
			{
				// version courante, on la met a pas courante
				if(source.children('img').attr('src') == baseUrl+'public/images/current.png')
				{
					source.children('img').attr('src',baseUrl+'public/images/notCurrent.png');
				}
				else
				{
					$('a[idCreation='+idCreation+'] img').attr('src',baseUrl+'public/images/notCurrent.png');
					source.children('img').attr('src',baseUrl+'public/images/current.png');
				}
			});
			
			return false;
		});
		
		//effets hover
		$('.myContent').on('mouseenter', function()
		{
			$(this).children('div.history').stop(true,true).slideDown(200);
		});
		
		$('.myContent').on('mouseleave', function()
		{
			$(this).children('div.history').hide();
		});
		</script>
	
	
	<?php elseif($categorie == 'order'): ?>
	
		<div class="orderMenu">
			<?php echo treeNavToAdmin($tree); ?>
		</div>
		
		<script>
		orderActive = false;
		
		$('.down').on('click', function()
		{
			if(orderActive) return;
			orderActive = true;
				
			var source = $(this);
			var idCreation = $(this).parent().parent().attr('idCreation');
			
			$.get(baseUrl+'sort/'+idCreation+'/0', function(data)
			{
				if(data != 'noUpdate')
				{
					var ligneCurrent = source.parent().parent();
					var ligneClone = ligneCurrent.clone(true);
					var ligneSuiv = ligneCurrent.next();
					ligneCurrent.remove();
					ligneClone.insertAfter(ligneSuiv);
					
				}
				orderActive = false;

			});
		});
		
		$('.up').on('click', function()
		{
			if(orderActive) return;
			orderActive = true;
				
			var source = $(this);
			var idCreation = $(this).parent().parent().attr('idCreation');
			
			$.get(baseUrl+'sort/'+idCreation+'/1', function(data)
			{
				
				if(data != 'noUpdate')
				{
					var ligneCurrent = source.parent().parent();
					var ligneClone = ligneCurrent.clone(true);
					var lignePrec = ligneCurrent.prev();
					ligneCurrent.remove();
					ligneClone.insertBefore(lignePrec);
					
				}
				orderActive = false;
			});
		});
		</script>
		
	
	
	
	
	
	
	<?php elseif($categorie == 'images2'): ?>
	
	
		<?php echo $this->pagination->create_links(); ?>
	
		<?php foreach($images2 as $img): ?>
			<div id="divIndex_<?php echo $img['id'] ?>" class="allImages sortImagesUser">
				<a onclick="return hs.expand(this)" target="_blank" href="<?php echo site_url('public/images_upload/big/'.$img['name']) ?>">
					<img id="imgIndex_<?php echo $img['id'] ?>" src="<?php echo site_url('public/images_upload/mini/'.$img['name']) ?>" />
				</a>
				<form noLoading method="post" action="<?php echo site_url('indexImage') ?>">
					<input type="hidden" value="<?php echo $img['id'] ?>" name="id" />
					<div class="lineForm">
						<label>Catégorie</label>
						<select class="txt" name="categorie">
							<option value="0">Aucune</option>
							<?php foreach($categories as $cat):	
							if($cat['content_type'] != 4) continue; 
							$selected=''; if($cat['id'] == $img['categorie']) $selected = 'selected'; ?>	
								<option <?php echo $selected ?> value="<?php echo $cat['id'] ?>"><?php echo $cat['name'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
					
					<div class="lineForm">
						<label>Type d'images</label>
						<select class="txt" name="categorieType">
							<option value="0">Aucune</option>
							<?php foreach($categories as $cat):	
							if($cat['content_type'] != 5) continue; 
							$selected=''; if($cat['id'] == $img['categorieType']) $selected = 'selected'; ?>	
								<option <?php echo $selected ?> value="<?php echo $cat['id'] ?>"><?php echo $cat['name'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
				
					<input type="submit" value="Ok" class="btn" />
					
				</form>
				<a title="Enlever de la galerie" class="refuse" href="<?php echo site_url('removeFromGallery/'.$img['id']) ?>"><?php echo img('delete.png') ?></a>
			</div>
			
			
		<?php endforeach; ?>
		
		<?php echo $this->pagination->create_links(); ?>
	
	<?php elseif($categorie == 'images'): ?>
	
		<div style="height:70px">
			<a class="add" href="<?php echo site_url('admin/images2') ?>">
				<?php echo img('sortImages.png') ?>
				Gérer les images qui sont déjà dans la galerie
			</a>
		</div>
		
		<?php foreach($images as $img): ?>
			<div id="divIndex_<?php echo $img['id'] ?>" class="allImages sortImagesUser">
				<a onclick="return hs.expand(this)" target="_blank" href="<?php echo site_url('public/images_upload/big/'.$img['name']) ?>">
					<img id="imgIndex_<?php echo $img['id'] ?>" src="<?php echo site_url('public/images_upload/mini/'.$img['name']) ?>" />
				</a>
				<form noLoading method="post" action="<?php echo site_url('addToGallery') ?>">
					<input type="hidden" value="<?php echo $img['id'] ?>" name="id" />
					<div class="lineForm">
						<label>Catégorie</label>
						<select class="txt" name="categorie">
							<option value="0">Aucune</option>
							<?php foreach($categories as $cat):	
							if($cat['content_type'] != 4) continue; 
							$selected=''; if($cat['id'] == $img['categorie']) $selected = 'selected'; ?>	
								<option <?php echo $selected ?> value="<?php echo $cat['id'] ?>"><?php echo $cat['name'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
					
					<div class="lineForm">
						<label>Type d'images</label>
						<select class="txt" name="categorieType">
							<option value="0">Aucune</option>
							<?php foreach($categories as $cat):	
							if($cat['content_type'] != 5) continue; 
							$selected=''; if($cat['id'] == $img['categorieType']) $selected = 'selected'; ?>	
								<option <?php echo $selected ?> value="<?php echo $cat['id'] ?>"><?php echo $cat['name'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
				
					<input type="submit" value="Ok" class="btn" />
					
				</form>
				<a title="Ne pas ajouter à la galerie" class="refuse" href="<?php echo site_url('refuseGallery/'.$img['id']) ?>"><?php echo img('delete.png') ?></a>
			</div>
			
			
		<?php endforeach; ?>
		
		
		

	<?php elseif($categorie == 'events'): ?>
		
			<form noLoading style="margin-bottom:50px;" method="post" action="<?php echo site_url('addEvent') ?>">
				<input style="width:196px" class="txt" type="text" name="name" />
				<input style="width:100px" class="txt" type="text" name="date" />
				
				<select class="txt"  style="width:120px" name="isValid">
					<option value="0">Non publié</option>
					<option value="1">Publié</option>
				</select>
				
				
				<input class="btn2" value="Ajouter" type="submit" />
			</form>
			
		<?php foreach($events as $event): ?>
			<form noLoading style="margin-bottom:3px;" method="post" action="<?php echo site_url('editEvent') ?>">
				<a class="deleteEvent" style="float:left;margin-top:5px;margin-right:10px;" href="<?php echo site_url('deleteEvent/'.$event['id']) ?>"><?php echo img('delete.png') ?></a>
				<input name="id" type="hidden" value="<?php echo $event['id'] ?>" />
				<input style="width:160px" class="txt" type="text" name="name" value="<?php echo htmlspecialchars($event['name']) ?>" />
				<input style="width:100px" class="txt" type="text" name="date" value="<?php echo date('d/m/y H:i', datetime2timestamp($event['date'])) ?>" />
				
				<select class="txt"  style="width:120px" name="isValid">
					<option <?php echo $event['isValid'] == 0 ? 'selected' : '' ?> value="0">Non publié</option>
					<option <?php echo $event['isValid'] == 1 ? 'selected' : '' ?> value="1">Publié</option>
				</select>
				
				<?php echo profil_url($event['username'],$event['user'],$event['color']) ?>
				
				<input class="btn2" value="Editer" type="submit" />
			</form>
		<?php endforeach; ?>
		
		<script>
		$('.deleteEvent').on('click', function()
		{
			var source = $(this);
			
			if(!confirm('Etes-vous sur ?')) return false;
			
			$.get( source.attr('href'), function(data)
			{
				source.parent('form').slideUp();
			});
			
			return false;
		});	

		</script>
	<?php elseif($categorie == 'members'): ?>
	
	<table class="table">
		<tr>
			<th style="width:50%">
				Bannir
			</th>
			<th>
				Donner un avertissement
			</th>
		</tr>
		<tr>
			<td>
				<form class="noValid" method="post" action="<?php echo site_url('actionMembers/bann/0') ?>">
					<input style="width:200px" class="txt" type="text" name="username" />
					<input class="btn2" value="Bannir" type="submit" />
				</form>
			</td>
			<td>
				<form class="noValid" method="post" action="<?php echo site_url('actionMembers/averto/0') ?>">
					<input style="width:200px" class="txt" type="text" name="username" />
					<input class="btn2" value="Avertir" type="submit" />
				</form>
			</td>
		</tr>
			<tr>
			<th>
				Liste des bannis
				
				
				
			</th>
			<th>
				Liste des avertissements
			</th>
		</tr>
		<tr>
			<td>
				<?php foreach($banned as $user): ?>
					<a href="<?php echo site_url('actionMembers/debann/'.$user['username']) ?>">Débannir</a> - <?php echo $user['username'] ?><br />
				
				<?php endforeach; ?>
			</td>
			<td>
				<?php foreach($averto as $user): ?>
					<?php echo $user['username'] ?> - <?php echo $user['averto'] ?> avertissement(s)<br />
				
				<?php endforeach; ?>
			</td>
		</tr>
	</table>
	
	<?php endif; ?>
</div>


<script src="<?php echo plugin_url('highslide/highslide.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo plugin_url('highslide/highslide.css') ?>" />
<script>
	hs.graphicsDir = '<?php echo base_url() ?>public/plugins/highslide/graphics/';
</script>


<script>
$('.refuse').on('click', function()
{
	var source = $(this);
	$.get( source.attr('href'), function(data)
	{
		source.parent('.sortImagesUser').slideUp(200);
	});
	
	return false;
});	

$('.sortImagesUser form').on('submit', function()
{
	var source = $(this);
	
	
	if(!source.attr('action').match(/indexImage/))
	source.parent('.sortImagesUser').slideUp(200);
});
</script>
