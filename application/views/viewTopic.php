<div class="sideMenuLeft">
	<div class="menu">
		<ul class="classic mainSide">
			<li>
				<a style="background-image:url(<?php echo img_url('forum.png') ?>);" href="<?php echo site_url('forum') ?>">Retour au forum</a>
			</li>
		</ul>
		<br />
		
		<h4>Derniers sujets actifs</h4>
		<div class="listSide">
			<ul>
				<?php foreach($topics as $topic): ?>
				<li>
					<a href="<?php echo site_url('forum/'.$topic['id'].'/'.url_title($topic['title'])) ?>"><?php echo htmlspecialchars($topic['title']) ?></a>
				</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	
	<div class="pub">
		<script type="text/javascript"><!--
		google_ad_client = "ca-pub-5044588723656079";
		/* FF13v4 */
		google_ad_slot = "7319975990";
		google_ad_width = 200;
		google_ad_height = 200;
		//-->
		</script>
		<script type="text/javascript"
		src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
	</div>
</div>

<div class="contentCenter">


	<h1><?php echo htmlspecialchars($title) ?></h1>

	<?php if($this->session->userdata('group') >= 2): ?>
	<ul class="myUl">
		<li>
			<a href="<?php echo site_url('epingle/'.$infos['id']) ?>">
				<?php echo $infos['epingled'] == 1 ? 'Ne plus épingler ce topic' : 'Epingler ce topic'; ?>
			</a>
		</li>
		<li>
			<a href="<?php echo site_url('block/'.$infos['id']) ?>">
				<?php echo $infos['blocked'] == 1 ? 'Ne plus bloquer ce topic' : 'Bloquer ce topic'; ?>
			</a>
		</li>
		<?php if($infos['sondage'] == 1): ?>
		<li>
			<a href="<?php echo site_url('currentSondage/'.$infos['id']) ?>">
				Mettre ce sondage sur l'accueil
			</a>
		</li>
		<?php endif; ?>
		<li>
			<a onclick="if(!confirm('Etes-vous sur ?')) return false;" href="<?php echo site_url('delete/'.$infos['id']) ?>">
				Supprimer ce topic
			</a>
		</li>
	</ul>
	<br />
	<?php endif; ?>

	<?php if($infos['sondage'] == 1): ?>
	<div class="viewSondage">
		<ul>
			<?php if(!$this->input->cookie('vote_'.$infos['id'], TRUE)): ?>
				<?php foreach($responses as $rep): ?>
					<li class="choose"><a href="<?php echo site_url('voteSondage/'.$infos['id'].'/'.$rep['id']) ?>"><?php echo htmlspecialchars($rep['response']); ?></a></li>
				<?php endforeach; ?>
			<?php else: ?>
				<?php foreach($responses as $rep): ?>
					<li class="results">
						<div class="cadreJauge">
							<div class="jauge" style="width:<?php echo round($rep['nbVote']*100/ $total) ?>%"></div>
						</div>
						<div class="result">
							<?php echo round($rep['nbVote']*100/ $total) ?>% &nbsp;
							<?php echo $rep['nbVote'] ?> votes &nbsp;
							<span><?php echo htmlspecialchars($rep['response']); ?></span>
						</div>
					</li>
				<?php endforeach; ?>
			<?php endif; ?>
		</ul>
	</div>
	<?php endif; ?>


	<?php if(isset($commentaires)) : echo $commentaires; ?>
	<script src="<?php echo js_url('comments') ?>"></script>
	<?php endif; ?>

	<script src="<?php echo plugin_url('highslide/highslide.js') ?>"></script>

	<script>
		hs.graphicsDir = '<?php echo base_url() ?>public/plugins/highslide/graphics/';
	</script>
</div>