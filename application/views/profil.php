<h1 class="mainH1" style="white-space:normal">
	<?php echo $title ?>
	<a class="sendmail" href="<?php echo site_url('monCompte/messagerie/'.$infos['username'].'#new_convers') ?>">Lui envoyer un message privé</a>
</h1>

<div class="profil">
	
	<div class="avatar"><?php echo avatar_url($infos['id']) ?></div>
	<div class="profilInfos">
		
			<div class="profilGils"> <?php echo number_format($infos['gils'],1,',',' ') ?> gils</div>
			<?php if($infos['group'] == 0) echo '<span style="color:#ff8c00">Membre</span>'; ?>
			<?php if($infos['group'] == 1) echo '<span style="color:#800000">Banni</span>'; ?>
			<?php if($infos['group'] == 2) echo '<span style="color:#008000">Modérateur</span>'; ?>
			<?php if($infos['group'] == 3) echo '<span style="color:#ff0000">Administrateur</span>'; ?>
			<?php if($infos['group'] == 4) echo '<span style="color:#4169e1">Webmaster</span>'; ?> - 
			<?php echo $infos['online'] == 1 ? '<span style="color:green">Connecté</span>' : '<span style="color:red">Hors ligne</span>' ?>
			<br />
			Inscrit <?php echo madate($infos['registerDate']) ?><br />
			Dernier passage <?php echo madate($infos['lastDate']) ?>
	</div>
	<?php 
		$file_headers = @get_headers('http://mypsn.eu.playstation.com/psn/profile/'.$infos['idPSN'].'.png');
		if($file_headers[0] == 'HTTP/1.1 404 Not Found')
		$exists = false;
		else
		$exists = true;

		if(trim($infos['idPSN']) != '' && $exists) 
		echo '<img style="margin:0;position:absolute;right:-15px;top:35px" src="http://mypsn.eu.playstation.com/psn/profile/'.$infos['idPSN'].'.png" />';
		?>
	
	
	
	<div style="clear:both"></div>
	<br />

	<div class="cadreParam">
		<h2>Ma description</h2>
		<p class="description"><?php echo montexte($infos['description']) ?></p>
	</div>

	<div class="cadreParam">
		<h2>Mon profil</h2>
		

		<div class="infoProfil">
			<div>Pays</div>
			<?php echo trim($infos['country']) != '' ? $infos['country'] : '-' ?>
		</div>
		
		
		
		<div class="infoProfil">
			<div>Sexe</div>
			<?php echo $infos['sexe'] == 0 ? 'Homme' : '<span style="color:#ff1493">Femme</span>' ?>
		</div>
		
		<div class="infoProfil">
			<div>Site web</div>
			<?php echo trim($infos['urlWeb']) != '' ? '<a target="_blank" href="'.prep_url($infos['urlWeb']).'">'.$infos['urlWeb'].'</a>' : '-' ?>
		</div>
		
		<div class="infoProfil">
			<div>Compte PSN</div>
			<?php echo trim($infos['idPSN']) != '' ? $infos['idPSN'] : '-' ?>
		</div>
		
		<div class="infoProfil">
			<div>Compte Xbox live</div>
			<?php echo trim($infos['idXboxlive']) != '' ? $infos['idXboxlive'] : '-' ?>
		</div>
		
		<div class="infoProfil">
			<div>Date de naissance</div>
			<?php echo $infos['birth'] != '0000-00-00' ? date('d/m/Y', date2timestamp($infos['birth'])) : '-' ?> - <?php echo $infos['birth'] != '0000-00-00' ? age($infos['birth']).' ans' : '' ?>
		</div>
				
		<div class="infoProfil">
			<div>Messages postés</div>
			<?php echo $infos['nbPost']  ?>
		</div>
		
		<div class="infoProfil">
			<div>Messages chatbox</div>
			<?php echo $infos['nbPostChatbox']  ?>
		</div>
		
		<div class="infoProfil">
			<div>Connexions</div>
			<?php echo $infos['nbConnection']  ?>
		</div>
		
		<div class="infoProfil">
			<div>Avertissements</div>
			<?php echo $infos['averto']  ?>
		</div>
	</div>

	<div style="clear:both"></div>

	<div class="cadreParam">
		<h2>Mes contributions</h2>
		
		<div class="contributions">
			<?php foreach($contents as $content): ?>
				<a href="<?php echo site_url('historique/'.$content['id']) ?>">
					<span>[<?php echo $content['name'] ?>]</span> 
					<?php echo htmlspecialchars($content['headTitle']) ?>
				</a>
			<?php endforeach; ?>
		</div>
	</div>
	
	<div class="cadreParam">
		<h2>Mes images</h2>
		
		<div class="allImages">
		<?php foreach($images as $img): ?>
			<a onclick="return hs.expand(this)" target="_blank" href="<?php echo site_url('public/images_upload/big/'.$img['name']) ?>"> 
				<img src="<?php echo site_url('public/images_upload/mini/'.$img['name']) ?>" />
			</a>
		<?php endforeach ?>
		</div>

	</div>
</div>


<script src="<?php echo plugin_url('highslide/highslide.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo plugin_url('highslide/highslide.css') ?>" />
<script>
    hs.graphicsDir = '<?php echo base_url() ?>public/plugins/highslide/graphics/';
</script>