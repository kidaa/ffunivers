<?php
class Shop extends CI_Model 
{
	function add($link,$title,$img)
	{
		$this->db->insert('shop', array('link'=>$link,'image'=>$img,'title'=>$title));
	}
	
	function getAlea()
	{
		return $this->db->order_by('id','random')->limit(1)->get('shop')->row_array();
	}
}