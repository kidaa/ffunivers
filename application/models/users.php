<?php
class Users extends CI_Model 
{
    function __construct()
    {
        parent::__construct();
    }
	
	/*
		groupes
		0 => membre
		1 => banni
		2 => modo
		3 => admin
		4 => webmaster
	*/

	function gils($nb,$to = null)
	{
		if($to == null)
		$to = $this->session->userdata('id');
		
		if(!is_numeric($to))
		$to = $this->getIdByName($username);
		
		if($nb == 0.1) // message dans la tchatbox
		$this->db->where('id',$to)->set('nbPostChatbox','nbPostChatbox+1',false)->set('gils','gils+'.$nb, false)->update('users');
		else
		$this->db->where('id',$to)->set('gils','gils+'.$nb, false)->update('users');
	}
	
	function getTeam()
	{
		return $this->db->select('id,username,color,online,group,gils,sexe')
		->where('group > ',1)
		->order_by('gils desc')
		->get('users')
		->result_array();
	}
	
	function getMembers($page, $sort)
	{
		return $this->db->select('id,username,color,online,gils,sexe')
		->where('group',0)
		->where('isAnonymous', 0)
		->order_by($sort)
		->order_by('id asc')
		->limit(14, $page)
		->get('users')
		->result_array();
	}
	
	function getNbMembers()
	{
		return $this->db->where('group',0)
		->where('isAnonymous', 0)
		->count_all_results('users');
	}
	
	function topGils()
	{
		return $this->db->select('id,username,color,gils')
		->where('isAnonymous', 0)
		->where('group',0)
		->order_by('gils desc')
		->limit(10)
		->get('users')
		->result_array();
	}
	
	
	function getBanned()
	{
		return $this->db->select('username')->where('group',1)->order_by('username')->get('users')->result_array();
	}
	function getAverto()
	{
		return $this->db->select('username,averto')->where('averto >=',1)->order_by('averto DESC,username')->get('users')->result_array();
	}
	
	function getInfosForMap()
	{
		return $this->db->select('id,username,color,postal,address,city,country')
		->where('onMap',1)
		->where('postal !=','')
		->where('address !=','')
		->where('city !=','')
		->where('country !=','')
		->where('postal !=','')
		->get('users')
		->result_array();
	}

	function update($data)
	{
		$this->db->where('id', $this->session->userdata('id'))
		->update('users',$data);
		
		$res = $this->db->select('gilsProfil')->where('id', $this->session->userdata('id'))->get('users')->row_array();
		
		if($res['gilsProfil'] == 0 &&
		trim($data['mail']) != '' &&
		trim($data['color']) != '' &&
		trim($data['birth']) != '' &&
		trim($data['sexe']) != '' &&
		trim($data['postal']) != '' &&
		trim($data['address']) != '' &&
		trim($data['city']) != '' &&
		trim($data['country']) != '' &&
		trim($data['firstName']) != '' &&
		trim($data['description']) != ''
		&& avatar_exist($this->session->userdata('id')))
		{
			$this->users->gils(100);
			$this->db->set('gilsProfil',1)->where('id', $this->session->userdata('id'))->update('users');
		}
	}
	
	function getIdByName($username)
	{
		$res = $this->db->select('id')->where('username', $username)->get('users')->row_array();
		
		if(count($res) == 0) return false;
		else return $res['id'];
	}

	
	function connect($username,$password)
	{
		$res = $this->db->select('id,username,password,group')
		->where('username',$username)
		->where('isAnonymous',0)
		->get('users')
		->row_array();
		
		if(count($res) == 0)
		return false;
		else if($password != $res['password'])
		return false;
		else
		{
			$this->db->set('online',0)->where('id', $this->session->userdata('id'))->update('users');
			$this->db->set('nbConnection','nbConnection+1',false)
			->where('id',$res['id'])->update('users');
			
			$this->session->set_userdata('id', $res['id']);
			$this->session->set_userdata('username', $res['username'] );
			$this->session->set_userdata('isAnonymous',0 );
			$this->session->set_userdata('group', $res['group']);
			
			$this->updateLastInfos();
			
			return true;
		}
	}
	
	function alreadyVisited()
	{
		$res = $this->db->select('id,username')
		->where('lastIp',$this->input->ip_address())
		->where('isAnonymous',1)
		->get('users')
		->row_array();
		if(count($res) == 0)
		return false;
		else
		{
			$this->session->set_userdata('id', $res['id']);
			$this->session->set_userdata('username', $res['username'] );
			$this->session->set_userdata('isAnonymous',1 );
			$this->session->set_userdata('group', 0);
			
			return true;
		}
	}
	
	function createAnonymous()
	{
		$this->load->helper('string');
		
		$username = 'Anonyme_'.random_string('alnum',5);
		while($this->isUsernameUsed($username))
		$username = 'Anonyme_'.random_string('alnum',5);
		
		$this->db->insert('users', array(
		'userAgent' => $this->load->library('user_agent'),
		'username' => $username,
		'lastIp' => $this->input->ip_address(),
		'lastDate' => date('Y-m-d H:i:s'),
		'isAnonymous' => 1,
		'isRobot' => $this->agent->is_robot()
		));
		
		$this->session->set_userdata('id', $this->db->insert_id());
		$this->session->set_userdata('username', $username );
		$this->session->set_userdata('isAnonymous',1 );
		$this->session->set_userdata('group', 0);
	}
	
	function addUser($username, $password)
	{
		$this->db->where('id', $this->session->userdata('id'))
		->where('isAnonymous', 1)
		->update('users', array(
		'userAgent' => $this->load->library('user_agent'),
		'username' => $username,
		'password' => $password,
		'lastIp' => $this->input->ip_address(),
		'registerDate' => date('Y-m-d H:i:s'),
		'lastDate' => date('Y-m-d H:i:s'),
		'isAnonymous' => 0
		));
	}
	
	
	function idExist($id)
	{
		if($this->db->where('id',$id)->count_all_results('users') == 1)
		return true;
		else
		return false;
	}
	
	function isUsernameUsed($username)
	{
		if($this->db->where('username',$username)->count_all_results('users') == 1)
		return true;
		else
		return false;
	}
	
	function getInfosHeader()
	{
		$res = $this->db->select('gils,nbMP,group')
		->where('id', $this->session->userdata('id'))
		->get('users')
		->row_array();
		
		return array($res['gils'], $res['nbMP'], $res['group']);
	}
	
	function getInfosAccount( $id )
	{
		return $this->db->select('id,username,password,mail,gils,group,registerDate,lastDate,online,color, birth, sexe, postal,address, city, country, firstName, lastName, urlWeb, description, rank, isAnonymous, isRobot, chatboxFixed,mailNotif,averto,idPSN,idXboxlive,nbPost,nbPostChatbox,nbConnection,onMap')
		->where('id', $id)
		->get('users')
		->row_array();
	}
	
	function getConnected()
	{
		return $this->db->select('id,username,color,isAnonymous')
		->where('isRobot',0)
		->where('online',1)
		->order_by('group DESC, id')
		->get('users')
		->result_array();
	}
	
	function updateLastInfos()
	{
		// mise a jour de nos infos
		$this->db->set('lastIp',$this->input->ip_address())
			->set('lastDate','"'.date('Y-m-d H:i:s').'"',false)
			->set('online',1)
			->set('isRobot',$this->agent->is_robot())
			->set('userAgent',$this->agent->agent_string())
			->where('id',$this->session->userdata('id'))->update('users');
		
		
		// deconnexion timeout
		$date5mnAgo = date('Y-m-d H:i:s', strtotime('-5 minutes') );
		$this->db->set('online',0)
		->where('lastDate < ','"'.$date5mnAgo.'"', false)
		->update('users');
	}
}