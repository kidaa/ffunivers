<?php
class Comments extends CI_Model 
{
    function __construct()
    {
        parent::__construct();
    }
	
	function getLastComments($limit = 3)
	{
		$commentaires = $this->db->select('c.id,c.idData,c.table,c.message,c.date,u.username,u.color,c.user')
        ->join('users u','u.id = c.user')
        ->join('sites s','c.site = s.id','left')
		->where_in('c.table', array('contents','events','forum'))
		->where('(s.url = "'.base_url().'" OR c.site IS NULL)',null,false)
        ->order_by('c.date desc')
        ->limit($limit+10)
        ->get('comments c')
        ->result_array();
		
		$array = array();
		$nb =  0;
		foreach($commentaires as $key => $line)
		{
			if($nb >= $limit) break;
			if($line['table'] == 'contents')
			{
				$data = $this->db->select('title,url,type')
				->where('idCreation', $line['idData'])
				->where('isCurrent', 1)
				->where_in('type', array(1,3))
				->get('contents')
				->row_array();
				
				if(count($data) == 0) continue;
				
				if($data['type'] == 1) 
				$array[$key]['categorie'] = 'Actualité';
				else
				$array[$key]['categorie'] = 'Article';
				
				$array[$key]['title'] = $data['title'];
				$array[$key]['url'] = site_url($data['url']);
			}
			elseif($line['table'] == 'forum')
			{
				$data = $this->db->select('id,title')
				->where('id', $line['idData'])
				->get('topics')
				->row_array();
				
				if(count($data) == 0) continue;
				
				$array[$key]['categorie'] = 'Forum';
				$array[$key]['title'] = $data['title'];
				$array[$key]['url'] = site_url('forum/'.$data['id'].'/'.url_title($data['title']));
			}
			elseif($line['table'] == 'events')
			{
				$data = $this->db->select('id,name')
				->where('id', $line['idData'])
				->get('events')
				->row_array();
				
				if(count($data) == 0) continue;
				
				$array[$key]['categorie'] = 'Evènement';
				$array[$key]['title'] = $data['name'];
				$array[$key]['url'] = site_url('evenement/'.$data['id'].'/'.url_title($data['name']));
			}
	
			$nb++;
			$array[$key]['id'] = $line['id'];
			$array[$key]['user'] = $line['user'];
			$array[$key]['username'] = $line['username'];
			$array[$key]['date'] = $line['date'];
			$array[$key]['color'] = $line['color'];
			$array[$key]['message'] = $line['message'];
		}
		
		return $array;
	}
    
    function getComments($table, $page, $idData = null, $nbMessageParPages)
    {
        return $this->db->select('c.id,c.user,c.message,c.date,u.username,u.color,u.group')
        ->where('table', $table)
        ->where('idData', $idData)
        ->join('users u','u.id = c.user')
        ->order_by('c.date asc')
        ->limit($nbMessageParPages, $page)
        ->get('comments c')
        ->result_array();
    }
    
    function addComment($idData, $table, $message)
    {
        if($idData == '') $idData = null;
        
        if(!in_array($table, array('contents', 'events','messaging','forum')))
        return;
		
		$idSite = null;
		$blocked = false;
		if($table == 'contents')
		{
			$res = $this->db->get_where('sites', array('url' => base_url()))->row_array();
			$idSite = $res['id'];
		}
		elseIf($table == 'forum')
		{
			$this->load->model('forums');
			$blocked = $this->forums->isBlocked($idData);
			$this->input->set_cookie('topic_'.$idData, time(), 50000000);
		}
        
		if($blocked) return false;
		
        $this->db->insert('comments', array('table' => $table, 
            'idData' => $idData, 
            'user' => $this->session->userdata('id'), 
            'site' => $idSite, 
            'date' => date('Y-m-d H:i:s'),
            'message' => $message));
			
		$this->db->set('nbPost','nbPost+1',false)->where('id',$this->session->userdata('id'))->update('users');
		
		if($table == 'contents')
		{
			$this->db->set('nbComment','nbComment+1',false)->where('idCreation',$idData)->update('contents');
			$this->users->gils(10);
		
		}
		else if($table == 'messaging')
		{
			$this->users->gils(2);
			$this->db->set('lastDate',date('Y-m-d H:i:s'))->where('id',$idData)->update('messaging');
			
			// notifier tout le monde
			$this->db->set('notif',1)->where('messaging',$idData)->where('user !=',$this->session->userdata('id'))->update('messaging_users');		
			
			// ajouter +1  MP a tout le monde
			$users = $this->db->select('user')->where('messaging', $idData)->get('messaging_users')->result_array();
			foreach($users as $user)
			{
				if($user['user'] != $this->session->userdata('id'))
				$this->db->set('nbMP','nbMP+1',false)->where('id',$user['user'])->update('users');
			}
		}
		else if($table == 'forum')
		{
			$this->users->gils(5);
			$this->db->set('nbPost','nbPost+1',false)->set('lastDate',date('Y-m-d H:i:s'))->where('id',$idData)->update('topics');
		}
		
    }
    
    function nbComm($table,$idData)
    {
         return $this->db->where('table', $table)->where('idData', $idData)->count_all_results('comments');
    }
    
     
    function edit($text, $idComm)
    {
        $this->db->update('comments', array('message' => $text), 'id = '.$idComm.' and user='.$this->session->userdata('id'));
    }
    
    function getSource($id)
    {
        $res = $this->db->select('message')->get_where('comments', 'id = '.$id)->row_array();
            
        echo $res['message'];
    }
}