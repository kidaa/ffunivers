<?php
class Chatboxs extends CI_Model 
{
    function __construct()
    {
        parent::__construct();
    }

	function add($message, $toUser)
	{
		$room = null;
		if($toUser == null)
		$room = $this->session->userdata('currentRoom');
		
		$this->db->insert('chatbox_messages', array(
			'toUser' => $toUser,
			'room' => $room,
			'user' =>  $this->session->userdata('id'),
			'message' => $message,
			'date' => date('Y-m-d H:i:s')			
		));
	}
	
	function roomExist($roomName)
	{
		if($this->db->where('name',$roomName)->count_all_results('chatbox_rooms') == 1)
		return true;
		else
		return false;
	}
	
	function createRoom($roomName, $password)
	{
		$this->db->insert('chatbox_rooms', array(
			'name' => $roomName,
			'password' => $password,
			'date' => date('Y-m-d H:i:s'),
			'user' =>  $this->session->userdata('id')
		));
	}
	
	function getRoom($idRoom)
	{
		return $this->db->select('name,password,user')
		->where('id',$idRoom)
		->get('chatbox_rooms')
		->row_array();
	}
	
	function getRooms()
	{
		return $this->db->select('id,name,password,user')
		->order_by('id')
		->get('chatbox_rooms')
		->result_array();
	}
	
	function getHistory($page)
	{
		if($this->session->userdata('currentRoom') == false)
		$this->session->set_userdata('currentRoom',1);		
		
		$limit_start = ($page-1)*200;
		
		return $this->db->select('c.id,c.toUser,c.user,c.message,c.date, c.isDeleted, u.username, u.color, u2.username AS username2, u2.color AS color2 ')
		->join('users u','u.id = c.user')
		->join('users u2','u2.id = c.toUser','left')
		->order_by('c.id DESC')
		->where('room',$this->session->userdata('currentRoom'))
		->or_where('toUser',$this->session->userdata('id'))
		->or_where('(user = '.$this->session->userdata('id').' AND room IS NULL)')
		->limit(200,$limit_start)
		->get('chatbox_messages c')
		->result_array();
	}
	
	function getMsg()
	{
		return $this->db->select('c.id,c.toUser,c.user,c.message,c.date, c.isDeleted, u.username, u.color, u2.username AS username2, u2.color AS color2 ')
		->join('users u','u.id = c.user')
		->join('users u2','u2.id = c.toUser','left')
		->order_by('c.id DESC')
		->where('room',$this->session->userdata('currentRoom'))
		->or_where('toUser',$this->session->userdata('id'))
		->or_where('(user = '.$this->session->userdata('id').' AND room IS NULL)')
		->limit(60)
		->get('chatbox_messages c')
		->result_array();
	}

}