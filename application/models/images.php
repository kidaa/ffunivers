<?php
class Images extends CI_Model 
{
    function __construct()
    {
        parent::__construct();
    }
	
	function add($data)
	{
		$this->db->insert('images',$data);
		$this->users->gils(5);
	}

	function getMyLastImages($nb = 50, $id = null)
	{
		if($id == null)
		$id = $this->session->userdata('id');
		
		// return $this->db->select('i.id,i.name,i.indexed,i.categorie,i.categorieType')
		// ->join('categories c','c.id = i.categorie')
		// ->join('sites s','s.id = c.site')
		// ->where('i.user', $id )
		// ->where('s.url', base_url())
		// ->order_by('i.date DESC')
		// ->limit($nb)
		// ->get('images i')
		// ->result_array();	
		
		return $this->db->select('i.id,i.name,i.indexed,i.categorie,i.categorieType')
		->where('i.user', $id )
		->order_by('i.date DESC')
		->limit($nb)
		->get('images i')
		->result_array();
	}
	
	function getImagesProfil($nb = 50,$id)
	{
		return $this->db->select('i.name')
		->join('categories c','c.id = i.categorie')
		->join('sites s','s.id = c.site')
		->where('i.user', $id )
		->where('s.url', base_url())
		->order_by('i.date DESC')
		->limit($nb)
		->get('images i')
		->result_array();

	}
	
	// toutes les images qui ont une catégorie qui appartient au site courant
	function loadAjaxImages($page, $cat1, $cat2)
	{
		$nbImagesParPage = 24;
		$limit_start = ($page-1)*$nbImagesParPage;
		
		return $this->db->select('i.id,i.name')
		->join('categories c','c.id = i.categorie')
		->join('sites s','s.id = c.site')
		->where('i.inGallery',1)
		->where('s.url', base_url())
		->where_in('i.categorie', $cat1)
		->where_in('i.categorieType', $cat2)
        ->limit($nbImagesParPage, $limit_start)
        ->order_by('i.id desc')
        ->get('images i')
        ->result_array();
	}
	
	function getNotInGallery()
	{
		return $this->db->select('i.id,i.name,i.date,i.categorie,i.categorieType')
		->join('categories c','c.id = i.categorie')
		->join('sites s','s.id = c.site')
		->where('i.viewed',0)
		->where('i.indexed',1)
		->where('i.inGallery',0)
		->where('s.url', base_url())
		->where('i.categorie IS NOT NULL',null,false)
		->where('i.categorieType IS NOT NULL',null,false)
		->order_by('i.id ASC')
		->get('images i')
		->result_array();
	}
	
	function getInGallery($page)
	{
		return $this->db->select('i.id,i.name,i.date,i.categorie,i.categorieType')
		->join('categories c','c.id = i.categorie')
		->join('sites s','s.id = c.site')
		->where('i.inGallery',1)
		->where('s.url', base_url())
		->order_by('i.id DESC')
		->limit(30, $page)
		->get('images i')
		->result_array();
	}
	

	function nbInGallery()
	{
		return $this->db->select('i.id,i.name,i.date,i.categorie,i.categorieType')
		->join('categories c','c.id = i.categorie')
		->join('sites s','s.id = c.site')
		->where('i.inGallery',1)
		->where('s.url', base_url())
		->count_all_results('images i');
	}
}