<?php
class Events extends CI_Model 
{
	function getEventsHome($limit = 5)
	{
		return $this->db->select('id, date, name')
		->where('date >',date('Y-m-d H:i:s'))
		->where('isValid',1)
		->order_by('date ASC')
		->limit($limit)
		->get('events')
		->result_array();
	}
	
	function getPreviousEvents($limit = 5)
	{
		return $this->db->select('id, date, name')
		->where('date <',date('Y-m-d H:i:s'))
		->where('isValid',1)
		->order_by('date DESC')
		->limit($limit)
		->get('events')
		->result_array();
	}
	
	function getEvent($id)
	{
		return $this->db->select('e.date, e.name, e.user, u.username, u.color')
		->join('users u','u.id = e.user')
		->where('e.id', $id)
		->get('events e')
		->row_array();
	}
	
	function eventExist($id)
	{
		if($this->db->where('id',$id)->count_all_results('events') == 1)
		return true;
		else
		return false;
	}
	
	function getAll()
	{
		return $this->db->select('e.id,e.isValid, e.date, e.name, e.user, u.username, u.color')
		->join('users u','u.id = e.user')
		->order_by('e.date DESC')
		->get('events e')
		->result_array();
	}
}