<?php
class Contents extends CI_Model 
{
    function __construct()
    {
        parent::__construct();
    }
	
	function getLastUpdate()
	{
		return $this->db->select('c.url,c.title,c.editDate,c.user,u.color,u.username,c.type')
		->join('sites s','s.id = c.site')
		->join('users u','u.id = c.user')
		->where('c.isCurrent', 1)
		->where('s.url', base_url())
		->order_by('c.editDate DESC')
		->limit(7)
		->get('contents c')
		->result_array();
	}
	
	function urlUsed($url, $idCreation)
	{
		$res = $this->db->select('DISTINCT c.idCreation',false)
		->join('sites s','s.id = c.site')
		->where('s.url', base_url())
		->where('c.url', $url)
		->where('c.idCreation !=', $idCreation)
		->get('contents c')
		->result_array();
		
		if(count($res) >= 1)
		return true;
		else
		return false;
	}
	
	function getRSSNews()
	{
		return $this->db->select('c.id,c.user,c.date,c.title,c.content,u.username,c.url')
		->join('users u','u.id = c.user')
		->where('c.isCurrent', 1)
		->where('c.type', 1)
		->order_by('c.date DESC')
		->limit(20)
		->get('contents c')
		->result_array();
	}
	
	function getById($idContent)
	{
		return $this->db->get_where('contents', array('id' => $idContent))->row_array();
	}
	
	function getContentTypes()
	{
		return $this->db->get_where('content_types', array('text' => 1))->result_array();
	}
	
	function getCategories()
	{
		return $this->db->select('c.id,c.name,c.content_type')
		->join('sites s','s.id = c.site','left')
		->where('s.url', base_url())
		->or_where('s.url', null)
		->get('categories c')
		->result_array();	
	}
	
	function getSites()
	{
		return $this->db->get('sites')->result_array();
	}
	
	function getIdSite()
	{
		$res = $this->db->get_where('sites', array('url' => base_url()))->row_array();
		
		return $res['id'];
	}
	
	function getTitleSite()
	{
		$res = $this->db->get_where('sites', array('url' => base_url()))->row_array();
		
		return $res['title'];
	}
	
	function getSommaire($id)
	{
		return $this->db->select('title, description, url')
		->where('parent', $id)
		->where('isCurrent', 1)
		->order_by('order ASC')
		->get('contents')
		->result_array();
	}

	
	function getLastVersion($idCreation)
	{
		$res = $this->db->select('version')
		->where('idCreation', $idCreation)
		->order_by('editDate DESC')
		->limit(1)
		->get('contents')
		->row_array();
		
		return $res['version'];
	}
	
	function getContributs($idCreation, $id)
	{
		return $this->db->select('DISTINCT c.user,u.color,u.username',false)
		->join('users u','c.user = u.id')
		->where('c.idCreation', $idCreation)
		->where('c.id <=', $id)
		->order_by('c.editDate DESC')
		->limit(6)
		->get('contents c')
		->result_array();
	}
	
	function setCurrent($id)
	{
		$res = $this->db->select('idCreation,isCurrent,user,gilsGiven')->where('id',$id)->get('contents')->row_array();
		
		if(count($res) != 0)
		{
			if($res['isCurrent'] == 1)
			{
				$this->db->where('id', $id)->set('isCurrent',0)->update('contents');
			}
			else
			{
				if($res['gilsGiven'] == 0)
				$this->users->gils(500,$res['user']);
				
				$this->db->where('idCreation', $res['idCreation'])->where('isCurrent',1)->set('isCurrent',0)->update('contents');
				$this->db->where('id', $id)->set('isCurrent',1)->set('gilsGiven',1)->update('contents');
			}
		}
	}
	
	function getMyContents($id = null,$limit = 6000)
	{
		$admin = false;
		
		
		if($id == null)
		$id = $this->session->userdata('id');
		else if($id == 'admin')
		$admin = true;
		
		$this->db->select('c.idCreation, c.id, c.title,c.headTitle, c.type, ct.name,c.inSlider')
		->join('content_types ct', 'c.type = ct.id')
		->join('sites s', 'c.site = s.id')
		->where('s.url', base_url());
		
		if(!$admin)
		$this->db->where('c.user', $id );
		
		$req = $this->db->order_by('c.editDate DESC')
		->limit($limit)
		->get('contents c')
		->result_array();
		
		// on fait le group by nous meme parce que le order ne fonctionne pas avec le group by
		$idCreations = array();
		$res = array();
		foreach($req as $content)
		{
			if(!in_array($content['idCreation'],$idCreations))
			{
				$res[] = $content;
				$idCreations[] = $content['idCreation'];
			}
		}
		
		return $res;
	}
	
	function getHistoryByIdCreation($idCreations)
	{
		return $this->db->select('c.id, c.idCreation, c.title, c.version,c.editFromVersion, c.linesModified,c.sizeModified, c.user, u.username, u.color, c.isCurrent, c.date, c.editDate,c.content')
		->join('users u', 'c.user = u.id')
		->where_in('c.idCreation', $idCreations)
		->order_by('c.version DESC')
		->get('contents c')
		->result_array();
	}
	
	function getSliderNews()
	{
		$req = $this->db->select('c.idCreation, c.url, c.title, c.image')
		->join('sites s', 'c.site = s.id')
		->where('s.url', base_url())
		->where('c.type',1)
		->where('c.isCurrent',1)
		->where('c.inSlider',1)
		->where('c.image != ','')
		->order_by('c.date DESC')
		->limit(8)
		->get('contents c')
		->result_array();
		
		// on fait le group by nous meme parce que le order ne fonctionne pas avec le group by
		$idCreations = array();
		$res = array();
		foreach($req as $content)
		{
			if(!in_array($content['idCreation'],$idCreations))
			{
				$res[] = $content;
				$idCreations[] = $content['idCreation'];
			}
		}
		
		return $res;
	}
	
	function getHistoryNews($page)
	{
		return $this->db->select('c.url, c.title, c.categorie, c.content, c.date,c.nbComment')
		->join('sites s', 'c.site = s.id')
		->where('c.type',1)
		->where('c.inSlider',0)
		->where('s.url', base_url())
		->where('c.isCurrent',1)
		->order_by('c.date DESC')
		->limit(20, $page)
		->get('contents c')
		->result_array();
	}
	
	function getNbHistoryNews()
	{
		return $this->db->join('sites s', 'c.site = s.id')
		->where('c.type',1)
		->where('c.inSlider',0)
		->where('s.url', base_url())
		->where('c.isCurrent',1)
		->count_all_results('contents c');
	}
	
	function getHomeNews()
	{
		return $this->db->select('c.url, c.title, c.categorie, c.content, c.date,c.nbComment')
		->join('sites s', 'c.site = s.id')
		->where('c.type',1)
		->where('c.inSlider',0)
		->where('s.url', base_url())
		->where('c.isCurrent',1)
		->order_by('c.date DESC')
		->limit(7)
		->get('contents c')
		->result_array();
	}
	
	function getArticles($idCat)
	{
		$this->db->select('c.title,c.url,c.image')
		->join('sites s','s.id = c.site','left')
		->where('c.isCurrent',1)
		->where('c.type',3)
		->where('s.url', base_url());
		
		if($idCat != '*') 
		$this->db->where('c.categorie',$idCat);
		
		return $this->db->order_by('c.date DESC')->get('contents c')->result_array();
	}
	
	function getLastForSide($type)
	{
		return $this->db->select('c.url,c.title')
		->join('sites s','s.id = c.site')
		->where('c.isCurrent',1)
		->where_in('c.type',$type)
		->where('s.url', base_url())
		->order_by('c.date DESC')
		->limit(7)
		->get('contents c')
		->result_array();
	}
	
	function getListVersions($idCreation)
	{
		return $this->db->select('c.id,c.title,c.version, u.username, u.color, c.user,c.isCurrent')
		->join('users u','u.id = c.user')
		->where('c.idCreation',$idCreation)
		->order_by('c.editDate DESC')
		->get('contents c')
		->result_array();
	}
	
		
	function getIdCreationByUrl($url)
	{
		$res = $this->db->select('c.idCreation')
		->join('sites s','s.id = c.site')
		->where('s.url', base_url())
		->where('c.url', $url)
		->limit(1)
		->get('contents c')
		->row_array();
		
		return isset($res['idCreation']) ? $res['idCreation'] : false;
	}
	
	function getInfosForView($idCreation)
	{
		return $this->db->select('c.id,c.idCreation, c.url,c.type,c.categorie,c.title,c.headTitle,c.content,c.redirect,c.nbComment,c.user,c.date,c.editDate,c.description,u.username,u.color,ct.name,cat.name AS categorieName')
		->join('users u','u.id = c.user')
		->join('sites s','s.id = c.site')
		->join('content_types ct','ct.id = c.type')
		->join('categories cat','cat.id = c.categorie','left')
		->where('c.idCreation', $idCreation)
		->where('c.isCurrent', 1)
		->where('s.url', base_url())
		->get('contents c')
		->row_array();
	}
	
	function getInfosForHistory($id)
	{
		return $this->db->select('c.id,c.idCreation, c.url,c.type,c.categorie,c.title,c.headTitle,c.content,c.redirect,c.nbComment,c.user,c.date,c.editDate,c.description,u.username,u.color,ct.name,cat.name AS categorieName')
		->join('users u','u.id = c.user')
		->join('sites s','s.id = c.site')
		->join('content_types ct','ct.id = c.type')
		->join('categories cat','cat.id = c.categorie','left')
		->where('c.id', $id)
		->where('s.url', base_url())
		->get('contents c')
		->row_array();
	}
	
	
	function getTreeNav($parent = null)
	{
		if( $parent == null)
		{
			$parsed = parse_url( base_url() ,PHP_URL_HOST);
			$cache = 'db_cache/tree_'.$parsed;
			
			if(file_exists($cache))
			return unserialize( file_get_contents($cache) );
		}
		
		
		$contents = $this->db->select('c.id, c.parent, c.title, c.user, u.username, c.url, c.idCreation')
			->join('sites s','s.id = c.site')
			->join('users u','c.user = u.id')
			->where('c.parent',$parent)
			->where('c.isCurrent',1)
			->where('c.type',2)
			->where('s.url', base_url())
			->order_by('c.order asc')
			->get('contents c')
			->result_array();

		
		$res = array();

		foreach ($contents as $content)
		{
			$nbChild = $this->db->where('isCurrent',1)->where('type',2)->where('parent',$content['idCreation'])->count_all_results('contents');

			if($nbChild != 0)
			$contentChild = $this->getTreeNav($content['idCreation']);
			else
			$contentChild = null;

			$res[$content['id']]['title'] = $content['title'];
			$res[$content['id']]['url'] = $content['url'];
			$res[$content['id']]['idUser'] = $content['user'];
			$res[$content['id']]['username'] = $content['username'];
			$res[$content['id']]['parent'] = $content['parent'];
			$res[$content['id']]['idCreation'] = $content['idCreation'];
			$res[$content['id']]['child'] = $contentChild;
		   
		}
		
		if( $parent == null)
		file_put_contents($cache,serialize($res));
		
		return $res;
	}
	
	function setHeader($id)
	{
		$headTitle = '';
		$url = '';

		$niv1 = $this->db->select('type,title,parent')->where('id',$id)->get('contents')->row_array();
		$headTitle .= $niv1['title'];
		$url .= $niv1['title'];

		if(isset($niv1['parent']))
		{
			$niv2 = $this->db->select('title,parent')->where('isCurrent',1)->where('type',2)->where('idCreation',$niv1['parent'])->get('contents')->row_array();
			if(isset($niv2['title'])) 
			{
				$headTitle .= ' - '.$niv2['title'];
				$url .= '-'.$niv2['title'];
			}
		}

		if(isset($niv2['parent']))
		{
			$niv3 = $this->db->select('title,parent')->where('isCurrent',1)->where('type',2)->where('idCreation',$niv2['parent'])->get('contents')->row_array();
			if(isset($niv3['title']))
			{
				$headTitle .= ' - '.$niv3['title'];
				$url .= '-'.$niv3['title'];
			} 
		}

		if(isset($niv3['parent']))
		{
			$niv4 = $this->db->select('title,parent')->where('isCurrent',1)->where('type',2)->where('idCreation',$niv3['parent'])->get('contents')->row_array();
			if(isset($niv4['title']))
			{
				$headTitle .= ' - '.$niv4['title'];
				$url .= '-'.$niv4['title'];
			} 
		}

		$url = url_title($url);

		$this->db->set('headTitle',$headTitle)->set('url',$url)->where('id',$id)->update('contents');
	}
	
	function create($data)
	{
		// suppression du dernier retour a la ligne
		$lastChar = mb_substr($data['content'], -1);
		if($lastChar == "\n")
		{
			$data['content'] = mb_substr($data['content'], 0, -1);
		}
		
		$this->db->insert('contents',$data);
		$lastId = $this->db->insert_id();
		
		if($data['idCreation'] == 0)
		{
			$this->db->set('idCreation',$lastId)->where('id',$lastId)->update('contents');
		}

		$this->setHeader($lastId);
		
		return $lastId;
	}
	
	function getInfosForNewVersion($idCreation)
	{
		return $this->db->select('date, nbComment,order,inSlider')->where('idCreation', $idCreation)->order_by('editDate ASC')->limit(1)->get('contents')->row_array();
	}
	
	function getLastOrder($parent)
	{
		$ligne = $this->db->select('order')
		->where('parent',$parent)
		->where('type',2)
		->order_by('order desc')
		->limit(1)
		->get('contents')
		->row_array();
		
		if(!isset($ligne['order']))
		$ligne['order'] = 0;
		
		return ($ligne['order']+1);
	}
	
	
	// function initOrder()
	// {
		// $lignes = $this->db->select('id,idCreation,order,parent')
			// ->order_by('parent asc, idCreation asc, order asc')
			// ->get('contents')
			// ->result_array();
			
		
		// $lastIdCat = null;
		// $lastIdCreation = null;
		// $i = 1;
		// foreach($lignes as $ligne)
		// {
			// if($lastIdCat != $ligne['idCat'])
			// {
				// $lastIdCat = $ligne['idCat'];
				// $i = 1;
			// }
			
			// $this->db->set('order',$i)->where('id',$ligne['id'])->update('contents');
			
			// $i++;
		// }
	// }
	
	function sortTree($idCreation, $direction)
	{
		$infos = $this->db->select('parent,order')
		->where('type',2)
		->where('isCurrent',1)
		->where('idCreation',$idCreation)
		->limit(1)
		->get('contents')
		->row_array();

		$parent = $infos['parent'];
		$actualOrder = $infos['order'];
	
		if($direction == 0)//down
		{
			$ligne = $this->db->select('idCreation,order')
			->where('isCurrent',1)
			->where('type',2)
			->where('parent',$parent)
			->where('order > '.$actualOrder)
			->order_by('order asc')
			->limit(1)
			->get('contents')
			->row_array();
			
			if(isset($ligne['order']))
			{
				$newOrder = $ligne['order'];
			}
			else
			{
				return 'noUpdate';
			}
		}
		else
		{
			$ligne = $this->db->select('idCreation,order')
			->where('isCurrent',1)
			->where('type',2)
			->where('parent',$parent)
			->where('order < '.$actualOrder)
			->order_by('order desc')
			->limit(1)
			->get('contents')
			->row_array();
			
			// exit($parent);	
			
			if(isset($ligne['order']))
			{
				$newOrder = $ligne['order'];
			}
			else
			{
				return 'noUpdate';
			}
		}
		
		if(isset($ligne['order']))
		$this->db->set('order',$actualOrder)->where('idCreation',$ligne['idCreation'])->update('contents');
		
		$this->db->set('order',$newOrder)->where('idCreation',$idCreation)->update('contents');
		
		return $newOrder;
	}
}