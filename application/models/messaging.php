<?php
class Messaging extends CI_Model 
{
    function __construct()
    {
        parent::__construct();
    }
	
	function newConvers($title,$usernames)
	{
		if(trim($title) == '')
		$title = 'Conversation sans titre';
		
		$this->db->insert('messaging', array('title' => $title));	
		$idConvers = $this->db->insert_id();
		
		
		$this->db->insert('messaging_users', array('messaging' =>  $idConvers, 'user' => $this->session->userdata('id')));
		$this->addUsersToConvers($usernames, $idConvers);
		
		return $idConvers;
	}
	
	function addUsersToConvers($usernames, $idConvers)
	{
		// on verifie si on est deja dans la convers
		if(!$this->iAmInConvers($idConvers))
		return false;
		
		$users = explode(',', $usernames);
		
		foreach($users as $username)
		{
			$res = $this->db->select('id')->where('username',$username)->get('users')->row_array();
			
			//user existe
			if(isset($res['id']) && $res['id'] != $this->session->userdata('id'))
			{
				// pas deja dans la convers
				if($this->db->where('user',$res['id'])->where('messaging',$idConvers)->count_all_results('messaging_users') == 0)
				{
					$this->db->insert('messaging_users', array('messaging' =>  $idConvers, 'user' =>  $res['id'], 'notif' => 1));
				}
			
			}
		}
	}
	
	function getTitle($id)
	{
		$res = $this->db->select('title')->where('id', $id)->get('messaging')->row_array();
		return $res['title'];
	}
	
	function getUsersInConvers($id)
	{
		return $this->db->select('mu.user,u.username,u.color,mu.notif')
		->join('users u','mu.user = u.id')
		->where('mu.messaging', $id)
		->get('messaging_users mu')
		->result_array();	
	}
	
	function getMyConvers()
	{
		return $this->db->select('m.id,m.title,m.lastDate,mu.notif')
		->join('messaging_users mu','mu.messaging = m.id')
		->where('mu.user', $this->session->userdata('id'))
		->order_by('mu.notif DESC')
		->order_by('m.lastDate DESC')
		->get('messaging m')
		->result_array();	
	}
	
	function getUsernamesMyConvers($arrayIdMyConvers)
	{
		return $this->db->select('mu.user,mu.messaging,u.username,u.color,mu.notif')
		->join('users u','mu.user = u.id')
		->where_in('mu.messaging', $arrayIdMyConvers)
		->get('messaging_users mu')
		->result_array();	
	}
	
	function iAmInConvers($idConvers)
	{
		if($this->db->where('user',$this->session->userdata('id'))->where('messaging',$idConvers)->count_all_results('messaging_users') != 1)
		return false;
		else
		return true;
	}
}