<?php
class Forums extends CI_Model 
{
	function getTopics($limit = 60, $filtre = 'all')
	{
		$this->db->select('t.id,t.title, t.user, t.sondage, t.lastDate, t.blocked, t.nbPost, t.epingled, u.username, u.color')
		->join('users u', 'u.id = t.user')
		->order_by('t.epingled DESC, t.lastDate DESC')
		->limit($limit);
		
		if($filtre == 'my')
		$this->db->where('user', $this->session->userdata('id'));

		return $this->db->get('topics t')
		->result_array();
	}
	
	function getLastTopics($limit = 60)
	{
		return $this->db->select('id,title')
		->order_by('lastDate DESC')
		->limit($limit)
		->get('topics')
		->result_array();
	}
	
	function getTopic($id)
	{
		return $this->db->select('id,title,sondage,blocked,epingled')->where('id', $id)->get('topics')->row_array();
	}
	
	function getCurrentSondage()
	{
		return $this->db->select('id,title')
		->where('isHome', 1)
		->where('sondage', 1)
		->limit(1)
		->get('topics')
		->row_array();
	}
	
	function getResponses($id)
	{
		return $this->db->select('id,response,nbVote')->where('topic', $id)->get('sondage')->result_array();
	}
	
	function addTopic($data,$reponse)
	{
		$this->db->insert('topics',$data);
		$id = $this->db->insert_id();
		
		if($data['sondage'] == 1)
		{
			$this->users->gils(20);
			foreach($reponse as $rep)
			{
				$this->db->insert('sondage', array('topic' => $id, 'response' => $rep));
			}
		}
		else
		{
			$this->users->gils(15);
		}
		
		return $id;
	}
	
	function topicExist($id)
	{
		if($this->db->where('id',$id)->count_all_results('topics') == 1)
		return true;
		else
		return false;
	}
	
	function responseExist($idSondage, $idResponse)
	{
		if($this->db->where('topic',$idSondage)->where('id',$idResponse)->count_all_results('sondage') == 1)
		return true;
		else
		return false;
	}
	
	function isBlocked($id)
	{
		if($this->db->where('blocked',1)->where('id',$id)->count_all_results('topics') == 1)
		return true;
		else
		return false;
	}
	
	function isEpingled($id)
	{
		if($this->db->where('epingled',1)->where('id',$id)->count_all_results('topics') == 1)
		return true;
		else
		return false;
	}
}