<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	
	public $colors = array("Black"			 => "#000000",
	"Indian Red" 		 => "#CD5C5C",
		"Light Coral" 		 => "#F08080",
		"Salmon" 		 => "#FA8072",
		"Dark Salmon" 		 => "#E9967A",
		"Light Salmon" 		 => "#FFA07A",
		"Crimson" 		 => "#DC143C",
		"Red" 			 => "#FF0000",
		"Fire Brick" 		 => "#B22222",
		"Dark Red" 		 => "#8B0000",
		
		"Pink" 			 => "#FFC0CB",
		"Light Pink" 		 => "#FFB6C1",
		"Hot Pink" 		 => "#FF69B4",
		"Deep Pink" 		 => "#FF1493",
		"Medium Violet Red" 	 => "#C71585",
		"Pale Violet Red" 	 => "#DB7093",
		
		"Light Salmon" 		 => "#FFA07A",
		"Coral" 		 => "#FF7F50",
		"Tomato" 		 => "#FF6347",
		"Orange Red" 		 => "#FF4500",
		"Dark Orange" 		 => "#FF8C00",
		"Orange" 		 => "#FFA500",
		
		"Gold" 			 => "#FFD700",
		"Dark Khaki" 		 => "#BDB76B",
				
		"Thistle" 		 => "#D8BFD8",
		"Plum" 			 => "#DDA0DD",
		"Violet" 		 => "#EE82EE",
		"Orchid" 		 => "#DA70D6",
		"Fuchsia" 		 => "#FF00FF",
		"Magenta" 		 => "#FF00FF",
		"Medium Orchid" 	 => "#BA55D3",
		"Medium Purple" 	 => "#9370DB",
		"Blue Violet" 		 => "#8A2BE2",
		"Dark Violet" 		 => "#9400D3",
		"Dark Orchid" 		 => "#9932CC",
		"Dark Magenta" 		 => "#8B008B",
		"Purple" 		 => "#800080",
		"Indigo" 		 => "#4B0082",
		"Slate Blue" 		 => "#6A5ACD",
		"Dark Slate Blue" 	 => "#483D8B",
		
		"Green Yellow" 		 => "#ADFF2F",
		"Chartreuse" 		 => "#7FFF00",
		"Lawn Green" 		 => "#7CFC00",
		"Lime" 			 => "#00FF00",
		"Lime Green" 		 => "#32CD32",
		"Pale Green" 		 => "#98FB98",
		"Light	Green" 		 => "#90EE90",
		"Medium Spring Green" 	 => "#00FA9A",
		"Spring Green" 		 => "#00FF7F",
		"Medium Sea Green" 	 => "#3CB371",
		"Sea Green" 		 => "#2E8B57",
		"Forest Green" 		 => "#228B22",
		"Green" 		 => "#008000",
		"Dark Green" 		 => "#006400",
		"Yellow Green" 		 => "#9ACD32",
		"Olive Drab" 		 => "#6B8E23",
		"Olive" 		 => "#808000",
		"Dark Olive Green" 	 => "#556B2F",
		"Medium Aquamarine" 	 => "#66CDAA",
		"Dark Sea Green" 	 => "#8FBC8F",
		"Light Sea Green" 	 => "#20B2AA",
		"Dark Cyan" 		 => "#008B8B",
		"Teal" 			 => "#008080",
		
		"Aqua" 			 => "#00FFFF",
		"Cyan" 			 => "#00FFFF",
		"Aquamarine" 		 => "#7FFFD4",
		"Turquoise" 		 => "#40E0D0",
		"Medium Turquoise" 	 => "#48D1CC",
		"Dark Turquoise" 	 => "#00CED1",
		"Cadet Blue" 		 => "#5F9EA0",
		"Steel Blue" 		 => "#4682B4",
		"Light Steel Blue" 	 => "#B0C4DE",
		"Powder Blue" 		 => "#B0E0E6",
		"Light Blue" 		 => "#ADD8E6",
		"Sky Blue" 		 => "#87CEEB",
		"Light Sky Blue" 	 => "#87CEFA",
		"Deep Sky Blue" 	 => "#00BFFF",
		"Dodger Blue" 		 => "#1E90FF",
		"Cornflower Blue" 	 => "#6495ED",
		"Medium Slate Blue" 	 => "#7B68EE",
		"Royal Blue" 		 => "#4169E1",
		"Blue" 			 => "#0000FF",
		"Medium Blue" 		 => "#0000CD",
		"Dark Blue"		 => "#00008B",
		"Navy" 			 => "#000080",
		"Midnight Blue" 	 => "#191970",
				
		"Burly Wood" 		 => "#DEB887",
		"Tan" 			 => "#D2B48C",
		"Rosy Brown" 		 => "#BC8F8F",
		"Sandy Brown" 		 => "#F4A460",
		"Goldenrod" 		 => "#DAA520",
		"Dark Goldenrod" 	 => "#B8860B",
		"Peru" 			 => "#CD853F",
		"Chocolate" 		 => "#D2691E",
		"Saddle Brown" 		 => "#8B4513",
		"Sienna" 		 => "#A0522D",
		"Brown" 		 => "#A52A2A",
		"Maroon" 		 => "#800000",
				
		"Silver"		 => "#C0C0C0",
		"Dark Gray"		 => "#A9A9A9",
		"Gray"			 => "#808080",
		"Dim Gray"		 => "#696969",
		"Light Slate Gray"	 => "#778899",
		"Slate Gray"		 => "#708090",
		"Dark Slate Gray"	 => "#2F4F4F"
	);
	
	
	public function index($categorie = 'home',$idConvers = 0, $mpTo = '')
	{
		$this->load->model('images');
		$data['myImages'] = $this->images->getMyLastImages(100);
		$data['categories'] = $this->contents->getCategories();
		$data['sites'] = $this->contents->getSites();
		
		$data['current'] = 'account';
		$data['nbImageToIndexed'] = 0;
		foreach($data['myImages'] as $img)
		if($img['indexed'] == 0) $data['nbImageToIndexed'] ++;
		
		
		if($categorie == 'home')
		{
			$data['title'] = 'Mon profil';
			
			$data['colors'] = $this->colors;
			$data['infos'] = $this->users->getInfosAccount( $this->session->userdata('id'));
		
		}
		
		
		if($categorie == 'viewMessaging')
		{
			$this->load->model('messaging');
		
			if($this->session->userdata('id') != 1)
			{
				if(!$this->messaging->iAmInConvers($idConvers))
				{
					redirect();
				}
			}
			
			$data['idConvers'] = $idConvers;
			$this->db->set('notif',0)->where('notif',1)->where('user', $this->session->userdata('id'))->where('messaging', $idConvers)->update('messaging_users');
		
			$data['title'] = $this->messaging->getTitle($idConvers);
			$data['usersInConvers'] = $this->messaging->getUsersInConvers($idConvers);
			
			$this->load->helper('comments');
			$_POST = array('table' => 'messaging','idData' => $idConvers);
			$data['commentaires'] = displayCommentaires();
			
			
		}
		
		
		
		
		
		if($categorie == 'messagerie')
		{
			$this->db->set('nbMP',0)->where('id', $this->session->userdata('id'))->update('users');
			
			$data['title'] = 'Ma messagerie';
			
			$this->load->model('messaging');
			
			$data['myConvers'] = $this->messaging->getMyConvers();
			$data['mpTo'] = $mpTo;
			
			$listId = array();
			foreach($data['myConvers'] as $convers)
			$listId[] = $convers['id'];
			
			if(count($listId) != 0)
			$data['usernamesMyConvers'] = $this->messaging->getUsernamesMyConvers($listId);
			
		
		}
		
		if($categorie == 'images')
		{
			$data['title'] = 'Mes images';

			
		}

		if($categorie == 'trierImages')
		{
			$data['title'] = 'Trier mes images';
			
			
		}
		
		if($categorie == 'ajouterImages')
		{
			$data['title'] = 'Ajouter des images';
			
			
		}
		
		
	

		
		if($categorie == 'contributions')
		{
			$data['title'] = 'Mes contributions';
			
			$data['type_contents'] = $this->contents->getContentTypes();
			$myContents = $this->contents->getMyContents();
	
			
			$idCreationContents = array(null);
			foreach($myContents as $content)
			$idCreationContents[] = $content['idCreation'];
		
			$historyContents = $this->contents->getHistoryByIdCreation($idCreationContents);
			
			foreach($myContents as $key => $content)
			{
				$myContents[$key]['history'] = array();
				
				foreach($historyContents as $history)
				{
					if($history['idCreation'] == $content['idCreation'])
					$myContents[$key]['history'][] = $history;
				}
			}
			
			$data['myContents'] = $myContents;
			
		}
		
		if($categorie == 'gils')
		{
			$data['title'] = 'Mes gils';
		
		}
		
		$data['categorie'] = $categorie;
		$data['view'] = 'homeUser';
		
		$this->load->view('design.php',$data);
	}
	
	function mapMembers()
	{
		$data['users'] = $this->users->getInfosForMap();
		
		$data['title'] = 'Carte des membres';
		$data['view'] = 'map';
		$data['current'] = 'mapMembers';
		
		$this->load->view('design.php',$data);
	}
	
	function listeMembers($sort = 1, $page = 0)
	{
		if($sort == 1) $sortBy = 'gils desc';
		elseif($sort == 2) $sortBy = 'nbPost desc';
		elseif($sort == 3) $sortBy = 'nbPostChatbox desc';
		elseif($sort == 4) $sortBy = 'lastDate desc';
		elseif($sort == 5) $sortBy = 'nbConnection desc';
		elseif($sort == 6) $sortBy = 'registerDate desc';
		else $sortBy = 'username';

		$data['sort'] = $sort;
		$data['current'] = 'listMembers';
		$data['title'] = 'Liste des membres';
		$data['view'] = 'listeMembers';
		
		$data['team'] = $this->users->getTeam();
		$data['members'] = $this->users->getMembers($page,$sortBy);
		
		$data['nbMembers'] = $this->users->getNbMembers();
		
		$this->load->library('pagination');

		$config['total_rows'] = $data['nbMembers'];
		$config['per_page'] = 14;
		$config['uri_segment'] = $this->uri->total_segments();
		$config['base_url'] = site_url('listeDesMembres/'.$sort);
		$config['num_links'] = 6;
		$config['first_link'] = 'Début';
		$config['last_link'] = 'Fin';
		$config['next_link'] = 'Suivante »';
		$config['prev_link'] = '« Précédente';
		$config['full_tag_open'] = '<div class="pagination">';
		$config['full_tag_close'] = '</div>';
		$config['cur_tag_open'] = ' <span class="curr">';
		$config['cur_tag_close'] = '</span>';

		$this->pagination->initialize($config);
		
		$this->load->view('design.php',$data);
	}
	
	function newConvers()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('title', 'titre', 'max_length[80]');
		$this->form_validation->set_rules('usernames', 'champ des participants', 'required|callback_checkUsernames');
        $this->form_validation->set_rules('message', 'message', 'required');
	
		$formValid = $this->form_validation->run();
		
		$errors = $this->form_validation->error_array();
	
		if($formValid)
		{
			$errors['callback'] = 'reload';
			
			$this->load->model('messaging');
			$this->load->model('comments');
			$id = $this->messaging->newConvers($this->input->post('title'), $this->input->post('usernames'));
			$this->comments->addComment($id, 'messaging', $this->input->post('message'));
		}

		
		echo json_encode($errors);
	}
	
	function addToConvers()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('usernames', 'champ des participants', 'required|callback_checkUsernames');
	
		$formValid = $this->form_validation->run();
		
		$errors = $this->form_validation->error_array();
	
		if($formValid)
		{
			$errors['callback'] = 'reload';
			
			$this->load->model('messaging');
			$id = $this->messaging->addUsersToConvers($this->input->post('usernames'), $this->input->post('id'));
		}

		
		echo json_encode($errors);
	}
	
	function checkUsernames($usernames)
	{
		$users = explode(',', $usernames);
		
		foreach($users as $username)
		{
			if(!$this->users->isUsernameUsed($username))
			{
				$this->form_validation->set_message('checkUsernames', 'Le pseudo '.$username.' n\'existe pas.');
				return false;
			}
		}
		
		return true;
	}
	
	function deleteConvers($id)
	{
		$this->db->where('user', $this->session->userdata('id'))->where('messaging', $id)->delete('messaging_users');
	}
	
	function updateProfil()
	{
		$date = explode('/', $this->input->post('birth'));
		$jj = '00';
		$mm = '00';
		$aaaa = '0000';
		if(count($date) == 3)
		{
			$jj = intval($date[0]);
			$mm = intval($date[1]);
			$aaaa = intval($date[2]);
		}
		
		$data['mail'] = $this->input->post('mail');
		$data['birth'] = $aaaa.'/'.$mm.'/'.$jj;
		$data['sexe'] = toBool($this->input->post('sexe'));
		$data['urlWeb'] = $this->input->post('urlWeb');
		$data['description'] = $this->input->post('description');
		
		$data['address'] = $this->input->post('address');
		$data['postal'] = $this->input->post('postal');
		$data['city'] = $this->input->post('city');
		$data['country'] = $this->input->post('country');
		$data['firstName'] = $this->input->post('firstName');
		$data['lastName'] = $this->input->post('lastName');
		$data['idPSN'] = $this->input->post('idPSN');
		$data['idXboxlive'] = $this->input->post('idXboxlive');
		$data['onMap'] = $this->input->post('onMap');
		
		$data['mailNotif'] = toBool($this->input->post('mailNotif'));
		$data['chatboxFixed'] = toBool($this->input->post('chatboxFixed'));
		$this->session->set_userdata('chatboxFixed', $data['chatboxFixed'] );
		
		if(in_array($this->input->post('color'), $this->colors))
		$data['color'] = $this->input->post('color');
		
		
		
		if(trim($this->input->post('mdp1')) != '')
		{
			if($this->input->post('mdp1') !=  $this->input->post('mdp2'))
			{
				$errors['success'] = 'Vos mot de passes ne sont pas identiques.';
			}
			else
			{
				$data['password'] = $this->input->post('mdp1');
			}
		}
		
		
		if(!isset($errors))
		{
			$errors['callback'] = 'reload';
			$this->users->update($data);		
		}
		
		echo json_encode($errors);
		
	}
	
	function profil($id)
	{
		$this->load->model('images');
		
		if(!$this->users->idExist( $id))
		redirect();
		
		$data['infos'] = $this->users->getInfosAccount( $id);
		$data['contents'] = $this->contents->getMyContents( $id, 50);
		$data['images'] = $this->images->getImagesProfil(50, $id);
		$data['title'] = $data['infos']['username'];
		$data['view'] = 'profil';
		
		$this->load->view('design.php',$data);
	}

	function closeFollow()
	{
		$this->input->set_cookie('closeFollow', 1, 2592000);
	}
	
	function logout()
	{
		$this->db->set('online',0)
		->where('id', $this->session->userdata('id'))
		->update('users');
		
		$this->session->set_userdata('id',false);
		$cookie['name'] = 'autologin';
		$cookie['expire'] = '';
		$this->input->set_cookie($cookie);
		
		redirect();
	}
	
	
	function uploadAvatar()
	{
		//suppression ancien avatar
		$path1 = 'public/images_upload/avatars/'.$this->session->userdata('id').'.jpg';
		$path2 = 'public/images_upload/avatars/'.$this->session->userdata('id').'.png';
		$path3 = 'public/images_upload/mini_avatars/'.$this->session->userdata('id').'.jpg';
		$path4 = 'public/images_upload/mini_avatars/'.$this->session->userdata('id').'.png';
		
		if(file_exists($path1)) unlink($path1);
		if(file_exists($path2)) unlink($path2);
		if(file_exists($path3)) unlink($path3);
		if(file_exists($path4)) unlink($path4);
	
	
		if(in_array($_FILES['avatar']['type'], array('image/jpeg','image/png')) && $_FILES['avatar']['size'] < 5000000)
		{
			$ext = substr(strrchr(basename($_FILES['avatar']['name']), "."), 1);
			$name = strtolower($this->session->userdata('id').'.'.$ext);
			
			if(move_uploaded_file($_FILES['avatar']['tmp_name'], 'public/images_upload/avatars/'.$name))
			{
				$config['source_image'] = 'public/images_upload/avatars/'.$name;
				$config['quality'] = '100%';
				$config['width'] = 100;
				$config['height'] = 100;
				
				$this->load->library('image_lib', $config);			
				$this->image_lib->resize();
				
				
				$config['source_image'] = 'public/images_upload/avatars/'.$name;
				$config['new_image'] = 'public/images_upload/mini_avatars/'.$name;
				$config['quality'] = '100%';
				$config['width'] = 60;
				$config['height'] = 60;
				
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
			}
		}
		
		redirect('monCompte');
	}
}