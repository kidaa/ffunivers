<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller {

	function index($id)
	{
		$this->load->model( array('events','comments') );
		if(!$this->events->eventExist($id))
		redirect();
		
		$data['events'] = $this->events->getEventsHome(14);
		$data['infos'] = $this->events->getEvent($id);
		
		$data['view'] = 'viewEvent';
		$data['title'] = $data['infos']['name'];
		
		$this->load->helper('comments');
		$_POST = array('table' => 'events','idData' => $id);
		$data['commentaires'] = displayCommentaires();
		
		$this->load->view('design.php',$data);
	}
	
	function listEvents()
	{
		$this->load->model('events' );
		$data['nextEvents'] = $this->events->getEventsHome(50);
		$data['previousEvents'] = $this->events->getPreviousEvents(50);
		
		$data['title'] = 'Evènements';
		$data['view'] = 'listEvents';
		$data['current'] = 'listEvent';
		
		$this->load->view('design.php',$data);
	}
	
	function add()
	{
		
		$name = $this->input->post('name');
		$stringDate = $this->input->post('date');
		
		list($date, $time) = explode(' ', $stringDate);
		list($day, $month, $year) = explode('/', $date);
		list($hour, $minute) = explode(':', $time);

		$timestamp = mktime($hour, $minute, 0, $month, $day, $year);
		
		if(trim($name) != '')
		{
			$this->db->insert('events', array(
				'name' => $name,
				'date' => date('Y-m-d H:i:s',$timestamp),
				'user' => $this->session->userdata('id')
			));
			
			$this->users->gils(50);
			
			echo json_encode( array('callback' => 'reload') );
		}
	}
	
	function deleteEvent($id)
	{
		if($this->session->userdata('group') > 1)
		{
			$this->db->where('id', $id)->delete('events');
		}
	}
	
	function editEvent()
	{
		if($this->session->userdata('group') > 1)
		{	
			$stringDate = $this->input->post('date');
			
			list($date, $time) = explode(' ', $stringDate);
			list($day, $month, $year) = explode('/', $date);
			list($hour, $minute) = explode(':', $time);

			$timestamp = mktime($hour, $minute, 0, $month, $day, $year);
			
			if($this->input->post('isValid') == 1)
			$this->users->gils(200,$res['user']);
			
			$this->db->where('id', $this->input->post('id'))
			->set('name', $this->input->post('name'))
			->set('date', date('Y-m-d H:i:s',$timestamp))
			->set('isValid', $this->input->post('isValid'))
			->update('events');
			
		}
	}
}