<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {

	function index()
	{
		$data['title'] = 'Rechercher sur le site';
		$data['current'] = 'search';
		$data['view'] = 'search';
		
		$this->load->view('design.php', $data);
	}
	
	function doSearch()
	{
		if(trim($_POST['keywords']) == '' || $_POST['keywords'] ==  'Mots clefs de votre recherche') return;
 		$keywords = $_POST['keywords'];
		$array_keywords = explode(' ', $keywords);
		$typeSearch = $_POST['typeSearch'];
		$title = $_POST['title'];
		@$cat1 = $_POST['cat1'];
		@$cat2 = $_POST['cat2'];
		$sondage = $_POST['sondage'];
		$sort = $_POST['sort'];
		$author = trim($_POST['author']);

		if($this->session->userdata('lastSearch') != $keywords)
		{

			$this->db->insert('search', array(
				'user' => $this->session->userdata('id'),
				'site' => $this->contents->getIdSite(),
				'keywords' => $keywords,
				'date' => date('Y-m-d H:i:s')
				));

				$this->session->set_userdata('lastSearch', $keywords);
		}

		
		if(!is_array($cat1))
			$cat1= array();

		// contenu
		if($_POST['where'] == 1)
		{			
			//requete
			$this->db->select('c.id,c.idCreation,c.title,c.date,c.editDate,c.url,c.content,ct.name')
			->join('sites s','s.id = c.site')
			->join('content_types ct','ct.id = c.type')
			->where('c.isCurrent',1)
			->where_in('c.type',$cat1);
			
			if($typeSearch == 1) // phrase exacte
			{
				if($title == 1) $this->db->like('c.title',$keywords);
				else $this->db->like('c.content',$keywords);
			}
			else // tous les mots
			{
				if($title == 1)
				{
					foreach($array_keywords as $keyword)
					$this->db->like('c.title',$keyword);
				}
				else
				{
					foreach($array_keywords as $keyword)
					$this->db->like('c.content',$keyword);
				}
				
			}
			
			$res = $this->db->get('contents c')->result_array();
			
			foreach($res as $key => $line)
			{
				$res[$key]['timestamp'] = datetime2timestamp($line['editDate']);
				$res[$key]['occurence'] = 0;
				
				if($typeSearch == 1) // phrase exacte
				{
					$res[$key]['occurence'] += countWord($keywords, $line['title']);
					$res[$key]['occurence'] += countWord($keywords, $line['content']);
				}
				else
				{
					foreach($array_keywords as $keyword)
					{
						$res[$key]['occurence'] += countWord($keyword, $line['title']);
						$res[$key]['occurence'] += countWord($keyword, $line['content']);
					}
				}
			}
			
			if($sort == 1)
			usort($res, 'sortByOccurence');
			else
			usort($res, 'sortByTimestamp');

			//affichage des resultats
			echo '<h1>'.count($res).' résultats</h1>';
			echo '<ul class="classic">';
			foreach($res as $line)
			{
				echo '<li><a href="'.$line['url'].'" class="resultContent">['.$line['name'].'] '.htmlspecialchars($line['title']).'</a>';
				echo '<span>'.$line['occurence'].' occurence'.( $line['occurence'] > 1 ? 's' : '' ).' - édité '.madate($line['editDate']).'</span>';
					
				echo '</li>';
			}
			echo '<ul>';
			
			// var_dump($res);
			// echo $this->db->last_query();
		}
	}
}