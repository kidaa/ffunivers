<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forum extends CI_Controller {
	
	function index($filtre = 1)
	{
		$this->load->model('forums');

		if($filtre == 1)
		{
			$data['title'] = 'Forum de discussion';
			$data['topics'] = $this->forums->getTopics(60,'all');
			$data['current'] = 'forum';
		}
		else if($filtre == 2)
		{
			$data['title'] = 'Mes sujets';
			$data['topics'] = $this->forums->getTopics(60,'my');
			$data['current'] = 'mySubjects';
		}

		
		$data['view'] = 'forum';
		
		$this->load->view('design.php',$data);
	}
	
	function block($id)
	{
		if($this->session->userdata('group') >= 2)
		{		
			$this->load->model('forums');
			
			if($this->forums->isBlocked($id))
			$this->db->set('blocked',0)->where('id',$id)->update('topics');
			else
			$this->db->set('blocked',1)->where('id',$id)->update('topics');
		}
		
		redirect('forum');
	}
	
	function epingle($id)
	{
		if($this->session->userdata('group') >= 2)
		{		
			$this->load->model('forums');
			
			if($this->forums->isEpingled($id))
			$this->db->set('epingled',0)->where('id',$id)->update('topics');
			else
			$this->db->set('epingled',1)->where('id',$id)->update('topics');
		}
		
		redirect('forum');
	}
	
	function currentSondage($id)
	{
		if($this->session->userdata('group') >= 2)
		{		
			$this->db->set('isHome',0)->update('topics');
			$this->db->set('isHome',1)->where('id',$id)->update('topics');
		}
		
		redirect('forum');
	}
	
	function delete($id)
	{
		if($this->session->userdata('group') >= 2)
		{
			$this->db->where('id',$id)->delete('topics');
		}
		
		redirect('forum');
	}
	
	function vote($idSondage, $idResponse,$redirect = 0)
	{
		$this->load->model('forums');
		if(!$this->forums->responseExist($idSondage, $idResponse))
		redirect();
		
		if(!$this->input->cookie('vote_'.$idSondage, TRUE))
		{
			$this->db->set('nbVote','nbVote+1',false)->where('topic',$idSondage)->where('id',$idResponse)->update('sondage');
			$this->input->set_cookie('vote_'.$idSondage, 1, 50000000);
			$this->users->gils(2);
		}
		
		$res = $this->db->select('title')->where('id',$idSondage)->get('topics')->row_array();
		
		if($redirect == 1)
		redirect();
		else
		redirect('forum/'.$idSondage.'/'.url_title($res['title']));
	}
	
	function viewTopic($id)
	{
		$this->load->model('forums');
		
		if(!$this->forums->topicExist($id))
		redirect();
		
		$this->input->set_cookie('topic_'.$id, time(), 50000000);
		
		$data['infos'] = $this->forums->getTopic($id);
		
		if($data['infos']['sondage'] == 1)
		{
			$data['total'] =  0;
			$data['responses'] = $this->forums->getResponses($id);
			foreach($data['responses'] as $rep)
			$data['total'] += $rep['nbVote'];
		}
		
		$data['title'] = $data['infos']['title'];
		
		$data['view'] = 'viewTopic';
		
		$this->load->helper('comments');
		$_POST = array('table' => 'forum','idData' => $id);
		$data['commentaires'] = displayCommentaires();
		$data['topics'] = $this->forums->getLastTopics(9);
		
		$this->load->view('design.php',$data);
	}
	
	function addTopic()
	{
		if($this->input->post('noRobot') == false) exit;
		
		$this->load->model('forums');
		$this->load->model('comments');
		
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('message', 'message', 'required');
		$this->form_validation->set_rules('title', 'titre', 'required');
		
		$formValid = $this->form_validation->run();
		
		$errors = $this->form_validation->error_array();
		
		if($formValid)
		{
			$errors['callback'] = 'reload';
			
			$data['user'] = $this->session->userdata('id');
			$data['title'] = $this->input->post('title');
			$data['lastDate'] = date('Y-m-d H:i:s');
			
			
			$response = $this->input->post('response');
			if($this->input->post('sondage') == 1 
			&& isset($response[0])
			&& isset($response[1]) 
			&& trim($response[0]) != ''
			&& trim($response[1]) != '')
			{
				$data['sondage'] = 1;
			}
			else
			{
				$data['sondage'] = 0;
			}
			
			$id = $this->forums->addTopic($data,$response);
			$this->comments->addComment($id, 'forum', $this->input->post('message'));
			
		}
		
		echo json_encode($errors);
	}
	
}