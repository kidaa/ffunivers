<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index($categorie, $page = 0)
	{
		if($this->session->userdata('group') <= 1) redirect();
		
		$this->load->model( array('comments','shop','forums','events','images') );
		$data['images'] = $this->images->getNotInGallery();
		
		if($categorie == 'content')
		{
			$data['title'] = 'Gérer le contenu';
			$data['type_contents'] = $this->contents->getContentTypes();
			$contents = $this->contents->getMyContents('admin');
			
			$idCreationContents = array(null);
			foreach($contents as $content)
			$idCreationContents[] = $content['idCreation'];
		
			$historyContents = $this->contents->getHistoryByIdCreation($idCreationContents);
			
			foreach($contents as $key => $content)
			{
				$contents[$key]['history'] = array();
				
				foreach($historyContents as $history)
				{
					if($history['idCreation'] == $content['idCreation'])
					$contents[$key]['history'][] = $history;
				}
			}
			
			$data['contents'] = $contents;
		}
		else if($categorie == 'order')
		{
			$data['tree'] = $this->contents->getTreeNav();
			
			$data['title'] = 'Ordonner l\'arborescence';
		}
		else if($categorie == 'images')
		{
			$data['categories'] = $this->contents->getCategories();
			
			$data['title'] = 'Gérer les nouvelles images';
		}
		else if($categorie == 'images2')
		{
			$data['categories'] = $this->contents->getCategories();
			
			$data['images2'] = $this->images->getInGallery($page);
			
			$this->load->library('pagination');

			$config['total_rows'] = $this->images->nbInGallery();
			$config['per_page'] = 30;
			$config['uri_segment'] = $this->uri->total_segments();
			$config['base_url'] = site_url('admin/images2');
			$config['num_links'] = 4;
			$config['first_link'] = 'Début';
			$config['last_link'] = 'Fin';
			$config['next_link'] = 'Suivante »';
			$config['prev_link'] = '« Précédente';
			$config['full_tag_open'] = '<div class="pagination">';
			$config['full_tag_close'] = '</div>';
			$config['cur_tag_open'] = ' <span class="curr">';
			$config['cur_tag_close'] = '</span>';

			$this->pagination->initialize($config);
			
			$data['title'] = 'Gérer les images de la galerie ';
		}

		else if($categorie == 'events')
		{
			$data['events'] = $this->events->getAll();
			
			$data['title'] = 'Gérer les évènements';
		}
		else if($categorie == 'members')
		{
			$data['banned'] = $this->users->getBanned();
			$data['averto'] = $this->users->getAverto();
			
			$data['title'] = 'Gérer les membres';
		}
		
		
		$data['categorie'] = $categorie;
		$data['view'] = 'admin';
		$this->load->view('design.php',$data);
	}
	
	function actionMembers($action, $username)
	{
		if($this->session->userdata('group') <= 1) redirect();
		
		if($action == 'bann')
		{
			$username = $this->input->post('username');
			$this->db->set('group',1)->where('username',$username)->update('users');
		}
		elseif($action == 'debann')
		{
			$this->db->set('group',0)->where('username',$username)->update('users');
		}
		elseif($action == 'averto')
		{
			$username = $this->input->post('username');
			$this->db->set('averto','averto+1',false)->where('username',$username)->update('users');
		}
		
		redirect('admin/members');
	}

}