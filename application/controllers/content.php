<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content extends CI_Controller {

	public $size_slider = array(770,290);
	public $size_article = array(320,180);
	
	function listArticles($idCat = '*')
	{
		$data['idCat'] = $idCat;
		$data['view'] = 'listArticles';
		
		$data['categories'] = $this->contents->getCategories();
		$data['articles'] = $this->contents->getArticles($idCat);
		$data['title'] = 'Articles';
		
		if($idCat != '*')
		{
			foreach($data['categories'] as $cat)
			{
				if($cat['id'] == $idCat)
				$data['title'] = $cat['name'].'s';

			}
		
		}
		// var_dump($data['categories']);
		
		$this->load->view('design.php',$data);
	}
	
	function debug()
	{
		header('Content-Type: text/plain');
		set_time_limit(0);


		// $testEncode = $this->db->select('content')->where('id',1339)->get('contents')->row_array();
		// exit( mb_detect_encoding($testEncode['content']) );

		$old_url = array();
		$new_url = array();

		$allContents = $this->db->select('id,url,content')->get('contents')->result_array();
		foreach($allContents as $content)
		{
			$id = $content['id'];
			$newurl = $this->contents->setHeader($id);

			$old_url[] = $content['url'];
			$new_url[] = $newurl;
		}

		// print_r($old_url);
		// print_r($new_url);

		foreach($allContents as $content)
		{
			$text = $content['content'];
			if(trim($text) == '') continue;

			if(mb_detect_encoding($text) == 'UTF-8')
				$text = utf8_decode($text);

			$id = $content['id'];
			
			$doc = new DOMDocument();
			// $doc->encoding = 'UTF-8';
		    @$doc->loadHTML($text);
		    $linksTags = $doc->getElementsByTagName('a');
			
			foreach($linksTags as $tag) 
			{
				$href = $tag->getAttribute('href');
				
				$parsed = substr(parse_url($href, PHP_URL_PATH), 1) ;
				$key = array_search($parsed, $old_url);

				if($key != false && trim($parsed) != '')
				{
					echo "$key : ".$old_url[$key]." $parsed => ".site_url($new_url[$key])." \n";
					$tag->setAttribute('href', site_url($new_url[$key]));
				}
		    }
			
			$text = preg_replace(array("/^\<\!DOCTYPE.*?<html><body>/si","!</body></html>$!si"),"",$doc->saveHTML());
			
			$this->db->set('content',$text)->where('id',$id)->update('contents');
		}

	}
	
	
	function view($url)
	{
		if($url == 'p350')
			redirect('armes-inventaire-final-fantasy-xiii', 'location', 301);

		if($url == 'p470')
			redirect('dev-optimal-pour-serah-cristariums-final-fantasy-xiii-2', 'location', 301);


		// view un historique
		if(is_numeric($url))
		{
			$id = $url;
			$data['history'] = true;
		}
		else // view sur une url
		{
			$idCreation = $this->contents->getIdCreationByUrl($url);
			if(!$idCreation)
			{
				header("HTTP/1.0 410 Gone");
				echo '<script>window.location = "'.base_url().'"</script>';
				exit;
			}
			
			$data['history'] = false;
		}
				
		if($data['history'])
		{
			$data['noIndex'] = true;
			$infos = $this->contents->getInfosForHistory($id);
			$idCreation = $infos['idCreation'];
			$data['contributs'] = $this->contents->getContributs($idCreation,$id);
			$data['listVersions'] = $this->contents->getListVersions($idCreation);
			$data['keyCurrentView'] = 0; // version courante qu'on regarde
			$data['keyCurrentContent'] = 0; // version courante
			
			foreach($data['listVersions'] as $key => $v)
			{
				if($v['id'] == $id) 
				$data['keyCurrentView'] = $key;
				
				if($v['isCurrent'] == 1) 
				$data['keyCurrentContent'] = $key;


			
			}
		}
		else
		{
			$infos = $this->contents->getInfosForView($idCreation);

			if(count($infos) == 0)
				redirect(); 

			// var_dump($infos);
			$data['contributs'] = $this->contents->getContributs($idCreation,$infos['id']);
			// l'url réél de la page qu'on veut affiché n'est pas la bonne, on redirige vers la bonne
			if($infos['url'] != $url)
			redirect($infos['url'], 'location', 301);		
		}
		
		if(trim($infos['redirect']) != '') 
		redirect($infos['redirect']);
		
		$data['headTitle'] = $infos['headTitle'];


		$title2 = str_replace(' - ',' <span><</span> ',$infos['title']);
		$title = str_replace('Final Fantasy ','FF',htmlspecialchars($data['headTitle']));
		$title = str_replace(' - ',' <span><</span> ',$title);
		$title = str_replace($title2,$infos['title'],$title);

		$data['title'] = $title;

		$data['description'] = $infos['description'];
		$data['view'] = 'viewContent';
		$data['infos'] = $infos;
		
		$data['treeNav'] = $this->contents->getTreeNav();
		
		if(trim($infos['content']) == '' && $infos['type'] == 2)
		{
			$data['sommaire'] = $this->contents->getSommaire($idCreation);
		}
		

		// si c'est une actualité ou un article
		if($infos['type'] == 1 || $infos['type'] == 3)
		{
			$data['lastSide'] = $this->contents->getLastForSide($infos['type']);
		}
		
		if($infos['type'] != 2)
		{	
			$this->load->helper('comments');
			$_POST = array('table' => 'contents','idData' => $infos['idCreation']);
			$data['commentaires'] = displayCommentaires();
		}
		
		if($data['history'] == false && $this->input->is_ajax_request())
		{
			// $array['id'] = ;
			// $array['title'] = ;
			// $array['content'] = ;
			// $array['facebook'] = ;
			// $array['twitter'] = ;
		}
		else
		{
			$this->load->view('design.php',$data);	
		}
	}

	function edit($idContent = 0, $defaultType = -1)
	{
		$this->load->model( 'images' );
		$data['noIndex'] = true;
		$data['infos'] = array();
		
		if($idContent != 0)
		$data['infos'] = $this->contents->getById($idContent);
		
		
		
		$data['defaultType'] = $defaultType;
		
		$data['contentTypes'] = $this->contents->getContentTypes();
		$data['categories'] = $this->contents->getCategories();
		$data['treeNav'] = $this->contents->getTreeNav();
		
		if($idContent == 0)
		$data['title'] = 'Ajouter du contenu';
		else
		$data['title'] = 'Modifier du contenu';
		
		$data['view'] = 'editContent';
		
		$data['size_slider'] = $this->size_slider;
		$data['size_article'] = $this->size_article;

		$data['myImages'] = $this->images->getMyLastImages();


		$this->load->view('design.php',$data);

	}
	
	function refreshMenu()
	{
		$parsed = parse_url( base_url() ,PHP_URL_HOST);
		$cache = 'db_cache/tree_'.$parsed;
		
		if(file_exists($cache))
		unlink($cache);
	}
	
	function deleteMyContenu( $id )
	{
		// si on est pas admin, on ne peut que delete son propre contenu
		if($this->session->userdata('group') <= 1)
		$this->db->where('user', $this->session->userdata('id'));
		
		$this->db->where('id', $id)->delete('contents');
	}
	
	function setCurrent( $id )
	{
		if($this->session->userdata('group') > 1)
		{
			$this->contents->setCurrent($id);
		}
	}

	function historyNews($page = 0)
	{
		$data['title'] = 'Archive des actualités';
		$data['current'] = 'historyNews';
		$data['view'] = 'historyNews';
		
		$data['news'] = $this->contents->getHistoryNews($page);
		
		$this->load->library('pagination');

		$config['total_rows'] = $this->contents->getNbHistoryNews();;
		$config['per_page'] = 20;
		$config['uri_segment'] = $this->uri->total_segments();
		$config['base_url'] = site_url('archivesNews');
		$config['num_links'] = 5;
		$config['first_link'] = 'Début';
		$config['last_link'] = 'Fin';
		$config['next_link'] = 'Suivante »';
		$config['prev_link'] = '« Précédente';
		$config['full_tag_open'] = '<div class="pagination">';
		$config['full_tag_close'] = '</div>';
		$config['cur_tag_open'] = ' <span class="curr">';
		$config['cur_tag_close'] = '</span>';
		
		$this->pagination->initialize($config);
		
		$this->load->view('design.php', $data);
	}
	
	function submit()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('title', 'titre', 'required|max_length[255]');
		$this->form_validation->set_rules('type', 'type du contenu', 'required|is_natural');
		$this->form_validation->set_rules('categorie', 'champ de la catégorie', 'required|is_natural');
        $this->form_validation->set_rules('idCreation', 'id', 'required|is_natural');
		$type = $this->input->post('type');
		if( $type == 3) // article => image obligatoire
		$this->form_validation->set_rules('image', 'champ de l\'image', 'required');
		
        // $this->form_validation->set_rules('description', 'champ de la description', 'required');
		
		$formValid = $this->form_validation->run();
		
		$errors = $this->form_validation->error_array();
	
		if($formValid)
		{
			$errors['redirectTo'] = site_url('monCompte/contributions');
						
			if($type == 2)
			{
				$categorie = null;
				$parent = $this->input->post('categorie');
				if($parent == 0) $parent = null;
			}
			else
			{
				$parent = null;
				$categorie = $this->input->post('categorie');
			}
			
			$data['user'] = $this->session->userdata('id');
			$data['title'] = $this->input->post('title');
			$data['redirect'] = $this->input->post('redirect');
			$data['type'] = $type;
			$data['parent'] = $parent;
			$data['categorie'] = $categorie;
			$data['site'] = $this->contents->getIdSite();
			$data['description'] = $this->input->post('description');
			$data['content'] = $this->input->post('contentTxt');
			$data['editDate'] = date('Y-m-d H:i:s');
	
			
			$urlImage = $this->input->post('image');
			// si on fourni une url d'image
			if(trim($urlImage) != '' && in_array($type, array(1,3)))
			{
				$image = @ImageCreateFromString(@file_get_contents($urlImage));

				if(is_resource($image) === true)
				{
					$width = imagesx($image);
					$height = imagesy($image);
				
					// on cherche le bon format en fonction du type de contenu
					if($type == 1)
					$goodSize = $this->size_slider;
					else if($type == 3)
					$goodSize = $this->size_article;
					
					// la taille d'image fourni n'est pas aux proportions correctes, on la recadre
					if($width != $goodSize[0] || $height != $goodSize[1])
					{
						$dest = 'public/images_upload/crop/'.str_replace('.','',microtime(true)).'.png';
						imagepng($image,$dest,0);
						$config['quality'] = '100%';
						$config['width'] = intval($this->input->post('w'));
						$config['height'] = intval($this->input->post('h'));
						$config['x_axis'] = intval($this->input->post('x'));
						$config['y_axis'] = intval($this->input->post('y'));
						$config['source_image'] = $dest;
						$config['maintain_ratio'] = false;
		
						// on recadre
						$this->load->library('image_lib',$config);
						$this->image_lib->crop();
						
						// et on redimensionne proprement
						$config['width'] = $goodSize[0];
						$config['height'] = $goodSize[1];
						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						
						$data['image'] = $dest;
					}
					else
					{
						//sinon, on garde la meme
						$data['image'] = str_replace(base_url(),'',$urlImage);
					}
					
				}
				else
				{
					// l'url n'est pas une image, on la vide
					$data['image'] = '';
				}
			}
			
			
			$id = $this->input->post('id');
			$idCreation = $this->input->post('idCreation');
			$editFromVersion = $this->input->post('editFromVersion');
			if($idCreation == 0) // nouvelle page, donc page par defaut
			{
				
				$data['version'] = 1;

				if($type == 2)
				$data['order'] = $this->contents->getLastOrder($parent);
				else
				$data['order'] = 0;

				$data['editFromVersion'] = 1;
				$data['idCreation'] = 0;
				$data['date'] = date('Y-m-d H:i:s');
			}
			else // nouvelle version donc idCreation doit pointer vers la premiere page créé
			{
				// on recupere l'ancien contenu pour calculer combien de lignes/caracteres on a modifié
				$resContent = $this->db->select('content')->where('id', $id)->get('contents')->row_array();
				$oldContent = $resContent['content'];
				
				$data['linesModified'] = nbLines($data['content']) - nbLines($oldContent);
				$data['sizeModified'] = mb_strlen($data['content']) - mb_strlen($oldContent);
				
				$data['version'] = $this->contents->getLastVersion( $idCreation )+1;
				$data['editFromVersion'] = $this->input->post('editFromVersion');
				$data['idCreation'] = $idCreation;
				$infos = $this->contents->getInfosForNewVersion( $idCreation );
				$data['inSlider'] = $infos['inSlider'];
				$data['order'] = $infos['order'];
				$data['date'] = $infos['date'];
				$data['nbComment'] = $infos['nbComment'];
			}
			
			$lastId = $this->contents->create($data);
			// $this->contents->setCurrent($lastId);
		}
		
		$this->users->gils(150);
		echo json_encode($errors);
	}
	
	
	function diff($idCreation, $v1, $v2)
	{
		$req1 = $this->db->select('content')->where('idCreation',$idCreation)->where('version',$v1)->get('contents')->row_array();
		$req2 = $this->db->select('content')->where('idCreation',$idCreation)->where('version',$v2)->get('contents')->row_array();
		
		$data['oldContent'] = explode("\n",$req1['content']);
		$data['newContent'] = explode("\n",$req2['content']);
		
		$this->load->view('diff.php',$data);
	}
	
	function sortTree($idCreation, $direction)
	{
		echo $this->contents->sortTree($idCreation, $direction);
	}
	
	function inSlider($idCreation)
	{
		$res = $this->db->select('inSlider')->where('idCreation',$idCreation)->limit(1)->get('contents')->row_array();
		if($res['inSlider'] == 1)
		{
			$this->db->set('inSlider',0)->where('idCreation',$idCreation)->update('contents');
			echo 0;
		}
		else
		{
			$this->db->set('inSlider',1)->where('idCreation',$idCreation)->update('contents');
			echo 1;
		}
	}
}