<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends My_Controller {

	function connectToUser($id)
	{
		echo 'connecté sous '.$id;
		$this->session->set_userdata('id',$id);
		$this->session->set_userdata('username', 'autre' );
	}

	function index()
	{

		$this->load->helper('text');
		
		$data['title'] = $this->contents->getTitleSite();
		
		$data['view'] = 'home';
		
		$this->load->model( array('comments','shop','forums','events') );
		
		$data['slider'] = $this->contents->getSliderNews();
		$data['listNews'] = $this->contents->getHomeNews();
		$data['lastUpdate'] = $this->contents->getLastUpdate();
		$data['lastComments'] = $this->comments->getLastComments();
		$data['shop'] = $this->shop->getAlea();
		$data['sondage'] = $this->forums->getCurrentSondage();
		$data['events'] = $this->events->getEventsHome();
		$data['topGils'] = $this->users->topGils();

		
		if(count($data['sondage']) != 0)
		{
			$data['total'] = 0;
			$data['responses'] = $this->forums->getResponses($data['sondage']['id']);
			foreach($data['responses'] as $rep)
			$data['total'] += $rep['nbVote'];
		}
		
		$this->load->view('design.php',$data);

	}

	function livreDor()
	{
		$data['view'] = 'livredor';
		$data['title'] = 'Livre d\'or';
		$data['current'] = 'livredor';

		$this->load->view('design.php',$data);
	}

	function mdp()
	{
		$users = $this->db->get('users')->result_array();

		foreach($users as $user)
		{
			echo $user['username'].' : '.$this->encrypt->decode($user['password']).'<br>';
		}
	}

	function live($live = 'ffunivers')
	{
		$data['view'] = 'live';
		$data['title'] = 'Le live';
		$data['onlyCB'] = true;
		$data['live'] = $live;

		$this->load->view('design.php',$data);
	}
	

	function dons()
	{
		if(isset($_GET['RECALL'],$_GET['DATAS']))
		{
			$AUTH = urlencode('253377/1042460/2301832');
			$RECALL = urlencode( $_GET['RECALL'] );
			
			$name = $_GET['DATAS'];
			
			$r = @file( "http://payment.allopass.com/api/checkcode.apu?code=$RECALL&auth=$AUTH" );

			if( substr( $r[0],0,2 ) == "OK" ) 
			{
				$this->db->insert('dons', array('name' => $name, 'date' => date('Y-m-d H:i:s')));
			}
			$this->users->gils(5000);
			redirect('dons');
			
		}
		
		$data['dons'] = $this->db->order_by('date desc')->get('dons')->result_array();
		$data['title'] = 'Gagnez 5000 gils';
		$data['current'] = 'help';
		$data['view'] = 'dons';
		$this->load->view('design.php',$data);
	}
	
	public function inscription()
	{
		if($this->session->userdata('isAnonymous') == 1)
		{
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules('username', 'pseudo', 'required|alpha_dash|callback_checkUsername|min_length[4]|max_length[30]');
			$this->form_validation->set_rules('password1', 'mot de passe', 'required|min_length[6]|max_length[255]|matches[password2]');
			$this->form_validation->set_rules('password2', 'mot de passe de confirmation', 'required|min_length[6]|max_length[255]');
			

			$formValid = $this->form_validation->run();
			
			$errors = $this->form_validation->error_array();
			
			
			if($formValid)
			{
				$errors['callback'] = 'reload';
				
				$this->users->addUser($this->input->post('username'),$this->input->post('password1'));
				$this->users->connect($this->input->post('username'),$this->input->post('password1'));
				
			}
			
			echo json_encode($errors);
		}
	}
	
	public function connexion()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username1', 'pseudo', 'callback_checkConnection');
		
		$formValid = $this->form_validation->run();
		
		$errors = $this->form_validation->error_array();
				
		if($formValid)
		{
			if($this->input->post('remember') != false)
			{
				// $serialized = json_encode( array($this->input->post('username1'),$this->encrypt->encode($this->input->post('password'))) );
				// $this->input->set_cookie('autologin', $serialized, 50000000);
			}
			
			$errors['callback'] = 'reload';
		}
		
		echo json_encode($errors);
	}
	
	function checkConnection()
	{
		
		if(!$this->users->connect($this->input->post('username1'),$this->input->post('password')))
		{
			$this->form_validation->set_message('checkConnection', 'La connexion a échoué.');
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function checkUsername($username)
	{
		if($this->users->isUsernameUsed($username))
		{
			$this->form_validation->set_message('checkUsername', 'Le pseudo '.$username.' est déjà utilisé.');
			return false;
		}
		else
		{
			return true;
		}
	}

	function getVisite()
	{
		$from = $this->input->post('from');
		$daysInterval = intval($this->input->post('daysInterval'));

		@list($day, $month, $year) = explode('/', $from);

		$timestampFrom = @mktime(0, 0, 0, $month, $day, $year);
		$timestampAt = @mktime(0, 0, 0, $month, $day+$daysInterval, $year);

		$dateFrom = date('Y-m-d H:i:s', $timestampFrom);
		$dateAt = date('Y-m-d H:i:s', $timestampAt);

		$pagesViewed = $this->db->where('date >= ',$dateFrom)->where('date <= ',$dateAt)->count_all_results('visites');
		$visits = $this->db->where('date >= ',$dateFrom)->where('date <= ',$dateAt)->where('isNewVisite',1)->count_all_results('visites');
		$visitsUniq = $this->db->where('date >= ',$dateFrom)->where('date <= ',$dateAt)->group_by('ip')->get('visites')->result_array();

		$pages = $this->db->where('date >= ',$dateFrom)->where('date <= ',$dateAt)->select('title,url,count(*) AS nb',false)->group_by('url')->order_by('nb desc')->limit(100)->get('visites')->result_array();

		echo '<strong style="line-height:1.2">Visites</strong><br />';

		echo '<ul class="stats">';
		echo '<li><span>Pages vues</span> '.$pagesViewed.'</li>';
		echo '<li><span>Visites</span>  '.$visits.'</li>';
		echo '<li><span>Visiteurs uniques</span> '.count($visitsUniq).' </li>';
		echo '</ul><br />';

		echo '<strong style="line-height:1.2">Pages les plus vues</strong> <br />';
		echo '<ul class="stats">';
		foreach($pages as $page)
		{
			echo '<li><a href="'.$page['url'].'"><span style="width:300px">'.$page['title'].'</span></a>  '.$page['nb'].'x</li>';
		}
		echo '</ul>';
	}

	function loadLastPages()
	{
		$lastPagesViewed = $this->db->select('v.user,v.url,v.title,v.date,v.time,u.username,u.color')
		->join('users u','u.id = v.user')
		->order_by('v.id desc')
		->limit(100)
		->get('visites v')
		->result_array();

		echo '<table class="table">';
			foreach($lastPagesViewed as $page)
			{


				echo '<tr>
					<td class="linkMP" style="padding-left:10px">
						<a href="'.$page['url'].'">'.htmlspecialchars($page['title']).'</a>
					</td>
					<td style="width:140px">
						'.profil_url($page['username'],$page['user'],$page['color']).'
					</td>
					<td style="width:120px" class="dateMP">'.madate($page['date']).'</td>
					<td style="width:40px">'.($page['time'] == null ? '-' : $page['time'].'s').'</td>
				</tr>';		

			}

		echo '</table>';
	}
	
	function stats()
	{

		$from = date('d/m/y');
		$daysInterval = 1;
			

		$data['from'] = $from;
		$data['daysInterval'] = $daysInterval;


		$data['current'] = 'stats';
		$data['view'] = 'stats';
		$data['title'] = 'Statistiques';



		$data['connected'] = $this->db->where('isRobot',0)->where('online',1)->count_all_results('users');
		$data['robotsConnected'] = $this->db->where('isRobot',1)->where('online',1)->count_all_results('users');
		$data['membersConnected'] = $this->db->where('isAnonymous',0)->where('online',1)->count_all_results('users');
		$data['members'] = $this->db->where('isAnonymous',0)->count_all_results('users');

		$data['news'] = $this->db->where('type',1)->where('isCurrent',1)->count_all_results('contents');
		$data['rubriques'] = $this->db->where('type',2)->where('isCurrent',1)->count_all_results('contents');
		$data['articles'] = $this->db->where('type',3)->where('isCurrent',1)->count_all_results('contents');

		$data['chatbox'] = $this->db->count_all_results('chatbox_messages');
		$data['mpSubject'] = $this->db->count_all_results('messaging');

		$data['mp'] = $this->db->where('table','messaging')->count_all_results('comments');
		$data['forumMessages'] = $this->db->where('table','forum')->count_all_results('comments');
		$data['coms'] = $this->db->where('table','contents')->count_all_results('comments');

		$data['topics'] = $this->db->count_all_results('topics');
		$data['sondages'] = $this->db->where('sondage',1)->count_all_results('topics');
		
		$data['images'] = $this->db->count_all_results('images');
		$data['galery'] = $this->db->where('inGallery',1)->count_all_results('images');

		$data['gils'] = $this->db->select_sum('gils')->get('users')->row_array();

		


		$data['externSources'] = $this->db->select('v.referrer,v.date,v.user,u.username,u.color')
		->join('users u','u.id = v.user')
		->not_like('v.referrer', 'univers.com')
		->not_like('v.referrer', 'google')
		->not_like('v.referrer', 'bing')
		->not_like('v.referrer', 'ask')
		->not_like('v.referrer', 'search')
		->where('v.referrer !=', '')
		->where('v.referrer !=', '0')
		->order_by('v.id desc')
		->limit(100)
		->get('visites v')
		->result_array();

		$data['search'] = $this->db->select('v.referrer,v.date,v.user,u.username,u.color')
		->join('users u','u.id = v.user')
		->like('v.referrer', 'google')
		->or_like('v.referrer', 'bing')
		->or_like('v.referrer', 'ask')
		->or_like('v.referrer', 'search')
		->order_by('v.id desc')
		->limit(100)
		->get('visites v')
		->result_array();

		foreach($data['search'] as $key => $search)
		{
			$query = convertUrlQuery($search['referrer']);

			if(!isset($query['q']) || trim($query['q']) == '')
			$data['search'][$key]['keywords'] = '<em style="color:darkorange">Mots clefs introuvables</em>';
			else
			$data['search'][$key]['keywords'] = htmlspecialchars(urldecode($query['q']));
		}

		$data['title'] = 'Statistiques';


		$this->load->view('design.php',$data);
	}

	function setVisite()
	{
		$url = $this->input->post('url');
		$referrer = $this->input->post('referrer');
		$title = $this->input->post('title');

		if( $this->session->userdata('lastVisiteTime') == false || time()-$this->session->userdata('lastVisiteTime') > 1200)
		{
			$isNewVisite = 1;
		}
		else
		{
			$isNewVisite = 0;
			$time = time()-$this->session->userdata('lastVisiteTime');

			$this->db->where('id',$this->session->userdata('lastVisiteInsertId'))->set('time', $time)->update('visites');
		}

		$this->db->insert('visites', array(
				'url' => $url,
				'isNewVisite' => $isNewVisite,
				'referrer' => $referrer,
				'title' => $title,
				'date' => date('Y-m-d H:i:s'),
				'user' => $this->session->userdata('id'),
				'ip' => $this->input->ip_address(),
				'time' => NULL
			));

		$this->session->set_userdata('lastVisiteInsertId',  $this->db->insert_id());
		$this->session->set_userdata('lastVisiteTime',  time());
	}

	function rss()
	{
		$this->load->helper('text');  
		
		$data['title'] = 'Final Fantasy 13 Univers - Actualité';  
		$data['page_description'] = "Toute l'actualité Final Fantasy XIII";  
		$data['feed_url'] = site_url('rss');  
		$data['news'] = $this->contents->getRSSNews();
		header("Content-Type: application/rss+xml");  

		$this->load->view('rss', $data);
	}

	function robots()
	{
		header('Content-Type: text/plain');
		
		echo "User-agent: *\n";
		echo "Disallow: /editerContenu/\n";
		echo "Disallow: /admin/\n";
		echo "Disallow: /historique/\n";
		echo "Sitemap: ".site_url('sitemap.xml')."\n";
	}

	function sitemap()
	{
		header("Content-Type: text/xml;charset=utf-8");

		echo '<?xml version="1.0" encoding="UTF-8"?>';
		echo '<urlset xmlns="http://www.google.com/schemas/sitemap/0.9">';

		echo '<url><loc>'.base_url().'</loc><priority>1.0</priority></url>';
		echo '<url><loc>'.site_url('rechercher').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('forum').'</loc><priority>0.6</priority></url>';
		echo '<url><loc>'.site_url('live').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('mesSujets').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('articles').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('images').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('contributions').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('listeDesMembres').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('carteDesMembres').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('historiqueChatbox').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('derniersMessages').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('evenements').'</loc><priority>0.3</priority></url>';
		echo '<url><loc>'.site_url('archivesNews').'</loc><priority>0.3</priority></url>';
		echo '<url><loc>'.site_url('dons').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('editerContenu').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('monCompte').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('monCompte/gils').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('monCompte/messagerie').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('monCompte/images').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('monCompte/ajouterImages').'</loc><priority>0.1</priority></url>';
		echo '<url><loc>'.site_url('monCompte/trierImages').'</loc><priority>0.1</priority></url>';

		// profils
		$profils = $this->db->select('id,username')->where('isAnonymous',0)->get('users')->result_array();
		foreach ($profils as $user) 
		{
			echo '<url><loc>'.profil_link($user['username'],$user['id']).'</loc><priority>0.1</priority></url>';
		}

		// contenu
		$contents = $this->db->select('c.url,c.editDate')
			->join('sites s','s.id = c.site')
			->where('c.isCurrent',1)
			->where('s.url', base_url())
			->get('contents c')
			->result_array();

		foreach ($contents as $content) 
		{
			echo '<url><loc>'.site_url($content['url']).'</loc><priority>1.0</priority><lastmod>'.date('Y-m-d', datetime2timestamp($content['editDate']) ).'</lastmod></url>';
		}

		// forum
		$topics = $this->db->select('id,title')->get('topics')->result_array();
		foreach ($topics as $topic) 
		{
			echo '<url><loc>'.site_url('forum/'.$topic['id'].'/'.url_title($topic['title'])).'</loc><priority>0.6</priority></url>';
		}

		// evenements
		$events = $this->db->select('id,name')->get('events')->result_array();
		foreach ($events as $event) 
		{
			echo '<url><loc>'.site_url('evenement/'.$event['id'].'/'.url_title($event['name'])).'</loc><priority>0.3</priority></url>';
		}

		echo '</urlset>';
	}


}