<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comment extends CI_Controller {

	
	function lastMsg()
	{
		$this->load->model('comments');
		
		$data['comments'] = $this->comments->getLastComments(30);
		
		$data['title'] = 'Derniers messages';
		$data['current'] = 'lastMsg';
		$data['view'] = 'lastMsg';
		$this->load->view('design.php',$data);
	}
	
	
	public function addComment()
	{
		if($this->input->post('noRobot') == false) exit;
		
		$this->load->model('comments');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('message', 'message', 'required');
		$idData = intval($this->input->post('idData'));
		
		$formValid = $this->form_validation->run();
		
		$errors = $this->form_validation->error_array();
		
		if($formValid)
		{
			$errors['callback'] = 'refreshComments';
			$errors['success'] = '';
			
			$this->comments->addComment($idData,$this->input->post('table'),$this->input->post('message'));
			
		}
		
		echo json_encode($errors);
	}
	
	function displayComments($page)
	{
		$this->load->helper('comments');
		displayCommentaires();
	}

	function editComm($id)
	{
		$this->load->model('comments');
		
		if($this->input->post('text') !== false)
		{
			$text = $this->input->post('text');
			$this->comments->edit($text, $id);
			echo montexte($text);
		}
		else
		{
			echo $this->comments->getSource($id);
		}
	}

	function deleteComm($id)
	{
		if($this->session->userdata('group') >= 2)
		{
			$res = $this->db->select('idData,table')->where('id', $id)->get('comments')->row_array();
			
			if($res['table'] == 'forum')
			$this->db->set('nbPost','nbPost-1',false)->where('id', $res['idData'])->update('topics');
			elseif($res['table'] == 'contents')
			$this->db->set('nbComment','nbComment-1',false)->where('idCreation', $res['idData'])->update('contents');
			
			$this->db->where('id', $id)->delete('comments');
		
		
		}
	}

}