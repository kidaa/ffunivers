<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Concours extends CI_Controller 
{
    function __construct()
    {
        parent::__construct();
    }
	
	function facts()
	{
		if(!is_file('facts.txt'))
		touch('facts.txt');
		
		$facts = unserialize(file_get_contents('facts.txt'));
		if($facts == false) $facts = array();
		echo '<meta charset="utf-8" />';
		
		
		foreach($facts as $key => $fact_info)
		{
			$infos = list($ip,$mail,$auteur,$fact) = explode('+-+',$fact_info);
			
			if(trim($fact) == '')
			{
				unset($facts[$key]);
				continue;
			}
			
			
			echo '<span style="font-size:16px;font-family:Arial;"><b>'.$auteur.' ('.$ip.' / '.$mail.') :</b> <br /> '.$fact.'</span> <br /> <br />';
		}
		
		file_put_contents('facts.txt',serialize($facts));
		
	}
    
	function submit()
	{
		if(!is_file('facts.txt'))
		touch('facts.txt');
		
		$facts = unserialize(file_get_contents('facts.txt'));
		if($facts == false) $facts = array();
		
		$facts[] = $this->input->ip_address().'+-+'.$_POST['mail'].'+-+'.$_POST['auteur'].'+-+'.$_POST['fact'];
		
		file_put_contents('facts.txt',serialize($facts));
		
		$errors['success'] = 'Votre participation à notre concours a été effectuée correctement.';
		
		echo json_encode($errors);
	}
}