<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chatbox extends CI_Controller 
{

	public function historiqueChatbox()
	{
		$data['title'] = 'Historique de la chatbox';
		$data['view'] = 'historyCB';
		$data['current'] = 'historyCB';
		
		$this->load->view('design.php',$data);
	}
	
	public function addRoom()
	{
		$this->load->model('chatboxs');
		if($this->chatboxs->roomExist($this->input->post('name')))
		echo 'Ce salon existe déjà';
		else
		{
			if(trim($this->input->post('password')) == '')
			$password = null;
			else
			$password = $this->input->post('password');
			
			if(trim($this->input->post('name')) != '')
			$this->chatboxs->createRoom($this->input->post('name'), $password );
				
		}
	}
	
	public function addMessage()
	{
		if(in_array($this->input->ip_address(), array('87.231.21.202'))) exit;

		$this->load->model('chatboxs');
		
		$msg = $this->input->post('message');
		if(trim($msg) == '') return;
		$idUser = null;
		
		if($msg[0] == '/')
		{
			$explode = explode(' ',$msg);
			$username = str_replace('/','',$explode[0]);
			
			$idUser = $this->users->getIdByName($username);
			$msg = str_replace('/'.$username.' ','',$msg);
			
			if($idUser == $this->session->userdata('id'))
			{
				echo json_encode( array('success' => 'Un message à vous même ? Trouvez-vous des amis !') );
				return;
			}
			
			if(!$idUser)
			{
				echo json_encode( array('success' => $username.' n\'est pas un membre.') );
				return;
			}
		}
		
		$this->chatboxs->add($msg, $idUser);
		$this->users->gils(0.1);
		echo json_encode( array('callback' => 'messageSended') );
	}
	
	public function loadRoom($id)
	{
		$this->load->model('chatboxs');
		
		$room = $this->chatboxs->getRoom($id);
		
		if(!is_array($this->session->userdata('listRooms')))
		$this->session->set_userdata('listRooms', array());
		
		// on est deja venu, on peut rentré
		if(in_array($id, $this->session->userdata('listRooms')))
		{
			$infos['response'] = 1;
			$infos['roomName'] = $room['name'];
			
			$this->session->set_userdata('currentRoom',$id);
			
			echo json_encode($infos);
		}
		else if($room['password'] != null) // password requis
		{
			if($this->input->post('password') == $room['password']) //bon password
			{
				$this->session->set_userdata('currentRoom',$id);
				
				// on indique qu'on est deja venu
				$rooms = $this->session->userdata('listRooms');
				$rooms[] = $id;
				$this->session->set_userdata('listRooms',$rooms);
				
				
				$infos['response'] = 1;
				$infos['roomName'] = $room['name'];
				echo json_encode($infos);
			}
			else // mauvais password
			{
				$infos['response'] = 0;
				echo json_encode($infos);
			}
		}
		else // aucun password requis, on vient direct
		{
				
			$this->session->set_userdata('currentRoom',$id);
			
			// on indique qu'on est deja venu
			$rooms = $this->session->userdata('listRooms');
			$rooms[] = $id;
			$this->session->set_userdata('listRooms',$rooms);
			
			$infos['response'] = 1;
			$infos['roomName'] = $room['name'];
			echo json_encode($infos);
		}
		
	}
	
	public function deleteMsg($id)
	{
		if($this->session->userdata('group') >= 2)
		{
			$this->db->where('id' , $id)->set('isDeleted',1)->update('chatbox_messages');
		}
	}
	
	public function deleteRoom($id)
	{
		$this->load->model('chatboxs');
		$room = $this->chatboxs->getRoom($id);
		
		if($this->session->userdata('group') >= 2 || $this->session->userdata('id') == $room['user'])
		{
			$this->db->delete('chatbox_rooms', array('id' => $id));
		}
	}
	
	public function listMsg($page = null)
	{
		$this->load->model('chatboxs');
		
		if($page == null)
		{
			$mgs = $this->chatboxs->getMsg();
			$mgs = array_reverse($mgs,true);
		}
		else
		{
			$mgs = $this->chatboxs->getHistory($page);
		}
		
		
		$json = array();
		$lastUser = false;
		foreach($mgs as $msg)
		{
			//si c'est un message privé
			//qu'on la pas envoyé
			//et qu'on n'est pas le destinataire => continue
			// if($msg['toUser'] != null &&  $msg['user'] != $this->session->userdata('id') && $msg['toUser'] != $this->session->userdata('id'))
			// continue;
			
			$json[$msg['id']] = '<div style="display:none" class="message" id="msgTchatbox'.$msg['id'].'">';
			
			if($this->session->userdata('group') >= 2 && $page == null)
			{
				$json[$msg['id']] .= '<a class="deleteMsg" href="'.site_url('deleteMsg/'.$msg['id']).'">'.img('mini_delete.png').'</a>';
			}
			
			if($msg['isDeleted'] == 1 && $page == null)
			{
				$json[$msg['id']] .= 'removedByAdmin';
			}
			else
			{
				$isMp = false;
				
				// pseudo du click message privée
				if($msg['toUser'] == $this->session->userdata('id'))
				{
					$mpUsername = $msg['username'];
					$isMp = true;
				}
				elseif($msg['toUser'] != null && $msg['user'] == $this->session->userdata('id'))
				{
					$mpUsername = $msg['username2'];
					$isMp = true;
				}
				else
				{
					$mpUsername = $msg['username'];
				}
				
				if($isMp)
				$testWith =  $msg['username'].'@'. $msg['username2'];
				else
				$testWith = $msg['username'];
				
				
				if($lastUser != $testWith)
				{
					$lastUser = $testWith;
					
					$json[$msg['id']] .= '<div class="author">';
			
					// bouton MP
					if($page == null)
					$json[$msg['id']] .= '<a class="mpMsg" username="'.$mpUsername.'" nohref>'.img('mp.png').'</a>';
					// personne qui a envoyé le message
					$json[$msg['id']] .= profil_url($msg['username'],$msg['user'],$msg['color']);
					// si c'est un MP, on affiche a qui
					if($isMp)
					$json[$msg['id']] .= '@'.profil_url($msg['username2'],$msg['toUser'],$msg['color2']);
					
					//date
					if($page == null)
					$json[$msg['id']] .= ' <span class="time">'.date('G\hi',datetime2timestamp($msg['date'])).'</span></div>';
					else
					$json[$msg['id']] .= ' <span class="time">'.madate($msg['date']).'</span></div>';
				}
				
				// on affiche le message
				if($isMp)
				$style = 'color:#006600';
				else
				$style = '';

				$json[$msg['id']] .= '<span style="'.$style.'"">'.montexte($msg['message'],true,false).'</span>';
				
			}
	
			
			$json[$msg['id']] .= '</div>';
		}
		
		echo json_encode($json);
	}
	
	public function listRooms()
	{
		$this->load->model('chatboxs');
		
		$rooms = $this->chatboxs->getRooms();
		
		echo '<a id="addRoom" nohref>Créer un nouveau salon</a>';
		echo '<div style="height:1px;border-bottom:1px dotted #999;margin-top:2px;margin-bottom:2px;"></div>';
		echo '<a href="'.site_url('historiqueChatbox').'">Historique de la chatbox</a>';
		echo '<div style="height:1px;border-bottom:1px dotted #999;margin-top:2px;margin-bottom:2px;"></div>';
		foreach($rooms as $room)
		{
			echo '<div class="goToRoom" idRoom="'.$room['id'].'">';
			
			if($this->session->userdata('group') >= 2 || $this->session->userdata('id') == $room['user'])
			{
				echo '<a class="deleteRoom" href="'.site_url('deleteRoom/'.$room['id']).'">'.img('mini_delete.png').'</a>';
			}
			
			echo $room['name'];
			
			if($room['password'] != null) echo img('admin.png');
			echo '</div>';
		}
	}
	
		
	public function getConnected()
	{
		$connected = $this->users->getConnected();
		
		$nbConnected = count($connected);
		
		echo '<a id="linkMembersH2" class="more" href="'.site_url('listeDesMembres/1/0').'" title="Voir la liste des membres">'.$nbConnected.' connecté'.($nbConnected > 1 ? 's' : '').'</a>';
		
		echo '<div class="listConnected">';
		foreach($connected as $user)
		{
			if($user['isAnonymous'] == 0)
			{
				echo profil_url( $user['username'] ,$user['id'],$user['color'], avatar_url($user['id'],true));
			}
		}
		echo '</div>';
		
		echo '<script>';
		echo '$(\'.listConnected a[title], #linkMembersH2\').tipTip({maxWidth: "300px", edgeOffset: 2, delay: 50});';
		echo '</script>';
	}
}