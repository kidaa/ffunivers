<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Image extends CI_Controller {

	function view()
	{
		$data['title'] = 'Galerie d\'images';
		$data['view'] = 'listImages';
		
		$this->load->model('images');
		
		$data['categories'] = $this->contents->getCategories();
		
		$this->load->view('design.php',$data);
	}
	
	function getImagesFromAjax($page)
	{
		$this->load->model('images');
		
		$images = $this->images->loadAjaxImages($page,$this->input->post('categories'),$this->input->post('categorieTypes'));
		
		if(count($images) == 0)
		echo 0;
		else
		echo json_encode($images);
	}
	
	function doIndex()
	{
		if($this->input->post('categorie') == 0)
		$categorie = null;
		else
		$categorie = intval($this->input->post('categorie'));
		
		if($this->input->post('categorieType') == 0)
		$categorieType = null;
		else
		$categorieType = intval($this->input->post('categorieType'));
		
		$id = intval($this->input->post('id'));
		$res = $this->db->select('indexed')->where('id', $id)->where('user', $this->session->userdata('id'))->get('images')->row_array();
		
		if($res['indexed'] == 0)
		$this->users->gils(10);
		
		
		$this->db->set('categorie', $categorie)
		->set('categorieType', $categorieType)
		->set('indexed', 1);
		
		if($this->session->userdata('group') < 2)
		$this->db->where('user', $this->session->userdata('id'));
		
		$this->db->where('id', $id)->update('images');
		
	}
	
	function addToGallery()
	{
		if($this->session->userdata('group') > 1)
		{
			if($this->input->post('categorie') == 0)
			$categorie = null;
			else
			$categorie = intval($this->input->post('categorie'));
			
			if($this->input->post('categorieType') == 0)
			$categorieType = null;
			else
			$categorieType = intval($this->input->post('categorieType'));
			
			$id = intval($this->input->post('id'));
			
			$res = $this->db->select('user')->where('id', $id)->get('images')->row_array();
		
			$this->users->gils(80,$res['user']);
			
			$this->db->set('categorie', $categorie)
			->set('categorieType', $categorieType)
			->set('inGallery', 1)
			->set('viewed', 1)
			->where('id', $id)
			->update('images');
		}
	}
	
	function refuseGallery($id)
	{
		if($this->session->userdata('group') > 1)
		{
			$this->db->set('viewed', 1)
			->where('id', $id)
			->update('images');
		}
	}
	
	function removeFromGallery($id)
	{
		if($this->session->userdata('group') > 1)
		{
			$this->db->set('inGallery', 0)
			->where('id', $id)
			->update('images');
		}
	}
	
	function deleteMyImage($id)
	{
		$img = $this->db->select('name')->where('id', $id)->get('images')->row_array();
		
		$nb1 = $this->db->like('content', 'public/images_upload/big/'.$img['name'])->count_all_results('contents');
		$nb2 = $this->db->like('content', 'public/images_upload/mini/'.$img['name'])->count_all_results('contents');
		
		if($nb1 >= 1 || $nb2 >= 1)
		echo 'Impossible de supprimer cette image, elle semble être utilisée dans une des pages du site.';
		else
		{
			$this->db->where('id',$id)->where('user', $this->session->userdata('id'))->delete('images');
			@unlink('public/images_upload/big/'.$img['name']);
			@unlink('public/images_upload/mini/'.$img['name']);
		}
	}
	
	function upload()
	{
		set_time_limit(2*60);
		$this->load->model('images');
		$this->load->library('image_lib');
		
		// var_dump($_FILES['files']);
		
		// les images a partir d'url
		foreach($_POST['urlImg'] as $url)
		{
			$image = @ImageCreateFromString(@file_get_contents($url));
			
			if(is_resource($image) === true)
			{
				$name = correctNameUpload(basename($url) ,'public/images_upload/big/');
				imagepng($image,'public/images_upload/big/'.$name);
				
				// redimension miniature
				$config['quality'] = '100%';
				$config['source_image'] = 'public/images_upload/big/'.$name; 
				$config['width'] = 180;
				$config['height'] = 180;
				$config['master_dim'] = 'height';
				$config['new_image'] = 'public/images_upload/mini/'.$name;
				$this->image_lib->initialize($config);
				$this->image_lib->resize();

				$add['name'] = $name;
				$add['date'] = date('Y-m-d H:i:s');
				$add['user'] = $this->session->userdata('id');
				$add['categorie'] = null; 
				$add['categorieType'] = null; 

				$this->images->add($add);
			}
		}
		
		// les images à partir du PC
		$file_ary = reArrayFiles($_FILES['files']);
		foreach($file_ary as $file)
		{
			if(in_array($file['type'], array('image/jpeg','image/png','image/gif')) && $file['size'] < 5000000)
			{
				$name = correctNameUpload($file['name'] ,'public/images_upload/big/');
				if(move_uploaded_file($file['tmp_name'], 'public/images_upload/big/'.$name))
				{
					// redimension miniature
					$config['quality'] = '100%';
					$config['source_image'] = 'public/images_upload/big/'.$name; 
					$config['width'] = 180;
					$config['height'] = 180;
					$config['master_dim'] = 'height';
					$config['new_image'] = 'public/images_upload/mini/'.$name;
					$this->image_lib->initialize($config);
					$this->image_lib->resize();

					$add['name'] = $name;
					$add['date'] = date('Y-m-d H:i:s');
					$add['user'] = $this->session->userdata('id');
					$add['categorie'] = null; 
					$add['categorieType'] = null; 

					$this->images->add($add);
				}
				
			}
		}
		
		redirect('monCompte/images');
	}
}