<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function css_url($nom)
{
	return base_url() . 'public/css/' . $nom . '.css';
}


function js_url($nom)
{
	return base_url() . 'public/javascript/' . $nom . '.js';
}


function img_url($nom)
{
	return base_url() . 'public/images/' . $nom;
}


function plugin_url($nom)
{
	return base_url() . 'public/plugins/' . $nom;
}


function img($nom, $alt = '', $style = '')
{
	return '<img src="' . img_url($nom) . '" alt="' . $alt . '" style="'.$style.'" '.($alt != '' ? 'title="'.$alt.'"' : '').' />';
}

function abs_img($url, $alt = '', $style = '')
{
	return '<img src="' .$url . '" alt="' . $alt . '" style="'.$style.'" />';
}

function profil_url($username, $id, $color, $img = false)
{
	$margin = '';

	if($img !== false && $id == 1) $margin = 'margin:1px 0 1px 2px';
	elseif($img !== false && $id == 2) $margin = 'margin:1px 2px 1px 0';
	elseif($img !== false) $margin = 'margin:1px';

	return '<a '.($img != false ? 'title="'.$username.'"' : '' ).' style="color:'.$color.';'.$margin.'" href="'.site_url('profil-'.$id.'/'.$username).'">'.($img == false ? $username : $img).'</a>';
}

function profil_link($username, $id)
{
	return site_url('profil-'.$id.'/'.$username);
}

function avatar_url($id, $mini = false , $style = '')
{
	if(!$mini )
	$path = 'public/images_upload/avatars/';
	else
	$path = 'public/images_upload/mini_avatars/';
	
	if(file_exists($path.$id.'.png')) $name = $id.'.png';
	else if(file_exists($path.$id.'.jpg')) $name = $id.'.jpg';
	else $name = '0.png';
	
	return abs_img(base_url().$path.$name,'',$style);
}

function avatar_exist($id)
{
	$path = 'public/images_upload/mini_avatars/';
	
	if(file_exists($path.$id.'.png')) return true;
	else if(file_exists($path.$id.'.jpg')) return true;
	else return false;
}

function boutique_url($url)
{
	return $url.'?utm_source=site_ff13univers&utm_medium=lien_generique&utm_campaign=communaute_ff';
}











function montextepreview($texte)
{
	$texte = htmlspecialchars($texte);
	
	$texte = preg_replace('!\[spoil\](.+)\[/spoil\]!isU', '<span onclick="showSpoil(this)" class="spoiler">Spoiler<span>$1</span></span>',$texte);
	// $texte = preg_replace('!\[img\](.+)\[/img\]!isU', '<img src="$1" alt="" />',$texte);
	$texte = preg_replace('!\[s\](.+)\[/s\]!isU', '$1',$texte);
	$texte = preg_replace('!\[g\](.+)\[/g\]!isU', '$1',$texte);
	$texte = preg_replace('!\[i\](.+)\[/i\]!isU', '$1',$texte);
	
	return $texte;
}

function montexte($texte,$bn=true,$bbcode=true)
{

	$texte = htmlspecialchars($texte);
	
	if($bbcode)
	{


		$texte = preg_replace('!\[img\](.+)\[/img\]!isU', '<a onclick="return hs.expand(this)" href="$1"><img src="$1" alt="" /></a>',$texte);
		$texte = preg_replace('!\[spoil\](.+)\[/spoil\]!isU', '<span onclick="showSpoil(this)" class="spoiler">Cliquez pour afficher le spoiler<span>$1</span></span>',$texte);
		// $texte = preg_replace('!\[img\](.+)\[/img\]!isU', '<img src="$1" alt="" />',$texte);
		$texte = preg_replace('!\[s\](.+)\[/s\]!isU', '<span style="text-decoration:underline">$1</span>',$texte);
		$texte = preg_replace('!\[g\](.+)\[/g\]!isU', '<strong>$1</strong>',$texte);
		$texte = preg_replace('!\[i\](.+)\[/i\]!isU', '<em>$1</em>',$texte);
	}

	$texte = preg_replace('`(?<!src=")(?<!href=")((?:https?|ftp)://\S+)(\s|\z)`', '<a target="_blank" href="$1">$1</a>$2', $texte);

	$texte=str_replace(':-)))','<img src="http://www.jeuxvideo.com/smileys/23.gif">',$texte);
	$texte=str_replace(':-)','<img src="http://www.jeuxvideo.com/smileys/46.gif">',$texte);
	$texte=str_replace(':-((','<img src="http://www.jeuxvideo.com/smileys/15.gif">',$texte);
	$texte=str_replace(':-(','<img src="http://www.jeuxvideo.com/smileys/14.gif">',$texte);
	$texte=str_replace(':o))','<img src="http://www.jeuxvideo.com/smileys/12.gif">',$texte);
	$texte=str_replace(':-D','<img src="http://www.jeuxvideo.com/smileys/40.gif">',$texte);
	$texte=str_replace(':-p','<img src="http://www.jeuxvideo.com/smileys/31.gif">',$texte);
	$texte=str_replace(':d)','<img src="http://www.jeuxvideo.com/smileys/4.gif">',$texte);
	$texte=str_replace(':g)','<img src="http://www.jeuxvideo.com/smileys/3.gif">',$texte);
	$texte=str_replace(':p)','<img src="http://www.jeuxvideo.com/smileys/7.gif">',$texte);
	$texte=str_replace(':ange:','<img src="http://www.jeuxvideo.com/smileys/60.gif">',$texte);
	$texte=str_replace(':banzai:','<img src="http://www.jeuxvideo.com/smileys/70.gif">',$texte);
	$texte=str_replace(':bave:','<img src="http://www.jeuxvideo.com/smileys/71.gif">',$texte);
	$texte=str_replace(':bravo:','<img src="http://www.jeuxvideo.com/smileys/69.gif">',$texte);
	$texte=str_replace(':bye:','<img src="http://www.jeuxvideo.com/smileys/48.gif">',$texte);
	$texte=str_replace(':cd:','<img src="http://www.jeuxvideo.com/smileys/5.gif">',$texte);
	$texte=str_replace(':coeur:','<img src="http://www.jeuxvideo.com/smileys/54.gif">',$texte);
	$texte=str_replace(':content:','<img src="http://www.jeuxvideo.com/smileys/24.gif">',$texte);
	$texte=str_replace(':cool:','<img src="http://www.jeuxvideo.com/smileys/26.gif">',$texte);
	$texte=str_replace(':diable:','<img src="http://www.jeuxvideo.com/smileys/61.gif">',$texte);
	$texte=str_replace(':dehors:','<img src="http://www.jeuxvideo.com/smileys/52.gif">',$texte);
	$texte=str_replace(':desole:','<img src="http://www.jeuxvideo.com/smileys/65.gif">',$texte);
	$texte=str_replace(':doute:','<img src="http://www.jeuxvideo.com/smileys/28.gif">',$texte);
	$texte=str_replace(':dpdr:','<img src="http://www.jeuxvideo.com/smileys/49.gif">',$texte);
	$texte=str_replace(':fete:','<img src="http://www.jeuxvideo.com/smileys/66.gif">',$texte);
	$texte=str_replace(':fier:','<img src="http://www.jeuxvideo.com/smileys/53.gif">',$texte);
	$texte=str_replace(':fou:','<img src="http://www.jeuxvideo.com/smileys/50.gif">',$texte);
	$texte=str_replace(':gba:','<img src="http://www.jeuxvideo.com/smileys/17.gif">',$texte);
	$texte=str_replace(':globe:','<img src="http://www.jeuxvideo.com/smileys/6.gif">',$texte);
	$texte=str_replace(':gne:','<img src="http://www.jeuxvideo.com/smileys/51.gif">',$texte);
	$texte=str_replace(':gni:','<img src="http://www.jeuxvideo.com/smileys/62.gif">',$texte);
	$texte=str_replace(':hap:','<img src="http://www.jeuxvideo.com/smileys/18.gif">',$texte);
	$texte=str_replace(':hello:','<img src="http://www.jeuxvideo.com/smileys/29.gif">',$texte);
	$texte=str_replace(':honte:','<img src="http://www.jeuxvideo.com/smileys/30.gif">',$texte);
	$texte=str_replace(':hum:','<img src="http://www.jeuxvideo.com/smileys/68.gif">',$texte);
	$texte=str_replace(':hs:','<img src="http://www.jeuxvideo.com/smileys/64.gif">',$texte);
	$texte=str_replace(':lol:','<img src="http://www.jeuxvideo.com/smileys/32.gif">',$texte);
	$texte=str_replace(':mac:','<img src="http://www.jeuxvideo.com/smileys/16.gif">',$texte);
	$texte=str_replace(':malade:','<img src="http://www.jeuxvideo.com/smileys/8.gif">',$texte);
	$texte=str_replace(':merci:','<img src="http://www.jeuxvideo.com/smileys/58.gif">',$texte);
	$texte=str_replace(':monoeil:','<img src="http://www.jeuxvideo.com/smileys/34.gif">',$texte);
	$texte=str_replace(':mort:','<img src="http://www.jeuxvideo.com/smileys/21.gif">',$texte);
	$texte=str_replace(':nah:','<img src="http://www.jeuxvideo.com/smileys/19.gif">',$texte);
	$texte=str_replace(':noel:','<img src="http://www.jeuxvideo.com/smileys/11.gif">',$texte);
	$texte=str_replace(':non:','<img src="http://www.jeuxvideo.com/smileys/35.gif">',$texte);
	$texte=str_replace(':nonnon:','<img src="http://www.jeuxvideo.com/smileys/25.gif">',$texte);
	$texte=str_replace(':non2:','<img src="http://www.jeuxvideo.com/smileys/33.gif">',$texte);
	$texte=str_replace(':ok:','<img src="http://www.jeuxvideo.com/smileys/36.gif">',$texte);
	$texte=str_replace(':ouch:','<img src="http://www.jeuxvideo.com/smileys/22.gif">',$texte);
	$texte=str_replace(':ouch2:','<img src="http://www.jeuxvideo.com/smileys/57.gif">',$texte);
	$texte=str_replace(':oui:','<img src="http://www.jeuxvideo.com/smileys/37.gif">',$texte);
	$texte=str_replace(':pacd:','<img src="http://www.jeuxvideo.com/smileys/10.gif">',$texte);
	$texte=str_replace(':pacg:','<img src="http://www.jeuxvideo.com/smileys/9.gif">',$texte);
	$texte=str_replace(':peur:','<img src="http://www.jeuxvideo.com/smileys/47.gif">',$texte);
	$texte=str_replace(':question:','<img src="http://www.jeuxvideo.com/smileys/2.gif">',$texte);
	$texte=str_replace(':rechercher:','<img src="http://www.jeuxvideo.com/smileys/38.gif">',$texte);
	$texte=str_replace(':rire:','<img src="http://www.jeuxvideo.com/smileys/39.gif">',$texte);
	$texte=str_replace(':rire2:','<img src="http://www.jeuxvideo.com/smileys/41.gif">',$texte);
	$texte=str_replace(':rouge:','<img src="http://www.jeuxvideo.com/smileys/55.gif">',$texte);
	$texte=str_replace(':salut:','<img src="http://www.jeuxvideo.com/smileys/42.gif">',$texte);
	$texte=str_replace(':sarcastic:','<img src="http://www.jeuxvideo.com/smileys/43.gif">',$texte);
	$texte=str_replace(':snif:','<img src="http://www.jeuxvideo.com/smileys/20.gif">',$texte);
	$texte=str_replace(':snif2:','<img src="http://www.jeuxvideo.com/smileys/13.gif">',$texte);
	$texte=str_replace(':sleep:','<img src="http://www.jeuxvideo.com/smileys/27.gif">',$texte);
	$texte=str_replace(':spoiler:','<img src="http://www.jeuxvideo.com/smileys/63.gif">',$texte);
	$texte=str_replace(':sors:','<img src="http://www.jeuxvideo.com/smileys/56.gif">',$texte);
	$texte=str_replace(':sournois:','<img src="http://www.jeuxvideo.com/smileys/67.gif">',$texte);
	$texte=str_replace(':svp:','<img src="http://www.jeuxvideo.com/smileys/59.gif">',$texte);
	$texte=str_replace(':up:','<img src="http://www.jeuxvideo.com/smileys/44.gif">',$texte);
	$texte=str_replace(':(','<img src="http://www.jeuxvideo.com/smileys/45.gif">',$texte);
	$texte=str_replace(':)','<img src="http://www.jeuxvideo.com/smileys/1.gif">',$texte);
	$texte=str_replace(':-)))','<img src="http://www.jeuxvideo.com/smileys/23.gif">',$texte);
	$texte=str_replace(':-)','<img src="http://www.jeuxvideo.com/smileys/46.gif">',$texte);
	$texte=str_replace(':-((','<img src="http://www.jeuxvideo.com/smileys/15.gif">',$texte);
	$texte=str_replace(':-(','<img src="http://www.jeuxvideo.com/smileys/14.gif">',$texte);
	$texte=str_replace(':o))','<img src="http://www.jeuxvideo.com/smileys/12.gif">',$texte);
	$texte=str_replace(':-D','<img src="http://www.jeuxvideo.com/smileys/40.gif">',$texte);
	$texte=str_replace(':-p','<img src="http://www.jeuxvideo.com/smileys/31.gif">',$texte);
	$texte=str_replace(':d)','<img src="http://www.jeuxvideo.com/smileys/4.gif">',$texte);
	$texte=str_replace(':g)','<img src="http://www.jeuxvideo.com/smileys/3.gif">',$texte);
	$texte=str_replace(':p)','<img src="http://www.jeuxvideo.com/smileys/7.gif">',$texte);
	$texte=str_replace(':ange:','<img src="http://www.jeuxvideo.com/smileys/60.gif">',$texte);
	$texte=str_replace(':banzai:','<img src="http://www.jeuxvideo.com/smileys/70.gif">',$texte);
	$texte=str_replace(':bave:','<img src="http://www.jeuxvideo.com/smileys/71.gif">',$texte);
	$texte=str_replace(':bravo:','<img src="http://www.jeuxvideo.com/smileys/69.gif">',$texte);
	$texte=str_replace(':bye:','<img src="http://www.jeuxvideo.com/smileys/48.gif">',$texte);
	$texte=str_replace(':cd:','<img src="http://www.jeuxvideo.com/smileys/5.gif">',$texte);
	$texte=str_replace(':coeur:','<img src="http://www.jeuxvideo.com/smileys/54.gif">',$texte);
	$texte=str_replace(':content:','<img src="http://www.jeuxvideo.com/smileys/24.gif">',$texte);
	$texte=str_replace(':cool:','<img src="http://www.jeuxvideo.com/smileys/26.gif">',$texte);
	$texte=str_replace(':diable:','<img src="http://www.jeuxvideo.com/smileys/61.gif">',$texte);
	$texte=str_replace(':dehors:','<img src="http://www.jeuxvideo.com/smileys/52.gif">',$texte);
	$texte=str_replace(':desole:','<img src="http://www.jeuxvideo.com/smileys/65.gif">',$texte);
	$texte=str_replace(':doute:','<img src="http://www.jeuxvideo.com/smileys/28.gif">',$texte);
	$texte=str_replace(':dpdr:','<img src="http://www.jeuxvideo.com/smileys/49.gif">',$texte);
	$texte=str_replace(':fete:','<img src="http://www.jeuxvideo.com/smileys/66.gif">',$texte);
	$texte=str_replace(':fier:','<img src="http://www.jeuxvideo.com/smileys/53.gif">',$texte);
	$texte=str_replace(':fou:','<img src="http://www.jeuxvideo.com/smileys/50.gif">',$texte);
	$texte=str_replace(':gba:','<img src="http://www.jeuxvideo.com/smileys/17.gif">',$texte);
	$texte=str_replace(':globe:','<img src="http://www.jeuxvideo.com/smileys/6.gif">',$texte);
	$texte=str_replace(':gne:','<img src="http://www.jeuxvideo.com/smileys/51.gif">',$texte);
	$texte=str_replace(':gni:','<img src="http://www.jeuxvideo.com/smileys/62.gif">',$texte);
	$texte=str_replace(':hap:','<img src="http://www.jeuxvideo.com/smileys/18.gif">',$texte);
	$texte=str_replace(':hello:','<img src="http://www.jeuxvideo.com/smileys/29.gif">',$texte);
	$texte=str_replace(':honte:','<img src="http://www.jeuxvideo.com/smileys/30.gif">',$texte);
	$texte=str_replace(':hum:','<img src="http://www.jeuxvideo.com/smileys/68.gif">',$texte);
	$texte=str_replace(':hs:','<img src="http://www.jeuxvideo.com/smileys/64.gif">',$texte);
	$texte=str_replace(':lol:','<img src="http://www.jeuxvideo.com/smileys/32.gif">',$texte);
	$texte=str_replace(':mac:','<img src="http://www.jeuxvideo.com/smileys/16.gif">',$texte);
	$texte=str_replace(':malade:','<img src="http://www.jeuxvideo.com/smileys/8.gif">',$texte);
	$texte=str_replace(':merci:','<img src="http://www.jeuxvideo.com/smileys/58.gif">',$texte);
	$texte=str_replace(':monoeil:','<img src="http://www.jeuxvideo.com/smileys/34.gif">',$texte);
	$texte=str_replace(':mort:','<img src="http://www.jeuxvideo.com/smileys/21.gif">',$texte);
	$texte=str_replace(':nah:','<img src="http://www.jeuxvideo.com/smileys/19.gif">',$texte);
	$texte=str_replace(':noel:','<img src="http://www.jeuxvideo.com/smileys/11.gif">',$texte);
	$texte=str_replace(':non:','<img src="http://www.jeuxvideo.com/smileys/35.gif">',$texte);
	$texte=str_replace(':nonnon:','<img src="http://www.jeuxvideo.com/smileys/25.gif">',$texte);
	$texte=str_replace(':non2:','<img src="http://www.jeuxvideo.com/smileys/33.gif">',$texte);
	$texte=str_replace(':ok:','<img src="http://www.jeuxvideo.com/smileys/36.gif">',$texte);
	$texte=str_replace(':ouch:','<img src="http://www.jeuxvideo.com/smileys/22.gif">',$texte);
	$texte=str_replace(':ouch2:','<img src="http://www.jeuxvideo.com/smileys/57.gif">',$texte);
	$texte=str_replace(':oui:','<img src="http://www.jeuxvideo.com/smileys/37.gif">',$texte);
	$texte=str_replace(':pacd:','<img src="http://www.jeuxvideo.com/smileys/10.gif">',$texte);
	$texte=str_replace(':pacg:','<img src="http://www.jeuxvideo.com/smileys/9.gif">',$texte);
	$texte=str_replace(':peur:','<img src="http://www.jeuxvideo.com/smileys/47.gif">',$texte);
	$texte=str_replace(':question:','<img src="http://www.jeuxvideo.com/smileys/2.gif">',$texte);
	$texte=str_replace(':rechercher:','<img src="http://www.jeuxvideo.com/smileys/38.gif">',$texte);
	$texte=str_replace(':rire:','<img src="http://www.jeuxvideo.com/smileys/39.gif">',$texte);
	$texte=str_replace(':rire2:','<img src="http://www.jeuxvideo.com/smileys/41.gif">',$texte);
	$texte=str_replace(':rouge:','<img src="http://www.jeuxvideo.com/smileys/55.gif">',$texte);
	$texte=str_replace(':salut:','<img src="http://www.jeuxvideo.com/smileys/42.gif">',$texte);
	$texte=str_replace(':sarcastic:','<img src="http://www.jeuxvideo.com/smileys/43.gif">',$texte);
	$texte=str_replace(':snif:','<img src="http://www.jeuxvideo.com/smileys/20.gif">',$texte);
	$texte=str_replace(':snif2:','<img src="http://www.jeuxvideo.com/smileys/13.gif">',$texte);
	$texte=str_replace(':sleep:','<img src="http://www.jeuxvideo.com/smileys/27.gif">',$texte);
	$texte=str_replace(':spoiler:','<img src="http://www.jeuxvideo.com/smileys/63.gif">',$texte);
	$texte=str_replace(':sors:','<img src="http://www.jeuxvideo.com/smileys/56.gif">',$texte);
	$texte=str_replace(':sournois:','<img src="http://www.jeuxvideo.com/smileys/67.gif">',$texte);
	$texte=str_replace(':svp:','<img src="http://www.jeuxvideo.com/smileys/59.gif">',$texte);
	$texte=str_replace(':up:','<img src="http://www.jeuxvideo.com/smileys/44.gif">',$texte);
	$texte=str_replace(':(','<img src="http://www.jeuxvideo.com/smileys/45.gif">',$texte);
	$texte=str_replace(':)','<img src="http://www.jeuxvideo.com/smileys/1.gif">',$texte);
	$texte=str_replace(':pf:','<img src="http://image.jeuxvideo.com/smileys_img/pf.gif">',$texte);
	$texte=str_replace(':play:','<img src="http://image.jeuxvideo.com/smileys_img/play.gif">',$texte);
	$texte=str_replace(':cute:','<img src="http://image.jeuxvideo.com/smileys_img/nyu.gif">',$texte);
	$texte=str_replace(':hapoelparty:','<img src="http://image.jeuxvideo.com/smileys_img/hapoelparty.gif">',$texte);
	$texte=str_replace(':loveyou:','<img src="http://image.jeuxvideo.com/smileys_img/loveyou.gif">',$texte);
	
	if($bn)
	$texte = nl2br($texte);
	
	return $texte;
}








function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}
function increase($matches)
{
	return '['.($matches[1]+1).']';
}

function correctNameUpload($name,$path)
{
	$ext = substr(strrchr(basename($name), "."), 1);
	$name = basename($name,'.'.$ext);
	
	while(file_exists($path.$name.'.'.$ext))
	{
		if(!preg_match('#\[[0-9]+\]$#',$name))
		$name = $name.'[0]';
		else
		$name = preg_replace_callback('#\[([0-9]+)\]$#','increase',$name);
	}
	
	return $name.'.'.$ext;
}


function countWord($word,$str)
{
	return mb_substr_count(strtolower($str),strtolower($word));
}

function sortByOccurence($a, $b)
{
	if($a['occurence'] < $b['occurence']) return 1;
	elseif($a['occurence'] > $b['occurence']) return -1;
	else return 0;
}
function sortByTimestamp($a, $b)
{
	if($a['timestamp'] < $b['timestamp']) return 1;
	elseif($a['timestamp'] > $b['timestamp']) return -1;
	else return 0;
}






function toBool($val)
{
	if(intval($val) == 1) return 1;
	else return 0;
}








function convertUrlQuery($query) 
{

	if(($pos = strpos($query, '?')) !== false)
	$query = substr($query, $pos + 1);

    $queryParts = explode('&', $query);
   
    $params = array();
    foreach ($queryParts as $param) {
        $item = explode('=', $param);
        @$params[$item[0]] = $item[1];
    }
   
    return $params;
} 

function nbLines($str)
{
	return substr_count($str, "\n" );
}

function colorNb($nb, $start = '(', $end = ')')
{
	if($nb > 0)
	return '<span style="color:green">'.$start.'+'.$nb.$end.'</span>';
	elseif($nb < 0)
	return '<span style="color:red">'.$start.$nb.$end.'</span>';
	else
	return '<span style="color:black">'.$start.'+'.$nb.$end.'</span>';
}



function age($date)
{
	$birthDate = date('m/d/Y',date2timestamp($date));
	$birthDate = explode("/", $birthDate);
	return $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y")-$birthDate[2])-1):(date("Y")-$birthDate[2]));
}

function datetime2timestamp($string) 
{
	list($date, $time) = explode(' ', $string);
	list($year, $month, $day) = explode('-', $date);
	list($hour, $minute, $second) = explode(':', $time);

	$timestamp = mktime($hour, $minute, $second, $month, $day, $year);

	return $timestamp;
}

function date2timestamp($string) 
{
	list($year, $month, $day) = explode('-', $string);

	$timestamp = mktime(0,0,0, $month, $day, $year);

	return $timestamp;
}

function madate($datetime,$a=1)
{
	$date = datetime2timestamp($datetime);
	
	if($a == 1)
	{
		$temps_s = intval(time()-$date);
		$temps_mn = round((time()-$date)/60);
		$temps_h = round((time()-$date)/60/60);
		
		if($date > (time()-60))
		{
			$madate = 'il y a '.$temps_s.' seconde';
			if($temps_s > 1) $madate .= 's';
		}
		elseif($date > (time()-60*60))
		{
			$madate = 'il y a '.$temps_mn.' minute';
			if($temps_mn > 1) $madate .= 's';
		}
		elseif($date > mktime(0,0,0,date('n'),date('j'),date('Y')))
		{
			$madate = 'il y a '.$temps_h.' heure';
			if($temps_h > 1) $madate .= 's';
		}
		elseif($date < mktime(0,0,0,date('n'),date('j'),date('Y')) && $date > strtotime('yesterday'))
		$madate = 'hier à '.date('H\hi',$date);
		elseif(date('Y') == date('Y', $date) )// meme année, pas besoin de l'afficher
		$madate = 'le '.date('d/m à H\hi',$date).'';
		else
		$madate = 'le '.date('d/m/y à H\hi',$date).'';

	}
	
	return $madate;
}

function future_date($datetime)
{
	$days = array('','lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche');
	$months = array('','janvier','février','mars','avril','mai','juin','juillet','août','septembre','octobre','novembre','décembre');
	$timestamp = datetime2timestamp($datetime);
	
	$heure = ' à '.date('G', $timestamp).'H';
	$mn = date('i', $timestamp);
		
	if($mn == '00') $mn = '';
	
	if($heure == ' à 0H')
	{
		$heure = '';
		$mn = '';
	}
	
	if(is_today($timestamp))
	return 'aujourd\'hui '.$heure.$mn;
	else if(is_tomorrow($timestamp))
	return 'demain '.$heure.$mn;
	else if(is_tomorrow2($timestamp))
	return 'après demain '.$heure.$mn;
	else
	return $days[date('N', $timestamp)].'  '.date('j', $timestamp).' '.$months[date('n',$timestamp)].' '.$heure.$mn;
}

function is_today($timestamp)
{
	$begin = mktime(0, 0, 0);
	$end = mktime(23, 59, 59);
	
	if($timestamp >= $begin && $timestamp <= $end)
	return true;
	else 
	return false;
}

function is_tomorrow($timestamp)
{
	$begin = mktime(0, 0, 0, date('n'), date('j')+1, date('Y'));
	$end = mktime(23, 59, 59, date('n'), date('j')+1, date('Y'));
	
	if($timestamp >= $begin && $timestamp <= $end)
	return true;
	else 
	return false;
}

function is_tomorrow2($timestamp)
{
	$begin = mktime(0, 0, 0, date('n'), date('j')+2, date('Y'));
	$end = mktime(23, 59, 59, date('n'), date('j')+2, date('Y'));
	
	if($timestamp >= $begin && $timestamp <= $end)
	return true;
	else 
	return false;
}





















function multi_array_key_exists($needle, $haystack) {
      foreach ($haystack as $key=>$value) {
        if ($needle==$key) {
          return $key;
        }
        if (is_array($value)) {
          if(multi_array_key_exists($needle, $value)) {
            return $key . ":" . multi_array_key_exists($needle, $value);
          }
        }
      }
  return false;
}

function treeNavToMainMenu($treeNav)
{
	static $call = 0;
	$call++;
	
	$res = '';
	
	if($call == 1)
	$res .= '<ul class="sf-menu">';
	else
	$res .= '<ul>';
	
	foreach($treeNav as $id => $content)
	{
		$down = '';
		if($content['parent'] == null)
		$down = img('down.png');
		
		$plus = '';
		if($content['child'] != null)
		$plus = 'plus';
	
		$res .=  '<li class="'.$plus.'"><a href="'.site_url($content['url']).'">'.htmlspecialchars($content['title']).$down.'</a>';

		if($content['child'] != null)
		$res .= treeNavToMainMenu($content['child']);

		$res .= '</li>';
	}
	
	$res .= '</ul>';
	
	return $res;
	
}

function treeNavToSideMenu($treeNav, $idCurrent)
{
	$res = '';
	$res .= '<ul>';
	
	foreach($treeNav as $id => $content)
	{
		$current = '';
		if($idCurrent == $id)
		$current = 'current';
		
		$plus = 'direct';
		if($content['child'] != null)
		$plus = 'plus';
		
		if($content['child'] != null && (multi_array_key_exists($idCurrent,$content['child']) !== false || $idCurrent == $id))
		$state = 'open';
		else
		$state = 'closed';

		$res .=  '<li class="'.$state.' '.$current.' '.$plus.'"><a href="'.site_url($content['url']).'">'.htmlspecialchars($content['title']).'</a>';

		if($content['child'] != null)
		$res .= treeNavToSideMenu($content['child'], $idCurrent);

		$res .= '</li>';
	}
	
	$res .= '</ul>';
	
	return $res;
	
}

function treeNavToAdmin($treeNav)
{
	$res = '';
	$res .= '<ul>';
	
	foreach($treeNav as $id => $content)
	{
		$res .=  '<li idCreation="'.$content['idCreation'].'"><div class="title">'.htmlspecialchars($content['title']);
		$res .=  '<div class="up img">'.img('sortUp.png').'</div>';
		$res .=  '<div class="down img">'.img('sortDown.png').'</div></div>';
		
		if($content['child'] != null)
		$res .= treeNavToAdmin($content['child']);

		$res .= '</li>';
	}
	
	$res .= '</ul>';
	
	return $res;
}

function treeNavToOptions($categories, $idToSelect , $level = -1)
{
	$level++;
	$res = '';
	

	foreach($categories as $id => $cat)
	{
		$stringSpaces = '';
		for($i = 0 ; $i < $level ; $i++)
		$stringSpaces .= '-----------------';
		
		if($idToSelect == $cat['idCreation'])
		$selected = 'selected';
		else
		$selected = '';
		
		$res .= '<option '.$selected.' value="'.$cat['idCreation'].'">'.$stringSpaces.' '.htmlspecialchars($cat['title']).'</option>';

		if($cat['child'] != null)
		$res .= treeNavToOptions($cat['child'], $idToSelect , $level);		
	}
	
	return $res;
}