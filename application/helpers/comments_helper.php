<?php
function displayCommentaires()
{
	$CI =& get_instance();
	$CI->load->model('comments');	

	$isAjaxRequest = $CI->input->is_ajax_request();
	$data['isAjaxRequest'] = $isAjaxRequest;

	$lastArgument = $CI->uri->segment($CI->uri->total_segments());

	if($isAjaxRequest)
	{
		$page = $lastArgument;
		$url = $CI->input->post('url');
	}
	else
	{
		if(!is_numeric($lastArgument))
		{
			$url = current_url();
			$page = 0;
		}
		else
		{
			$url = dirname(current_url());
			$page = $lastArgument;
		}
	}

	if($CI->input->post('nbMessageParPages') != false)
	$nbMessageParPages = intval($CI->input->post('nbMessageParPages'));
	else
	$nbMessageParPages = 20;

	$idData = intval($CI->input->post('idData'));
	$table = $CI->input->post('table');


	$data['blocked'] = false;
	if($table == 'forum')
	{
		$CI->load->model('forums');

		if($CI->forums->isBlocked($idData))
		$data['blocked'] = true;
	}

	$data['url'] = $url;
	$data['page'] = $page;
	$data['table'] = $table;
	$data['idData'] = $idData;
	$data['nbMessageParPages'] = $nbMessageParPages;
	$data['commentaires'] = $CI->comments->getComments($table, $page, $idData, $nbMessageParPages);
	$data['nbCommentaires'] = $CI->comments->nbComm($table, $idData);

	$CI->load->library('pagination');

	$config['total_rows'] = $data['nbCommentaires'];
	$config['per_page'] = $nbMessageParPages;
	$config['uri_segment'] = $CI->uri->total_segments();
	$config['base_url'] = $url;
	$config['num_links'] = 4;
	$config['first_link'] = 'Début';
	$config['last_link'] = 'Fin';
	$config['next_link'] = 'Suivante »';
	$config['prev_link'] = '« Précédente';
	$config['full_tag_open'] = '<div class="pagination">';
	$config['full_tag_close'] = '</div>';
	$config['cur_tag_open'] = ' <span class="curr">';
	$config['cur_tag_close'] = '</span>';

	$CI->pagination->initialize($config);

	if(!$isAjaxRequest)
	return $CI->load->view('comments',$data, true);
	else
	echo json_encode( array('nbCommentaires' => $data['nbCommentaires'], 'html' => $CI->load->view('comments',$data, true)));
}