<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['robots.txt'] = "home/robots";
$route['sitemap.xml'] = "home/sitemap";
$route['rss'] = "home/rss";
$route['dons'] = "home/dons";
$route['livreDor'] = "home/livreDor";
// $route['mdp'] = "home/mdp";


// stats
$route['stats'] = "home/stats";
$route['setVisite'] = "home/setVisite";
$route['getVisite'] = "home/getVisite";
$route['loadLastPages'] = "home/loadLastPages";

// live
$route['live'] = "home/live";
$route['live/(:any)'] = "home/live/$1";

$route['connectToUser-(:num)'] = "home/connectToUser/$1";
$route['encode/(:any)'] = "home/encode/$1";
$route['decode/(:any)'] = "home/decode/$1";

// chatbox
$route['historiqueChatbox'] = "chatbox/historiqueChatbox";
$route['loadRoom/(:num)'] = "chatbox/loadRoom/$1";
$route['addMessage'] = "chatbox/addMessage";
$route['listRooms'] = "chatbox/listRooms";
$route['listMsg'] = "chatbox/listMsg";
$route['listMsg/(:num)'] = "chatbox/listMsg/$1";
$route['addRoom'] = "chatbox/addRoom";
$route['getConnected'] = "chatbox/getConnected";
$route['deleteMsg/(:num)'] = "chatbox/deleteMsg/$1";
$route['deleteRoom/(:num)'] = "chatbox/deleteRoom/$1";

// evenements
$route['evenement/(:num)/:any'] = "event/index/$1";
$route['addEvent'] = "event/add";
$route['deleteEvent/(:num)'] = "event/deleteEvent/$1";
$route['editEvent'] = "event/editEvent";
$route['evenements'] = "event/listEvents";


// forum
$route['forum'] = "forum/index";
$route['mesSujets'] = "forum/index/2";
$route['forum/(:num)/:any'] = "forum/viewTopic/$1";
$route['addTopic'] = "forum/addTopic";
$route['epingle/(:num)'] = "forum/epingle/$1";
$route['block/(:num)'] = "forum/block/$1";
$route['delete/(:num)'] = "forum/delete/$1";
$route['voteSondage/(:num)/(:num)'] = "forum/vote/$1/$2";
$route['voteSondage/(:num)/(:num)/1'] = "forum/vote/$1/$2/1";
$route['currentSondage/(:num)'] = "forum/currentSondage/$1";

// commentaires
$route['displayComments/(:num)'] = "comment/displayComments/$1";
$route['addComment'] = "comment/addComment";
$route['editComm/(:num)'] = "comment/editComm/$1";
$route['commSuppr/(:num)'] = "comment/deleteComm/$1";
$route['derniersMessages'] = "comment/lastMsg";

// login
$route['connexion'] = "home/connexion";
$route['inscription'] = "home/inscription";
$route['logout'] = "user/logout";

// images
$route['indexImage'] = "image/doIndex";
$route['uploadImages'] = "image/upload";
$route['images'] = "image/view";
$route['getImagesFromAjax/(:num)'] = "image/getImagesFromAjax/$1";
$route['addToGallery'] = "image/addToGallery";
$route['refuseGallery/(:num)'] = "image/refuseGallery/$1";
$route['removeFromGallery/(:num)'] = "image/removeFromGallery/$1";
$route['deleteMyImage/(:num)'] = "image/deleteMyImage/$1";

// zone membre
$route['closeFollow'] = "user/closeFollow";
$route['carteDesMembres'] = "user/mapMembers";
$route['listeDesMembres'] = "user/listeMembers";
$route['listeDesMembres/(:num)'] = "user/listeMembers/$1";
$route['listeDesMembres/(:num)/(:num)'] = "user/listeMembers/$1/$2";
$route['profil-(:num)/(:any)'] = "user/profil/$1";
$route['monCompte'] = "user/index";
$route['monCompte/newConvers'] = "user/newConvers";
$route['monCompte/deleteConvers/(:num)'] = "user/deleteConvers/$1";
$route['monCompte/messagerie/(:any)'] = "user/index/messagerie/0/$1";
$route['addToConvers'] = "user/addToConvers";
$route['messagerie-(:num)'] = "user/index/viewMessaging/$1";
$route['messagerie-(:num)/:any'] = "user/index/viewMessaging/$1";
$route['monCompte/uploadAvatar'] = "user/uploadAvatar";
$route['monCompte/updateProfil'] = "user/updateProfil";
$route['monCompte/(:any)'] = "user/index/$1";


// articles
$route['articles'] = "content/listArticles";
$route['articles/(:num)/:any'] = "content/listArticles/$1";

// admin / site
$route['admin'] = "admin/index";
$route['admin/(:any)'] = "admin/index/$1";
$route['admin/(:any)/(:num)'] = "admin/index/$1/$2";
$route['actionMembers/(:any)/(:any)'] = "admin/actionMembers/$1/$2";

// contenu
$route['archivesNews'] = "content/historyNews";
$route['archivesNews/(:num)'] = "content/historyNews/$1";
$route['submitContent'] = "content/submit";
$route['editerContenu'] = "content/edit";
$route['editerContenu/(:num)'] = "content/edit/$1";
$route['editerContenu/(:num)/(:num)'] = "content/edit/$1/$2";
$route['historique/(:num)'] = "content/view/$1";
$route['deleteMyContenu/(:num)'] = "content/deleteMyContenu/$1";
$route['refreshMenu'] = "content/refreshMenu";
$route['setCurrent/(:num)'] = "content/setCurrent/$1";
$route['diff/(:num)/(:num)/(:num)'] = "content/diff/$1/$2/$3";
$route['sort/(:num)/(:num)'] = "content/sortTree/$1/$2";
$route['inSlider/(:num)'] = "content/inSlider/$1";

// rechercher
$route['rechercher'] = "search/index";
$route['doSearch'] = "search/doSearch";



//concours
$route['concours/(:any)'] = "concours/$1";

$route['debug'] = "content/debug";

$route['(:any)'] = "content/view/$1";
$route['404_override'] = '';

/* End of file routes.php */
/* Location: ./application/config/routes.php */