<?php
class MY_Form_validation extends CI_Form_validation
{
     function __construct($config = array())
     {
          parent::__construct($config);
     }
 
    /**
     * Error Array
     *
     * Returns the error messages as an array
     *
     * @return  array
     */
    function error_array()
    {
     
		$errorsArray = $this->_error_array;
		
		foreach($_POST as $name => $value)
		{
			if(!array_key_exists($name, $errorsArray))
			$errorsArray[$name] = null;
		}
		
		
        return $errorsArray;
    }
}