$(document).ready(function () {

	
	$('form:not(.noValid)').on('submit' ,formValidate);

	function formValidate()
	{
		
		var formSource = $(this);
		
		if(formSource.attr('editor') == 1)
		{
			for ( instance in CKEDITOR.instances )
            CKEDITOR.instances[instance].updateElement();
		}
		
		if(formSource.attr('noLoading') != '')
		formSource.find('input[type=submit]').after('<div id="loading"></div>');

		$.post( formSource.attr('action'), formSource.serialize(), function(json)
		{
			$('#loading').remove();
			
			$.each(json, function(fieldName, error)
			{
				
				
				
				if(error == null) // plus d'erreur, on cache
				{
					$('#error_'+fieldName).fadeOut(300, function() { $(this).remove() } );
				}
				else if($('#error_'+fieldName).length == 1 && $('#error_'+fieldName).html() != error) // il y avait deja une erreur, on l'?crase si elle est diff?rente
				{
					$('#error_'+fieldName).fadeOut(300, function() { 
						$(this).remove() 
						$('[name='+fieldName+']').after('<div id="error_'+fieldName+'" class="error">'+error+'</div>');
						$('#error_'+fieldName).fadeIn(300);
					} );
					
				}
				else if($('#error_'+fieldName).length == 1 && $('#error_'+fieldName).html() == error) // il y avait deja une erreur et c'est la meme
				{
					// on ne fait rien
				}
				else // l'erreur n'existe pas encore, on l'affiche
				{
					$('[name='+fieldName+']').after('<div id="error_'+fieldName+'" class="error">'+error+'</div>');
					$('#error_'+fieldName).fadeIn(300);
				}
				
				
				if(fieldName == 'success')
				{
					formSource[0].reset();
                                        
                    if(error != '')
					alert(error);
				}
				else if(fieldName == 'callback')
				{
					window[error]();
				}
				else if(fieldName == 'redirectTo')
				{
					window.location = error;
				}
				
			});
		
			
		},'json');
		
		return false;
		
	}
	
});


$('[class=error]').live('click', function() { $(this).fadeOut(300, function() { $(this).remove() } ); } );


function redirectMyAccount()
{
    window.location.href = window.location.pathname;
}

function reload()
{
    window.location.href = window.location.pathname;
}

function refresh()
{
    location.reload(true);
}