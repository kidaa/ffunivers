$.ajaxSetup({ cache: false });


$('#listRooms').on('click','#addRoom', createRoom);
function createRoom()
{
	var name = prompt('Entrez le nom de votre salon');
	if(name == null) return false;
	var password = prompt('Mot de passe (laissez vide pour aucun)');
	if(name == null || password == null) return false;
	
	$.post(baseUrl+'addRoom', 
	{
		name:name,
		password:password
	}
	,function(data)
	{
		if(data != '')
		alert(data);
		else
		loadListRoom();		
	});
}

function loadRoom(id,needPassword)
{
	var password = '';
	
	if(needPassword)
	password = prompt('Ce salon requiert un mot de passe');
	
	if(password == null) return;
	
	$.post(baseUrl+'loadRoom/'+id,
	{
		password:password
	}
	,function(data)
	{
		if(data['response'] == 1)
		{
			enterRoom(data['roomName']); 
		}
		else if(data['response'] == 0)
		{
			loadRoom(id, true);
		}
	},'json');
}


loadRoom(idRoomToLoad,false);

function enterRoom(name)
{
	$('#roomName span').html(name);
	$('#listMsg').empty();
	
	currentOnglet = 'roomName';
	
	displayListMsg();
}


// charge la liste des salons
function loadListRoom()
{
	$.get('listRooms', function(data)
	{

		$('#listRooms').html(data);
	});
}
loadListRoom();

// affichage de la liste des salons
var config = {    
     over: showListRooms,  
     timeout: 600,
     sensitivity:30,
     out: hideListRooms 
};

$('#showListRooms').hoverIntent(config);

function showListRooms()
{
	$('#listRooms').slideDown(200);
}

function hideListRooms()
{
	$('#listRooms').slideUp(200);
}

// message priv�e
$('#listMsg').on('click','.mpMsg', function()
{
	var input = $('#addMessage').val();
	
	if(input == 'Votre message :)') input = '';
	
	if(input.charAt(0) == '/')
	input = '/'+$(this).attr('username')+' ';
	else
	input = '/'+$(this).attr('username')+' '+input;
	
	
	$('#addMessage').val( input );
	$('#addMessage')[0].focus();
});

// clique sur un nom de salon dans la liste
$('#listRooms').on('click','.goToRoom',function()
{
	var idRoom = $(this).attr('idRoom');
	
	loadRoom(idRoom);
});

// supprimer un salon
$('#listRooms').on('click','.deleteRoom',function()
{
	var source = $(this);
	$.get( $(this).attr('href'), function()
	{
		source.parent('.goToRoom').slideUp(200);
	});
	return false;
});

// supprimer un message
$('#listMsg').on('click','.deleteMsg',function()
{
	var source = $(this);
	$.get(source.attr('href'), function()
	{
		source.parent('.message').hide();
	});
	return false;
});


// callback quand un message est post�
function messageSended()
{

	var msg = 	$('#addMessage').val();
	var firstWord = msg.split(' ')[0];
	if(firstWord.charAt(0) == '/')
	$('#addMessage').val(firstWord+' ');
	else
	$('#addMessage').val('');

	displayListMsg(true);
}



setInterval('displayListMsg(true)', 2000);
setInterval(displayConnected, 60000);

function displayListMsg(effect)
{

	$.getJSON(baseUrl+'listMsg',
	function(data)
	{
		if(currentOnglet != 'roomName') return;
		
		var newMsg = false;
		var scroll = false;
		if($('#listMsg').scrollTop() + $('#listMsg').innerHeight() >= ( $('#listMsg')[0].scrollHeight - 60) )
		scroll = true;
		
		
		for(idMsg in data)
		{
			if(data[idMsg].match(/removedByAdmin/i))
			$('#msgTchatbox'+idMsg).hide();
			else if($('#msgTchatbox'+idMsg).length == 0) // message pas affich�
			{
				newMsg = true;
				
				$('#listMsg').append(stripslashes(data[idMsg]));
				
				if(!effect)
				$('#listMsg > div:last').show();
				else
				$('#listMsg > div:last').slideDown(200);
				
			}
		}
		
		if(scroll && newMsg)
		{
			$('#listMsg').animate(
			{ 
				  scrollTop: $('#listMsg div:last').offset().top + 100000
			}, 200);
		}
	});
}

function displayConnected()
{
	$.get(baseUrl+'getConnected', function(data)
	{
		$('#connected').html(data);
	})
}
displayConnected();

function stripslashes(str) 
{
str=str.replace(/\\'/g,'\'');
str=str.replace(/\\"/g,'"');
str=str.replace(/\\0/g,'\0');
str=str.replace(/\\\\/g,'\\');
return str;
}
