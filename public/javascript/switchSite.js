$('#switchSite').hoverIntent( makeTall, makeShort )

function makeTall()
{
	$('.slideDownSite').attr('class', 'slideUpSite');
	
	$('.notCurrentSite').stop(true,true).slideDown(150);
}

function makeShort()
{
	$('.slideUpSite').attr('class', 'slideDownSite');
	
	$('.notCurrentSite').stop(true,true).slideUp(150);
}


$('#switchSite .lineSite').on('click', function()
{
	location.href = $(this).attr('link');
	return false;
});