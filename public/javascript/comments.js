var edit = false;

$('#content').on('click','a.commEdit', function()
{
    var id = $(this).parent().parent().parent().attr('idComm');
    var source = $(this);
    
    if(source.text() == ' Edition')
    {
        edit = true;
     
        $.get(source.attr('href'), function(data)
        {
            $('.commentaire[idComm='+id+'] p').replaceWith('<textarea>'+data+'</textarea>');
            $('.commentaire[idComm='+id+'] textarea').focus();
            source.html('<img src="'+baseUrl+'public/images/updateComm.png" /> Terminer');
        });
    }
    else
    {
        edit = false;
        var text = $('.commentaire[idComm='+id+'] textarea').val();
        
        $.post(source.attr('href'),
        {
            text:text,
            idComm:id
        }
        , function(data)
        {
           $('.commentaire[idComm='+id+'] textarea').replaceWith('<p>'+data+'</p>');
           source.html('<img src="'+baseUrl+'public/images/editComm.png" /> Edition');
        });
    }

	return false;

});

$('#content').on('click','a.commSuppr', function()
{
    var id = $(this).parent().parent().parent().attr('idComm');
	var source = $(this);
	  
	$.get(source.attr('href'), function()
    {
        $('.commentaire[idComm='+id+']').remove();
    });
	
	return false;
});



function reloadCommsInterval()
{
    if(edit) return;
    
    $.post(baseUrl+'displayComments/'+page,
    {
        url: url,
        table: table,
        idData: idData,
        nbMessageParPages: nbMessageParPages
    }
    ,function(data)
    {
        if(data.nbCommentaires != nbCommentaires)
        $('#listCommentaires').html(data.html);
    },'json');
    
    return false;
}


setInterval(reloadCommsInterval, 10000);


function refreshComments()
{
    if(edit) return;
	
    $.post(baseUrl+'displayComments/'+page,
    {
        url: url,
        table: table,
        idData: idData,
        nbMessageParPages: nbMessageParPages
    }
    ,function(data)
    {
        $('#listCommentaires').html(data.html);
    },'json');
    
	
    return false;
}


$('#content').on('click', '.pagination a' , function()
{
	var linkValue = $(this).html();
	var numPage = parseInt(linkValue);
	
	if(linkValue == 'Début')
	{
		page = 0;
	}
	else if(linkValue == 'Fin')
	{
		var pagesEnTrop = (nbCommentaires % nbMessageParPages);
		
		if(pagesEnTrop == 0)
		pagesEnTrop = nbMessageParPages;
		
		page = (nbCommentaires-pagesEnTrop);
	}
	else if(linkValue == 'Suivante »')
	{
		page = page+nbMessageParPages;
	}
	else if(linkValue == '« Précédente')
	{
		page = page-nbMessageParPages;
	}
	else
	{
		page = (numPage-1)*nbMessageParPages;
	}
	
	
	$('html,body').animate({scrollTop: $(".pagination:first").offset().top},300);
	refreshComments();
	
	return false;
})


// smileys 
jQuery.fn.extend({
insertAtCaret: function(myValue){
  return this.each(function(i) {
    if (document.selection) {
      //For browsers like Internet Explorer
      this.focus();
      sel = document.selection.createRange();
      sel.text = myValue;
      this.focus();
    }
    else if (this.selectionStart || this.selectionStart == '0') {
      //For browsers like Firefox and Webkit based
      var startPos = this.selectionStart;
      var endPos = this.selectionEnd;
      var scrollTop = this.scrollTop;
      this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
      this.focus();
      this.selectionStart = startPos + myValue.length;
      this.selectionEnd = startPos + myValue.length;
      this.scrollTop = scrollTop;
    } else {
      this.value += myValue;
      this.focus();
    }
  })
}
});


$('.smileys img').on('click', function()
{
	$('.lineComment #message').insertAtCaret( ' '+$(this).attr('alt')+' ' );
});


//bbcode

function showSpoil(source)
{
	var elem = $(source);
	
	if(elem.children('span').length == 1)
	{
		var spoil = elem.children('span').html();
		elem.children('span').remove();
		elem.hide();
		elem.removeClass('spoiler');
		elem.html(spoil);
		elem.fadeIn();
	}
}

function wrapText(elementID, openTag, closeTag) {
    var textArea = $('#' + elementID);
    var len = textArea.val().length;
    var start = textArea[0].selectionStart;
    var end = textArea[0].selectionEnd;
    var selectedText = textArea.val().substring(start, end);
    var replacement = openTag + selectedText + closeTag;
    textArea.val(textArea.val().substring(0, start) + replacement + textArea.val().substring(end, len));
}

$('.bbcode img').on('click', function()
{
	wrapText( 'message', '['+$(this).attr('alt')+']', '[/'+$(this).attr('alt')+']' );
});
